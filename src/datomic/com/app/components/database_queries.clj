(ns com.app.components.database-queries
  (:require
    [com.fulcrologic.rad.database-adapters.datomic :as datomic]
    [com.fulcrologic.rad.database-adapters.datomic-options :as do]
    [datomic.api :as d]
    [taoensso.encore :as enc]))

(defn- env->db [env]
       (some-> env (get-in [do/databases :production]) (deref)))

(defn get-all-todos
      [env query-params]
      (if-let [db (env->db env)]
              (let [ids (d/q '[:find ?uuid
                               :where
                               [?dbid :todo/id ?uuid]] db)]
                   (mapv (fn [[id]] {:todo/id id}) ids))))

(defn get-all-priorities
  [env query-params]
  (if-let [db (some-> (get-in env [::datomic/databases :production]) deref)]
    (let [ids (d/q '[:find [?id ...]
                     :where
                     [?e :priority/label]
                     [?e :priority/id ?id]] db)]
      (mapv (fn [id] {:priority/id id}) ids))))

(defn get-all-contacts
  [env query-params]
  (if-let [db (some-> (get-in env [::datomic/databases :production]) deref)]
    (let [ids (d/q '[:find [?id ...]
                     :where
                     [?e :contact/person]
                     [?e :contact/email]
                     [?e :contact/id ?id]] db)]
      (mapv (fn [id] {:contact/id id}) ids))))

(defn get-all-categories
  [env query-params]
  (if-let [db (some-> (get-in env [::datomic/databases :production]) deref)]
    (let [ids (d/q '[:find [?id ...]
                     :where
                     [?e :category/label]
                     [?e :category/id ?id]] db)]
      (mapv (fn [id] {:category/id id}) ids))))