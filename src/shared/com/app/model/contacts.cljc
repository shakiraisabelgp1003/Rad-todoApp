(ns com.app.model.contacts
  (:require
    [com.fulcrologic.rad.attributes :refer [defattr]]
    [com.fulcrologic.rad.attributes-options :as ao]
    [com.fulcrologic.fulcro.components :refer [defsc]]
    #?(:clj [com.app.components.database-queries :as queries])
    [com.fulcrologic.rad.report-options :as ro]))


(defsc ContactQuery [_ _]
  {:query [:contact/id :contact/person :contact/email]
   :ident :contact/id})

(defattr id :contact/id :uuid
         {ao/identity? true
          ao/schema    :production})

(defattr person :contact/person :string
         {ao/required?      true
          ao/identities     #{:contact/id}
          ro/column-heading "Person name"
          ao/schema         :production})

(defattr email :contact/email :string
         {ao/required?      true
          ao/identities     #{:contact/id}
          ro/column-heading "Person email"
          ao/schema         :production})

(defattr all-contacts :contact/all-contacts :ref
         {ao/target     :contact/id
          ao/pc-output  [{:contact/all-contacts [:contact/id]}]
          ao/pc-resolve (fn [{:keys [query-params] :as env} _]
                          #?(:clj
                             {:contact/all-contacts (queries/get-all-contacts env query-params)}))})

(def attributes [id person email all-contacts])