goog.provide('com.fulcrologic.rad.ui_validation');
/**
 * Use form/invalid-attribute-value? instead
 */
com.fulcrologic.rad.ui_validation.invalid_attribute_value_QMARK_ = com.fulcrologic.rad.form.invalid_attribute_value_QMARK_;
/**
 * Use form/validation-error-message instead
 */
com.fulcrologic.rad.ui_validation.validation_error_message = com.fulcrologic.rad.form.validation_error_message;

//# sourceMappingURL=com.fulcrologic.rad.ui_validation.js.map
