goog.provide('com.fulcrologic.semantic_ui.addons.pagination.ui_pagination');
var module$node_modules$semantic_ui_react$dist$commonjs$addons$Pagination$Pagination=shadow.js.require("module$node_modules$semantic_ui_react$dist$commonjs$addons$Pagination$Pagination", {});
/**
 * A component to render a pagination.
 * 
 *   Props:
 *  - activePage (number|string): Index of the currently active page. ()
 *  - aria-label (string): A pagination item can have an aria label.
 *  - boundaryRange (number|string): Number of always visible pages at the beginning and end. ()
 *  - defaultActivePage (number|string): Initial activePage value. ()
 *  - disabled (bool): A pagination can be disabled.
 *  - ellipsisItem (custom): A shorthand for PaginationItem.
 *  - firstItem (custom): A shorthand for PaginationItem.
 *  - lastItem (custom): A shorthand for PaginationItem.
 *  - nextItem (custom): A shorthand for PaginationItem.
 *  - onPageChange (func): Called on change of an active page.
 *  - pageItem (custom): A shorthand for PaginationItem.
 *  - prevItem (custom): A shorthand for PaginationItem.
 *  - siblingRange (number|string): Number of always visible pages before and after the current one. ()
 *  - totalPages (number|string): Total number of pages. ()
 */
com.fulcrologic.semantic_ui.addons.pagination.ui_pagination.ui_pagination = com.fulcrologic.semantic_ui.factory_helpers.factory_apply(module$node_modules$semantic_ui_react$dist$commonjs$addons$Pagination$Pagination.default);

//# sourceMappingURL=com.fulcrologic.semantic_ui.addons.pagination.ui_pagination.js.map
