goog.provide('com.fulcrologic.semantic_ui.modules.dropdown.ui_dropdown_item');
var module$node_modules$semantic_ui_react$dist$commonjs$modules$Dropdown$DropdownItem=shadow.js.require("module$node_modules$semantic_ui_react$dist$commonjs$modules$Dropdown$DropdownItem", {});
/**
 * An item sub-component for Dropdown component.
 * 
 *   Props:
 *  - active (bool): Style as the currently chosen item.
 *  - as (elementType): An element type to render as (string or function).
 *  - children (node): Primary content.
 *  - className (string): Additional classes.
 *  - content (custom): Shorthand for primary content.
 *  - description (custom): Additional text with less emphasis.
 *  - disabled (bool): A dropdown item can be disabled.
 *  - flag (custom): Shorthand for Flag.
 *  - icon (custom): Shorthand for Icon.
 *  - image (custom): Shorthand for Image.
 *  - label (custom): Shorthand for Label.
 *  - onClick (func): Called on click.
 *  - selected (bool): The item currently selected by keyboard shortcut.
 *  - text (custom): Display text.
 *  - value (bool|number|string): Stored value. ()
 */
com.fulcrologic.semantic_ui.modules.dropdown.ui_dropdown_item.ui_dropdown_item = com.fulcrologic.semantic_ui.factory_helpers.factory_apply(module$node_modules$semantic_ui_react$dist$commonjs$modules$Dropdown$DropdownItem.default);

//# sourceMappingURL=com.fulcrologic.semantic_ui.modules.dropdown.ui_dropdown_item.js.map
