goog.provide('com.wsscode.pathom.core');
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),cljs.core.map_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"keyword?","keyword?",1917797069,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.keyword_QMARK_,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410)], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__78814){
return cljs.core.map_QMARK_(G__78814);
}),new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__19036__auto__,v__19037__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__19037__auto__,(0));
}),new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410))], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentVector.EMPTY),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__78815){
return cljs.core.vector_QMARK_(G__78815);
}),new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.vector_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentVector.EMPTY,new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null))], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","optional?","com.wsscode.pathom.core/optional?",910087942),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"fn","fn",-1175266204),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),new cljs.core.Keyword(null,"list","list",765357683),new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"fn","fn",-1175266204),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword(null,"list","list",765357683)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword("com.wsscode.pathom.core","reader-map","com.wsscode.pathom.core/reader-map",456673075),new cljs.core.Keyword("com.wsscode.pathom.core","reader-seq","com.wsscode.pathom.core/reader-seq",6913016)], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","process-reader","com.wsscode.pathom.core/process-reader",348867871),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","process-error","com.wsscode.pathom.core/process-error",-2116719411),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","error","com.wsscode.pathom.core/error",1967516491),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","spec","cljs.spec.alpha/spec",-707298191,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"gen","gen",142575302),cljs.core.list(new cljs.core.Symbol(null,"fn*","fn*",-752876845,null),cljs.core.PersistentVector.EMPTY,cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","gen","cljs.spec.alpha/gen",147877780,null),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [cljs.core.list(new cljs.core.Symbol("cljs.core","ex-info","cljs.core/ex-info",-409744395,null),"Generated sample error",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"some","some",-1951079573),"data"], null)),"null"], null), null)))),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_,(function (){
return cljs.spec.alpha.gen.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentHashSet.createAsIfByAssoc([cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("Generated sample error",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"some","some",-1951079573),"data"], null))]));
}),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"vector?","vector?",-61367869,null),new cljs.core.Symbol(null,"any?","any?",-318999933,null)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.vector_QMARK_,cljs.core.any_QMARK_], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__78816){
return cljs.core.map_QMARK_(G__78816);
}),new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__19036__auto__,v__19037__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__19037__auto__,(0));
}),new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null))], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","errors*","com.wsscode.pathom.core/errors*",337011276),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("com.wsscode.pathom.core","atom?","com.wsscode.pathom.core/atom?",332525267,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),(function (p1__78817_SHARP_){
return (com.wsscode.pathom.core.atom_QMARK_.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.atom_QMARK_.cljs$core$IFn$_invoke$arity$1(p1__78817_SHARP_) : com.wsscode.pathom.core.atom_QMARK_.call(null,p1__78817_SHARP_));
}));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249),new cljs.core.Symbol("cljs.core","keyword?","cljs.core/keyword?",713156450,null),cljs.core.keyword_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","fail-fast?","com.wsscode.pathom.core/fail-fast?",-272943465),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"key","key",-1516042587)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null),cljs.core.string_QMARK_,null,null),new cljs.core.Symbol("cljs.core","string?","cljs.core/string?",-2072921719,null),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Keyword(null,"value","value",305978217)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_,cljs.core.any_QMARK_], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword(null,"value","value",305978217),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),cljs.core.any_QMARK_,null,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644),new cljs.core.Symbol("cljs.core","set?","cljs.core/set?",-1176684971,null),cljs.core.set_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","js-key-transform","com.wsscode.pathom.core/js-key-transform",-1588372758),new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800),new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","js-value-transform","com.wsscode.pathom.core/js-value-transform",1418749137),new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952),new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null),cljs.core.fn_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"reader","reader",169660853),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"reader","reader",169660853),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reader","reader",169660853)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"reader","reader",169660853),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","reader-fn","com.wsscode.pathom.core/reader-fn",1718614315),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"parser","parser",-1543495310),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"parser","parser",-1543495310),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"parser","parser",-1543495310)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"parser","parser",-1543495310),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","parser","com.wsscode.pathom.core/parser",-740326395),null,null,null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","plugin","com.wsscode.pathom.core/plugin",-881556304),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"opt","opt",-794706369),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null)),cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__78831){
return cljs.core.map_QMARK_(G__78831);
})], null),(function (G__78831){
return cljs.core.map_QMARK_(G__78831);
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null),cljs.core.PersistentVector.EMPTY,null,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null)))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)], null)])));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","parent-join-key","com.wsscode.pathom.core/parent-join-key",-289005491),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"call","call",-519999866),new cljs.core.Keyword("edn-query-language.core","mutation-key","edn-query-language.core/mutation-key",422562651)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword(null,"call","call",-519999866)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword("edn-query-language.core","mutation-key","edn-query-language.core/mutation-key",422562651)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword("edn-query-language.core","mutation-key","edn-query-language.core/mutation-key",422562651)], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),new cljs.core.Keyword("edn-query-language.core","join-query","edn-query-language.core/join-query",587629761),new cljs.core.Keyword("edn-query-language.core","join-query","edn-query-language.core/join-query",587629761));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","union-path","com.wsscode.pathom.core/union-path",-2083478095),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"keyword","keyword",811389747),new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Keyword(null,"fn","fn",-1175266204),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"keyword","keyword",811389747),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),new cljs.core.Symbol("cljs.core","fn?","cljs.core/fn?",71876239,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.core","property","edn-query-language.core/property",-1479624874),cljs.core.fn_QMARK_], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch-size","com.wsscode.pathom.core/async-request-cache-ch-size",-437531159),new cljs.core.Symbol("cljs.core","pos-int?","cljs.core/pos-int?",-2115888030,null),cljs.core.pos_int_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),new cljs.core.Symbol("cljs.core","boolean?","cljs.core/boolean?",1400713761,null),cljs.core.boolean_QMARK_);
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"coll","coll",1647737163),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword(null,"map","map",1371690461),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null))),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"coll","coll",1647737163),new cljs.core.Keyword(null,"map","map",1371690461)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null))], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Symbol(null,"map?","map?",-1780568534,null),cljs.core.map_QMARK_,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),null,new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__78834){
return cljs.core.coll_QMARK_(G__78834);
}),new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null))], null),null),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"any?","any?",-318999933,null),new cljs.core.Symbol(null,"map?","map?",-1780568534,null)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_,cljs.core.map_QMARK_], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__78835){
return cljs.core.map_QMARK_(G__78835);
}),new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__19036__auto__,v__19037__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__19037__auto__,(0));
}),new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null))], null),null)], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"index","index",-1531685915),new cljs.core.Symbol("cljs.core","nat-int?","cljs.core/nat-int?",-164364171,null)),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("s","or","s/or",1876282981,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"index","index",-1531685915),new cljs.core.Symbol(null,"nat-int?","nat-int?",-1879663400,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword(null,"index","index",-1531685915)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Symbol("cljs.core","nat-int?","cljs.core/nat-int?",-164364171,null)], null),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),cljs.core.nat_int_QMARK_], null),null),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__78838){
return cljs.core.vector_QMARK_(G__78838);
}),new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.vector_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","coll-of","cljs.spec.alpha/coll-of",1019430407,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"attr","attr",-604132353),new cljs.core.Keyword("com.wsscode.pathom.core","attribute","com.wsscode.pathom.core/attribute",1954645846),new cljs.core.Keyword(null,"ident","ident",-742346),new cljs.core.Keyword("edn-query-language.core","ident","edn-query-language.core/ident",419196228),new cljs.core.Keyword(null,"index","index",-1531685915),new cljs.core.Symbol("cljs.core","nat-int?","cljs.core/nat-int?",-164364171,null)),new cljs.core.Keyword(null,"kind","kind",-717265803),new cljs.core.Symbol("cljs.core","vector?","cljs.core/vector?",-1550392028,null))], null),null));
cljs.spec.alpha.def_impl(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.every_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","tuple","cljs.spec.alpha/tuple",-415901908,null),new cljs.core.Symbol(null,"any?","any?",-318999933,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.tuple_impl.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.any_QMARK_,new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)], null)),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"into","into",-150836029),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword("cljs.spec.alpha","kind-form","cljs.spec.alpha/kind-form",-1047104697),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword("cljs.spec.alpha","cpred","cljs.spec.alpha/cpred",-693471218),(function (G__78839){
return cljs.core.map_QMARK_(G__78839);
}),new cljs.core.Keyword(null,"kind","kind",-717265803),cljs.core.map_QMARK_,new cljs.core.Keyword("cljs.spec.alpha","kfn","cljs.spec.alpha/kfn",672643897),(function (i__19036__auto__,v__19037__auto__){
return cljs.core.nth.cljs$core$IFn$_invoke$arity$2(v__19037__auto__,(0));
}),new cljs.core.Keyword("cljs.spec.alpha","conform-all","cljs.spec.alpha/conform-all",45201917),true,new cljs.core.Keyword("cljs.spec.alpha","describe","cljs.spec.alpha/describe",1883026911),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","map-of","cljs.spec.alpha/map-of",153715093,null),new cljs.core.Symbol("cljs.core","any?","cljs.core/any?",-2068111842,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681))], null),null));
com.wsscode.pathom.core.break_values = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null,new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),null], null), null);
/**
 * Takes an AST and return a single set with all properties that appear in a query.
 * 
 *   Example:
 * 
 *   (-> [:foo {:bar [:baz]}] eql/query->ast pc/all-out-attributes)
 *   ; => #{:foo :bar :baz}
 */
com.wsscode.pathom.core.ast_properties = (function com$wsscode$pathom$core$ast_properties(p__78841){
var map__78842 = p__78841;
var map__78842__$1 = (((((!((map__78842 == null))))?(((((map__78842.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__78842.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__78842):map__78842);
var children = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78842__$1,new cljs.core.Keyword(null,"children","children",-940561982));
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (attrs,p__78844){
var map__78845 = p__78844;
var map__78845__$1 = (((((!((map__78845 == null))))?(((((map__78845.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__78845.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__78845):map__78845);
var node = map__78845__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78845__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var children__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78845__$1,new cljs.core.Keyword(null,"children","children",-940561982));
var G__78849 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(attrs,key);
if(cljs.core.truth_(children__$1)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(G__78849,(com.wsscode.pathom.core.ast_properties.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.ast_properties.cljs$core$IFn$_invoke$arity$1(node) : com.wsscode.pathom.core.ast_properties.call(null,node)));
} else {
return G__78849;
}
}),cljs.core.PersistentHashSet.EMPTY,children);
});
/**
 * Merges nested maps without overwriting existing keys.
 */
com.wsscode.pathom.core.deep_merge = (function com$wsscode$pathom$core$deep_merge(var_args){
var args__4742__auto__ = [];
var len__4736__auto___80784 = arguments.length;
var i__4737__auto___80788 = (0);
while(true){
if((i__4737__auto___80788 < len__4736__auto___80784)){
args__4742__auto__.push((arguments[i__4737__auto___80788]));

var G__80789 = (i__4737__auto___80788 + (1));
i__4737__auto___80788 = G__80789;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((0) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((0)),(0),null)):null);
return com.wsscode.pathom.core.deep_merge.cljs$core$IFn$_invoke$arity$variadic(argseq__4743__auto__);
});

(com.wsscode.pathom.core.deep_merge.cljs$core$IFn$_invoke$arity$variadic = (function (xs){
if(cljs.core.every_QMARK_((function (p1__78850_SHARP_){
return ((cljs.core.map_QMARK_(p1__78850_SHARP_)) || ((p1__78850_SHARP_ == null)));
}),xs)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(cljs.core.merge_with,com.wsscode.pathom.core.deep_merge,xs);
} else {
return cljs.core.last(xs);
}
}));

(com.wsscode.pathom.core.deep_merge.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(com.wsscode.pathom.core.deep_merge.cljs$lang$applyTo = (function (seq78851){
var self__4724__auto__ = this;
return self__4724__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq78851));
}));

/**
 * Given a query expression convert it into an AST.
 */
com.wsscode.pathom.core.query__GT_ast = (function com$wsscode$pathom$core$query__GT_ast(query_expr){
return com.wsscode.pathom.parser.query__GT_ast(query_expr);
});
/**
 * Call query->ast and return the first children.
 */
com.wsscode.pathom.core.query__GT_ast1 = (function com$wsscode$pathom$core$query__GT_ast1(query_expr){
return cljs.core.first(new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(com.wsscode.pathom.core.query__GT_ast(query_expr)));
});
/**
 * Given an AST convert it back into a query expression.
 */
com.wsscode.pathom.core.ast__GT_query = (function com$wsscode$pathom$core$ast__GT_query(query_ast){
return com.wsscode.pathom.parser.ast__GT_expr.cljs$core$IFn$_invoke$arity$2(query_ast,true);
});
com.wsscode.pathom.core.filter_ast = (function com$wsscode$pathom$core$filter_ast(f,ast){
return clojure.walk.prewalk((function com$wsscode$pathom$core$filter_ast_$_filter_ast_walk(x){
if(((cljs.core.map_QMARK_(x)) && (cljs.core.contains_QMARK_(x,new cljs.core.Keyword(null,"children","children",-940561982))))){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(x,new cljs.core.Keyword(null,"children","children",-940561982),(function (p1__78853_SHARP_){
return cljs.core.filterv(f,p1__78853_SHARP_);
}));
} else {
return x;
}
}),ast);
});
/**
 * Get params from env, always returns a map.
 */
com.wsscode.pathom.core.params = (function com$wsscode$pathom$core$params(env){
var or__4126__auto__ = new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"ast","ast",-860334068).cljs$core$IFn$_invoke$arity$1(env));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
});
/**
 * Add attribute param, eg:
 * 
 *   ```
 *   (p/update-attribute-param :keyword assoc :foo "bar") => (:keyword {:foo "bar"})
 *   (p/update-attribute-param '(:keyword {:param "prev"}) assoc :foo "bar") => (:keyword {:foo "bar" :param "prev"})
 *   ```
 *   
 */
com.wsscode.pathom.core.update_attribute_param = (function com$wsscode$pathom$core$update_attribute_param(var_args){
var args__4742__auto__ = [];
var len__4736__auto___80792 = arguments.length;
var i__4737__auto___80793 = (0);
while(true){
if((i__4737__auto___80793 < len__4736__auto___80792)){
args__4742__auto__.push((arguments[i__4737__auto___80793]));

var G__80794 = (i__4737__auto___80793 + (1));
i__4737__auto___80793 = G__80794;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((2) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.update_attribute_param.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4743__auto__);
});

(com.wsscode.pathom.core.update_attribute_param.cljs$core$IFn$_invoke$arity$variadic = (function (x,f,args){
if(cljs.core.seq_QMARK_(x)){
var vec__78871 = x;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__78871,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__78871,(1),null);
return (new cljs.core.List(null,k,(new cljs.core.List(null,cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f,p,args),null,(1),null)),(2),null));
} else {
return (new cljs.core.List(null,x,(new cljs.core.List(null,cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f,cljs.core.PersistentArrayMap.EMPTY,args),null,(1),null)),(2),null));
}
}));

(com.wsscode.pathom.core.update_attribute_param.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.update_attribute_param.cljs$lang$applyTo = (function (seq78865){
var G__78866 = cljs.core.first(seq78865);
var seq78865__$1 = cljs.core.next(seq78865);
var G__78867 = cljs.core.first(seq78865__$1);
var seq78865__$2 = cljs.core.next(seq78865__$1);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__78866,G__78867,seq78865__$2);
}));

com.wsscode.pathom.core.optional_attribute = (function com$wsscode$pathom$core$optional_attribute(x){
if(cljs.core.truth_((function (){var or__4126__auto__ = (x instanceof cljs.core.Keyword);
if(or__4126__auto__){
return or__4126__auto__;
} else {
return (new cljs.core.List(null,x,null,(1),null));
}
})())){
} else {
throw (new Error(["Assert failed: ","Optional value must be a keyword or a parameterized attribute","\n","(or (keyword? x) (list x))"].join('')));
}

return com.wsscode.pathom.core.update_attribute_param.cljs$core$IFn$_invoke$arity$variadic(x,cljs.core.assoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.core","optional?","com.wsscode.pathom.core/optional?",910087942),true], 0));
});
com.wsscode.pathom.core._QMARK_ = com.wsscode.pathom.core.optional_attribute;
/**
 * Given an AST point, check if the children is a union query type.
 */
com.wsscode.pathom.core.union_children_QMARK_ = (function com$wsscode$pathom$core$union_children_QMARK_(ast){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"union","union",2142937499),(function (){var G__78880 = ast;
var G__78880__$1 = (((G__78880 == null))?null:new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(G__78880));
var G__78880__$2 = (((G__78880__$1 == null))?null:cljs.core.first(G__78880__$1));
if((G__78880__$2 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(G__78880__$2);
}
})());
});
com.wsscode.pathom.core.maybe_merge_union_ast = (function com$wsscode$pathom$core$maybe_merge_union_ast(ast){
if(com.wsscode.pathom.core.union_children_QMARK_(ast)){
var merged_children = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.mapcat.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"children","children",-940561982)),(function (){var G__78883 = ast;
var G__78883__$1 = (((G__78883 == null))?null:new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(G__78883));
var G__78883__$2 = (((G__78883__$1 == null))?null:cljs.core.first(G__78883__$1));
if((G__78883__$2 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(G__78883__$2);
}
})());
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(ast,new cljs.core.Keyword(null,"children","children",-940561982),merged_children,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"query","query",-1288509510),edn_query_language.core.ast__GT_query(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"root","root",-448657453),new cljs.core.Keyword(null,"children","children",-940561982),merged_children], null))], 0));
} else {
return ast;
}
});
com.wsscode.pathom.core.merge_shapes = (function com$wsscode$pathom$core$merge_shapes(var_args){
var G__78885 = arguments.length;
switch (G__78885) {
case 1:
return com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$1 = (function (a){
return a;
}));

(com.wsscode.pathom.core.merge_shapes.cljs$core$IFn$_invoke$arity$2 = (function (a,b){
if(((cljs.core.map_QMARK_(a)) && (cljs.core.map_QMARK_(b)))){
return cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.merge_shapes,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([a,b], 0));
} else {
if(cljs.core.map_QMARK_(a)){
return a;
} else {
if(cljs.core.map_QMARK_(b)){
return b;
} else {
return b;

}
}
}
}));

(com.wsscode.pathom.core.merge_shapes.cljs$lang$maxFixedArity = 2);

cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","ast->shape-descriptor","com.wsscode.pathom.core/ast->shape-descriptor",880860245,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ast","ast",-860334068)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null));


/**
 * Convert AST to shape descriptor format
 * @type {function(*): *}
 */
com.wsscode.pathom.core.ast__GT_shape_descriptor = (function com$wsscode$pathom$core$ast__GT_shape_descriptor(ast){
var map__78899 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ast","ast",-860334068)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"ast","ast",-860334068),new cljs.core.Keyword("edn-query-language.ast","node","edn-query-language.ast/node",-1614840957)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null);
var map__78899__$1 = (((((!((map__78899 == null))))?(((((map__78899.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__78899.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__78899):map__78899);
var retspec78887 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78899__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
var argspec78886 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78899__$1,new cljs.core.Keyword(null,"args","args",1315556576));
if(cljs.core.truth_(argspec78886)){
com.fulcrologic.guardrails.core.run_check(true,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:221 ast->shape-descriptor's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),argspec78886,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ast], null));
} else {
}

var f78889 = (function (ast__$1){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (m,p__78902){
var map__78903 = p__78902;
var map__78903__$1 = (((((!((map__78903 == null))))?(((((map__78903.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__78903.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__78903):map__78903);
var node = map__78903__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78903__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78903__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var children = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78903__$1,new cljs.core.Keyword(null,"children","children",-940561982));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"union","union",2142937499),type)){
var unions = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(com.wsscode.pathom.core.ast__GT_shape_descriptor),children);
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(com.wsscode.pathom.core.merge_shapes,m,unions);
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m,key,(com.wsscode.pathom.core.ast__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.ast__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1(node) : com.wsscode.pathom.core.ast__GT_shape_descriptor.call(null,node)));
}
}),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast__$1));
});
var ret78888 = f78889(ast);
if(cljs.core.truth_(retspec78887)){
com.fulcrologic.guardrails.core.run_check(false,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:221 ast->shape-descriptor's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),retspec78887,ret78888);
} else {
}

return ret78888;
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","map->shape-descriptor","com.wsscode.pathom.core/map->shape-descriptor",464146637,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null));


/**
 * Convert Map to shape descriptor format
 * @type {function(!cljs.core.IMap): *}
 */
com.wsscode.pathom.core.map__GT_shape_descriptor = (function com$wsscode$pathom$core$map__GT_shape_descriptor(m){
var map__78918 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","shape-descriptor","com.wsscode.pathom.core/shape-descriptor",-1569907681),null,null,null);
var map__78918__$1 = (((((!((map__78918 == null))))?(((((map__78918.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__78918.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__78918):map__78918);
var argspec78912 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78918__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec78913 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78918__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec78912)){
com.fulcrologic.guardrails.core.run_check(true,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:234 map->shape-descriptor's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),argspec78912,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [m], null));
} else {
}

var f78915 = (function (m__$1){
return cljs.core.reduce_kv((function (m__$2,k,v){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(m__$2,k,((cljs.core.map_QMARK_(v))?(com.wsscode.pathom.core.map__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.map__GT_shape_descriptor.cljs$core$IFn$_invoke$arity$1(v) : com.wsscode.pathom.core.map__GT_shape_descriptor.call(null,v)):((cljs.core.sequential_QMARK_(v))?cljs.core.transduce.cljs$core$IFn$_invoke$arity$4(cljs.core.comp.cljs$core$IFn$_invoke$arity$2(cljs.core.filter.cljs$core$IFn$_invoke$arity$1(cljs.core.map_QMARK_),cljs.core.map.cljs$core$IFn$_invoke$arity$1(com.wsscode.pathom.core.map__GT_shape_descriptor)),com.wsscode.pathom.core.merge_shapes,cljs.core.PersistentArrayMap.EMPTY,v):cljs.core.PersistentArrayMap.EMPTY
)));
}),cljs.core.PersistentArrayMap.EMPTY,m__$1);
});
var ret78914 = f78915(m);
if(cljs.core.truth_(retspec78913)){
com.fulcrologic.guardrails.core.run_check(false,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:234 map->shape-descriptor's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),retspec78913,ret78914);
} else {
}

return ret78914;
});
com.wsscode.pathom.core.read_from_STAR_ = (function com$wsscode$pathom$core$read_from_STAR_(p__78922,reader){
var map__78923 = p__78922;
var map__78923__$1 = (((((!((map__78923 == null))))?(((((map__78923.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__78923.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__78923):map__78923);
var env = map__78923__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__78923__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
if(cljs.core.map_QMARK_(reader)){
var k = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var temp__5733__auto__ = cljs.core.find(reader,k);
if(cljs.core.truth_(temp__5733__auto__)){
var vec__78926 = temp__5733__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__78926,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__78926,(1),null);
return (com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2(env,v) : com.wsscode.pathom.core.read_from_STAR_.call(null,env,v));
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
} else {
if(cljs.core.vector_QMARK_(reader)){
var res = cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.comp.cljs$core$IFn$_invoke$arity$3(cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (p1__78920_SHARP_){
return (com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.read_from_STAR_.cljs$core$IFn$_invoke$arity$2(env,p1__78920_SHARP_) : com.wsscode.pathom.core.read_from_STAR_.call(null,env,p1__78920_SHARP_));
})),cljs.core.drop_while.cljs$core$IFn$_invoke$arity$1((function (p1__78921_SHARP_){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(p1__78921_SHARP_,new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194));
})),cljs.core.take.cljs$core$IFn$_invoke$arity$1((1))),reader);
if(cljs.core.seq(res)){
return cljs.core.first(res);
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
} else {
if(cljs.core.ifn_QMARK_(reader)){
return (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("Can't process reader",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"reader","reader",169660853),reader], null));

}
}
}
});
/**
 * Runs the read process for the reading, the reader can be a function, a vector or a map:
 * 
 *   function: will receive the environment as argument
 *   map: will dispatch from the ast key to a reader on the map value
 *   vector: will try to run each reader in sequence, when a reader returns ::p/continue it will try the next
 */
com.wsscode.pathom.core.read_from = (function com$wsscode$pathom$core$read_from(env,reader){
var res__75232__auto__ = com.wsscode.pathom.core.read_from_STAR_(env,reader);
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_78957){
var state_val_78958 = (state_78957[(1)]);
if((state_val_78958 === (7))){
var state_78957__$1 = state_78957;
var statearr_78960_80825 = state_78957__$1;
(statearr_78960_80825[(2)] = new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137));

(statearr_78960_80825[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_78958 === (1))){
var state_78957__$1 = state_78957;
var statearr_78961_80826 = state_78957__$1;
(statearr_78961_80826[(2)] = null);

(statearr_78961_80826[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_78958 === (4))){
var inst_78939 = (state_78957[(2)]);
var state_78957__$1 = state_78957;
var statearr_78963_80827 = state_78957__$1;
(statearr_78963_80827[(2)] = inst_78939);

(statearr_78963_80827[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_78958 === (6))){
var inst_78947 = (state_78957[(7)]);
var inst_78946 = (state_78957[(2)]);
var inst_78947__$1 = com.wsscode.async.async_cljs.throw_err(inst_78946);
var inst_78948 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_78947__$1,new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194));
var state_78957__$1 = (function (){var statearr_78965 = state_78957;
(statearr_78965[(7)] = inst_78947__$1);

return statearr_78965;
})();
if(inst_78948){
var statearr_78966_80828 = state_78957__$1;
(statearr_78966_80828[(1)] = (7));

} else {
var statearr_78967_80829 = state_78957__$1;
(statearr_78967_80829[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_78958 === (3))){
var inst_78955 = (state_78957[(2)]);
var state_78957__$1 = state_78957;
return cljs.core.async.impl.ioc_helpers.return_chan(state_78957__$1,inst_78955);
} else {
if((state_val_78958 === (2))){
var _ = (function (){var statearr_78969 = state_78957;
(statearr_78969[(4)] = cljs.core.cons((5),(state_78957[(4)])));

return statearr_78969;
})();
var state_78957__$1 = state_78957;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_78957__$1,(6),res__75232__auto__);
} else {
if((state_val_78958 === (9))){
var inst_78952 = (state_78957[(2)]);
var _ = (function (){var statearr_78970 = state_78957;
(statearr_78970[(4)] = cljs.core.rest((state_78957[(4)])));

return statearr_78970;
})();
var state_78957__$1 = state_78957;
var statearr_78971_80830 = state_78957__$1;
(statearr_78971_80830[(2)] = inst_78952);

(statearr_78971_80830[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_78958 === (5))){
var _ = (function (){var statearr_78972 = state_78957;
(statearr_78972[(4)] = cljs.core.rest((state_78957[(4)])));

return statearr_78972;
})();
var state_78957__$1 = state_78957;
var ex78968 = (state_78957__$1[(2)]);
var statearr_78984_80831 = state_78957__$1;
(statearr_78984_80831[(5)] = ex78968);


var statearr_78985_80832 = state_78957__$1;
(statearr_78985_80832[(1)] = (4));

(statearr_78985_80832[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_78958 === (8))){
var inst_78947 = (state_78957[(7)]);
var state_78957__$1 = state_78957;
var statearr_78986_80833 = state_78957__$1;
(statearr_78986_80833[(2)] = inst_78947);

(statearr_78986_80833[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$read_from_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$read_from_$_state_machine__50857__auto____0 = (function (){
var statearr_78987 = [null,null,null,null,null,null,null,null];
(statearr_78987[(0)] = com$wsscode$pathom$core$read_from_$_state_machine__50857__auto__);

(statearr_78987[(1)] = (1));

return statearr_78987;
});
var com$wsscode$pathom$core$read_from_$_state_machine__50857__auto____1 = (function (state_78957){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_78957);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e78988){var ex__50860__auto__ = e78988;
var statearr_78989_80834 = state_78957;
(statearr_78989_80834[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_78957[(4)]))){
var statearr_78990_80836 = state_78957;
(statearr_78990_80836[(1)] = cljs.core.first((state_78957[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80837 = state_78957;
state_78957 = G__80837;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$read_from_$_state_machine__50857__auto__ = function(state_78957){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$read_from_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$read_from_$_state_machine__50857__auto____1.call(this,state_78957);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$read_from_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$read_from_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$read_from_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$read_from_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$read_from_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_78991 = f__50893__auto__();
(statearr_78991[(6)] = c__50892__auto__);

return statearr_78991;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var res = res__75232__auto__;
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(res,new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194))){
return new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137);
} else {
return res;
}
}
});
/**
 * Like read-from, pulling reader from environment.
 */
com.wsscode.pathom.core.reader = (function com$wsscode$pathom$core$reader(env){
return com.wsscode.pathom.core.read_from(env,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410).cljs$core$IFn$_invoke$arity$1(env));
});
com.wsscode.pathom.core.native_map_QMARK_ = (function com$wsscode$pathom$core$native_map_QMARK_(x){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(x),cljs.core.PersistentArrayMap)) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(x),cljs.core.PersistentHashMap)));
});
/**
 * Walk the structure and transduce every map with xform.
 */
com.wsscode.pathom.core.transduce_maps = (function com$wsscode$pathom$core$transduce_maps(xform,input){
return clojure.walk.prewalk((function com$wsscode$pathom$core$transduce_maps_$_elide_items_walk(x){
if(com.wsscode.pathom.core.native_map_QMARK_(x)){
return cljs.core.with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,xform,x),cljs.core.meta(x));
} else {
return x;
}
}),input);
});
/**
 * Recursively transduce children on the AST, you can use this to apply filter/transformations
 *   on a whole AST. Each iteration of the transducer will get a single AST node to process.
 * 
 *   ```
 *   (->> [:a {:b [:c :d]} :e]
 *     (p/query->ast)
 *     (p/transduce-children (remove (comp #{:a :c} :key)))
 *     (p/ast->query))
 *   ; => [{:b [:d]} :e]
 *   ```
 */
com.wsscode.pathom.core.transduce_children = (function com$wsscode$pathom$core$transduce_children(xform,p__78998){
var map__79000 = p__78998;
var map__79000__$1 = (((((!((map__79000 == null))))?(((((map__79000.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79000.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79000):map__79000);
var node = map__79000__$1;
var children = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79000__$1,new cljs.core.Keyword(null,"children","children",-940561982));
var G__79003 = node;
if(cljs.core.seq(children)){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__79003,new cljs.core.Keyword(null,"children","children",-940561982),(function (children__$1){
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.comp.cljs$core$IFn$_invoke$arity$2(xform,cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (p1__78995_SHARP_){
return (com.wsscode.pathom.core.transduce_children.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.transduce_children.cljs$core$IFn$_invoke$arity$2(xform,p1__78995_SHARP_) : com.wsscode.pathom.core.transduce_children.call(null,xform,p1__78995_SHARP_));
}))),children__$1);
}));
} else {
return G__79003;
}
});
com.wsscode.pathom.core.special_outputs = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null,new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),null], null), null);
/**
 * Removes any item on set item-set from the input
 */
com.wsscode.pathom.core.elide_items = (function com$wsscode$pathom$core$elide_items(item_set,input){
return cljs.core.with_meta(com.wsscode.pathom.core.transduce_maps(cljs.core.remove.cljs$core$IFn$_invoke$arity$1((function (p__79009){
var vec__79011 = p__79009;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79011,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79011,(1),null);
return cljs.core.contains_QMARK_(item_set,v);
})),input),cljs.core.meta(input));
});
/**
 * Convert all ::p/not-found values of maps to nil
 */
com.wsscode.pathom.core.elide_not_found = (function com$wsscode$pathom$core$elide_not_found(input){
return com.wsscode.pathom.core.elide_items(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null], null), null),input);
});
/**
 * Convert all ::p/not-found values of maps to nil
 */
com.wsscode.pathom.core.elide_special_outputs = (function com$wsscode$pathom$core$elide_special_outputs(input){
return com.wsscode.pathom.core.elide_items(com.wsscode.pathom.core.special_outputs,input);
});
com.wsscode.pathom.core.focus_subquery = com.wsscode.pathom.parser.focus_subquery;
com.wsscode.pathom.core.atom_QMARK_ = (function com$wsscode$pathom$core$atom_QMARK_(x){
if((!((x == null)))){
if((((x.cljs$lang$protocol_mask$partition0$ & (32768))) || ((cljs.core.PROTOCOL_SENTINEL === x.cljs$core$IDeref$)))){
return true;
} else {
if((!x.cljs$lang$protocol_mask$partition0$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.IDeref,x);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.IDeref,x);
}
});
com.wsscode.pathom.core.normalize_atom = (function com$wsscode$pathom$core$normalize_atom(x){
if(com.wsscode.pathom.core.atom_QMARK_(x)){
return x;
} else {
return cljs.core.atom.cljs$core$IFn$_invoke$arity$1(x);
}
});
com.wsscode.pathom.core.raw_entity = (function com$wsscode$pathom$core$raw_entity(p__79020){
var map__79021 = p__79020;
var map__79021__$1 = (((((!((map__79021 == null))))?(((((map__79021.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79021.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79021):map__79021);
var env = map__79021__$1;
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79021__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,(function (){var or__4126__auto__ = entity_key;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031);
}
})());
});
com.wsscode.pathom.core.maybe_atom = (function com$wsscode$pathom$core$maybe_atom(x){
if(com.wsscode.pathom.core.atom_QMARK_(x)){
return cljs.core.deref(x);
} else {
return x;
}
});
/**
 * This is used for merging new parsed attributes from entity, works like regular merge but if the value from the right
 *   direction is not found, then the previous value will be kept.
 */
com.wsscode.pathom.core.entity_value_merge = (function com$wsscode$pathom$core$entity_value_merge(x,y){
if((y === new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137))){
return x;
} else {
return y;
}
});
/**
 * Fetch the entity according to the ::entity-key. If the entity is an IAtom, it will be derefed.
 * 
 *   If a second argument is sent, calls the parser against current element to guarantee that some fields are loaded. This
 *   is useful when you need to ensure some values are loaded in order to fetch some more complex data. NOTE: When using
 *   this call with an explicit vector of attributes the parser will not be invoked for attributes that already exist in
 *   the current value of the current entity.
 */
com.wsscode.pathom.core.entity = (function com$wsscode$pathom$core$entity(var_args){
var G__79032 = arguments.length;
switch (G__79032) {
case 1:
return com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1 = (function (env){
var e = com.wsscode.pathom.core.raw_entity(env);
return com.wsscode.pathom.core.maybe_atom(e);
}));

(com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2 = (function (p__79033,attributes){
var map__79034 = p__79033;
var map__79034__$1 = (((((!((map__79034 == null))))?(((((map__79034.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79034.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79034):map__79034);
var env = map__79034__$1;
var parser = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79034__$1,new cljs.core.Keyword(null,"parser","parser",-1543495310));
var e = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
var res__75232__auto__ = (function (){var G__79036 = env;
var G__79037 = cljs.core.filterv(cljs.core.complement(cljs.core.set(cljs.core.keys(e))),attributes);
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__79036,G__79037) : parser.call(null,G__79036,G__79037));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79057){
var state_val_79058 = (state_79057[(1)]);
if((state_val_79058 === (7))){
var inst_79046 = (state_79057[(7)]);
var inst_79049 = cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.entity_value_merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e,inst_79046], 0));
var state_79057__$1 = state_79057;
var statearr_79061_80839 = state_79057__$1;
(statearr_79061_80839[(2)] = inst_79049);

(statearr_79061_80839[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79058 === (1))){
var state_79057__$1 = state_79057;
var statearr_79064_80840 = state_79057__$1;
(statearr_79064_80840[(2)] = null);

(statearr_79064_80840[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79058 === (4))){
var inst_79038 = (state_79057[(2)]);
var state_79057__$1 = state_79057;
var statearr_79066_80841 = state_79057__$1;
(statearr_79066_80841[(2)] = inst_79038);

(statearr_79066_80841[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79058 === (6))){
var inst_79046 = (state_79057[(7)]);
var inst_79045 = (state_79057[(2)]);
var inst_79046__$1 = com.wsscode.async.async_cljs.throw_err(inst_79045);
var inst_79047 = cljs.core.map_QMARK_(inst_79046__$1);
var state_79057__$1 = (function (){var statearr_79068 = state_79057;
(statearr_79068[(7)] = inst_79046__$1);

return statearr_79068;
})();
if(inst_79047){
var statearr_79069_80842 = state_79057__$1;
(statearr_79069_80842[(1)] = (7));

} else {
var statearr_79071_80843 = state_79057__$1;
(statearr_79071_80843[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79058 === (3))){
var inst_79055 = (state_79057[(2)]);
var state_79057__$1 = state_79057;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79057__$1,inst_79055);
} else {
if((state_val_79058 === (2))){
var _ = (function (){var statearr_79073 = state_79057;
(statearr_79073[(4)] = cljs.core.cons((5),(state_79057[(4)])));

return statearr_79073;
})();
var state_79057__$1 = state_79057;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79057__$1,(6),res__75232__auto__);
} else {
if((state_val_79058 === (9))){
var inst_79052 = (state_79057[(2)]);
var _ = (function (){var statearr_79074 = state_79057;
(statearr_79074[(4)] = cljs.core.rest((state_79057[(4)])));

return statearr_79074;
})();
var state_79057__$1 = state_79057;
var statearr_79075_80844 = state_79057__$1;
(statearr_79075_80844[(2)] = inst_79052);

(statearr_79075_80844[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79058 === (5))){
var _ = (function (){var statearr_79076 = state_79057;
(statearr_79076[(4)] = cljs.core.rest((state_79057[(4)])));

return statearr_79076;
})();
var state_79057__$1 = state_79057;
var ex79072 = (state_79057__$1[(2)]);
var statearr_79077_80845 = state_79057__$1;
(statearr_79077_80845[(5)] = ex79072);


var statearr_79078_80846 = state_79057__$1;
(statearr_79078_80846[(1)] = (4));

(statearr_79078_80846[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79058 === (8))){
var state_79057__$1 = state_79057;
var statearr_79079_80847 = state_79057__$1;
(statearr_79079_80847[(2)] = e);

(statearr_79079_80847[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$state_machine__50857__auto____0 = (function (){
var statearr_79080 = [null,null,null,null,null,null,null,null];
(statearr_79080[(0)] = com$wsscode$pathom$core$state_machine__50857__auto__);

(statearr_79080[(1)] = (1));

return statearr_79080;
});
var com$wsscode$pathom$core$state_machine__50857__auto____1 = (function (state_79057){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79057);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79081){var ex__50860__auto__ = e79081;
var statearr_79082_80848 = state_79057;
(statearr_79082_80848[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79057[(4)]))){
var statearr_79083_80849 = state_79057;
(statearr_79083_80849[(1)] = cljs.core.first((state_79057[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80850 = state_79057;
state_79057 = G__80850;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__50857__auto__ = function(state_79057){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__50857__auto____1.call(this,state_79057);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__50857__auto____0;
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__50857__auto____1;
return com$wsscode$pathom$core$state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79084 = f__50893__auto__();
(statearr_79084[(6)] = c__50892__auto__);

return statearr_79084;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var res = res__75232__auto__;
if(cljs.core.map_QMARK_(res)){
return cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.entity_value_merge,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([e,res], 0));
} else {
return e;
}
}
}));

(com.wsscode.pathom.core.entity.cljs$lang$maxFixedArity = 2);

/**
 * Helper function to fetch a single attribute from current entity.
 */
com.wsscode.pathom.core.entity_attr = (function com$wsscode$pathom$core$entity_attr(var_args){
var G__79093 = arguments.length;
switch (G__79093) {
case 2:
return com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$2 = (function (env,attr){
var res__75232__auto__ = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79112){
var state_val_79113 = (state_79112[(1)]);
if((state_val_79113 === (1))){
var state_79112__$1 = state_79112;
var statearr_79115_80852 = state_79112__$1;
(statearr_79115_80852[(2)] = null);

(statearr_79115_80852[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79113 === (2))){
var _ = (function (){var statearr_79116 = state_79112;
(statearr_79116[(4)] = cljs.core.cons((5),(state_79112[(4)])));

return statearr_79116;
})();
var state_79112__$1 = state_79112;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79112__$1,(6),res__75232__auto__);
} else {
if((state_val_79113 === (3))){
var inst_79109 = (state_79112[(2)]);
var state_79112__$1 = state_79112;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79112__$1,inst_79109);
} else {
if((state_val_79113 === (4))){
var inst_79097 = (state_79112[(2)]);
var state_79112__$1 = state_79112;
var statearr_79118_80853 = state_79112__$1;
(statearr_79118_80853[(2)] = inst_79097);

(statearr_79118_80853[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79113 === (5))){
var _ = (function (){var statearr_79119 = state_79112;
(statearr_79119[(4)] = cljs.core.rest((state_79112[(4)])));

return statearr_79119;
})();
var state_79112__$1 = state_79112;
var ex79117 = (state_79112__$1[(2)]);
var statearr_79120_80854 = state_79112__$1;
(statearr_79120_80854[(5)] = ex79117);


var statearr_79121_80855 = state_79112__$1;
(statearr_79121_80855[(1)] = (4));

(statearr_79121_80855[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79113 === (6))){
var inst_79104 = (state_79112[(2)]);
var inst_79105 = com.wsscode.async.async_cljs.throw_err(inst_79104);
var inst_79106 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_79105,attr);
var _ = (function (){var statearr_79122 = state_79112;
(statearr_79122[(4)] = cljs.core.rest((state_79112[(4)])));

return statearr_79122;
})();
var state_79112__$1 = state_79112;
var statearr_79130_80856 = state_79112__$1;
(statearr_79130_80856[(2)] = inst_79106);

(statearr_79130_80856[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$state_machine__50857__auto____0 = (function (){
var statearr_79131 = [null,null,null,null,null,null,null];
(statearr_79131[(0)] = com$wsscode$pathom$core$state_machine__50857__auto__);

(statearr_79131[(1)] = (1));

return statearr_79131;
});
var com$wsscode$pathom$core$state_machine__50857__auto____1 = (function (state_79112){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79112);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79132){var ex__50860__auto__ = e79132;
var statearr_79134_80857 = state_79112;
(statearr_79134_80857[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79112[(4)]))){
var statearr_79136_80858 = state_79112;
(statearr_79136_80858[(1)] = cljs.core.first((state_79112[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80859 = state_79112;
state_79112 = G__80859;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__50857__auto__ = function(state_79112){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__50857__auto____1.call(this,state_79112);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__50857__auto____0;
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__50857__auto____1;
return com$wsscode$pathom$core$state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79139 = f__50893__auto__();
(statearr_79139[(6)] = c__50892__auto__);

return statearr_79139;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var e = res__75232__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,attr);
}
}));

(com.wsscode.pathom.core.entity_attr.cljs$core$IFn$_invoke$arity$3 = (function (env,attr,default$){
var res__75232__auto__ = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79162){
var state_val_79163 = (state_79162[(1)]);
if((state_val_79163 === (7))){
var state_79162__$1 = state_79162;
var statearr_79164_80860 = state_79162__$1;
(statearr_79164_80860[(2)] = default$);

(statearr_79164_80860[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79163 === (1))){
var state_79162__$1 = state_79162;
var statearr_79165_80861 = state_79162__$1;
(statearr_79165_80861[(2)] = null);

(statearr_79165_80861[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79163 === (4))){
var inst_79140 = (state_79162[(2)]);
var state_79162__$1 = state_79162;
var statearr_79166_80862 = state_79162__$1;
(statearr_79166_80862[(2)] = inst_79140);

(statearr_79166_80862[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79163 === (6))){
var inst_79149 = (state_79162[(7)]);
var inst_79147 = (state_79162[(2)]);
var inst_79148 = com.wsscode.async.async_cljs.throw_err(inst_79147);
var inst_79149__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_79148,attr);
var inst_79150 = [null,null,new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null];
var inst_79151 = (new cljs.core.PersistentArrayMap(null,2,inst_79150,null));
var inst_79152 = (new cljs.core.PersistentHashSet(null,inst_79151,null));
var inst_79153 = (inst_79152.cljs$core$IFn$_invoke$arity$1 ? inst_79152.cljs$core$IFn$_invoke$arity$1(inst_79149__$1) : inst_79152.call(null,inst_79149__$1));
var state_79162__$1 = (function (){var statearr_79167 = state_79162;
(statearr_79167[(7)] = inst_79149__$1);

return statearr_79167;
})();
if(cljs.core.truth_(inst_79153)){
var statearr_79176_80863 = state_79162__$1;
(statearr_79176_80863[(1)] = (7));

} else {
var statearr_79177_80864 = state_79162__$1;
(statearr_79177_80864[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79163 === (3))){
var inst_79160 = (state_79162[(2)]);
var state_79162__$1 = state_79162;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79162__$1,inst_79160);
} else {
if((state_val_79163 === (2))){
var _ = (function (){var statearr_79179 = state_79162;
(statearr_79179[(4)] = cljs.core.cons((5),(state_79162[(4)])));

return statearr_79179;
})();
var state_79162__$1 = state_79162;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79162__$1,(6),res__75232__auto__);
} else {
if((state_val_79163 === (9))){
var inst_79157 = (state_79162[(2)]);
var _ = (function (){var statearr_79180 = state_79162;
(statearr_79180[(4)] = cljs.core.rest((state_79162[(4)])));

return statearr_79180;
})();
var state_79162__$1 = state_79162;
var statearr_79182_80865 = state_79162__$1;
(statearr_79182_80865[(2)] = inst_79157);

(statearr_79182_80865[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79163 === (5))){
var _ = (function (){var statearr_79184 = state_79162;
(statearr_79184[(4)] = cljs.core.rest((state_79162[(4)])));

return statearr_79184;
})();
var state_79162__$1 = state_79162;
var ex79178 = (state_79162__$1[(2)]);
var statearr_79186_80866 = state_79162__$1;
(statearr_79186_80866[(5)] = ex79178);


var statearr_79187_80867 = state_79162__$1;
(statearr_79187_80867[(1)] = (4));

(statearr_79187_80867[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79163 === (8))){
var inst_79149 = (state_79162[(7)]);
var state_79162__$1 = state_79162;
var statearr_79190_80868 = state_79162__$1;
(statearr_79190_80868[(2)] = inst_79149);

(statearr_79190_80868[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$state_machine__50857__auto____0 = (function (){
var statearr_79191 = [null,null,null,null,null,null,null,null];
(statearr_79191[(0)] = com$wsscode$pathom$core$state_machine__50857__auto__);

(statearr_79191[(1)] = (1));

return statearr_79191;
});
var com$wsscode$pathom$core$state_machine__50857__auto____1 = (function (state_79162){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79162);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79194){var ex__50860__auto__ = e79194;
var statearr_79195_80869 = state_79162;
(statearr_79195_80869[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79162[(4)]))){
var statearr_79196_80870 = state_79162;
(statearr_79196_80870[(1)] = cljs.core.first((state_79162[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80871 = state_79162;
state_79162 = G__80871;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__50857__auto__ = function(state_79162){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__50857__auto____1.call(this,state_79162);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__50857__auto____0;
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__50857__auto____1;
return com$wsscode$pathom$core$state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79197 = f__50893__auto__();
(statearr_79197[(6)] = c__50892__auto__);

return statearr_79197;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var e = res__75232__auto__;
var x = cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,attr);
if(cljs.core.truth_((function (){var fexpr__79198 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [null,null,new cljs.core.Keyword("com.wsscode.pathom.core","not-found","com.wsscode.pathom.core/not-found",-1411330137),null], null), null);
return (fexpr__79198.cljs$core$IFn$_invoke$arity$1 ? fexpr__79198.cljs$core$IFn$_invoke$arity$1(x) : fexpr__79198.call(null,x));
})())){
return default$;
} else {
return x;
}
}
}));

(com.wsscode.pathom.core.entity_attr.cljs$lang$maxFixedArity = 3);

com.wsscode.pathom.core.entity_BANG_ = (function com$wsscode$pathom$core$entity_BANG_(p__79199,attributes){
var map__79200 = p__79199;
var map__79200__$1 = (((((!((map__79200 == null))))?(((((map__79200.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79200.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79200):map__79200);
var env = map__79200__$1;
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79200__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
var res__75232__auto__ = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,attributes);
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79244){
var state_val_79245 = (state_79244[(1)]);
if((state_val_79245 === (7))){
var inst_79225 = (state_79244[(7)]);
var inst_79220 = (state_79244[(8)]);
var inst_79229 = cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_79225], 0));
var inst_79230 = ["Entity attributes ",inst_79229," could not be realized"].join('');
var inst_79231 = [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),new cljs.core.Keyword("com.wsscode.pathom.core","missing-attributes","com.wsscode.pathom.core/missing-attributes",1114260849)];
var inst_79232 = [inst_79220,path,inst_79225];
var inst_79233 = cljs.core.PersistentHashMap.fromArrays(inst_79231,inst_79232);
var inst_79234 = cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2(inst_79230,inst_79233);
var inst_79235 = (function(){throw inst_79234})();
var state_79244__$1 = state_79244;
var statearr_79247_80872 = state_79244__$1;
(statearr_79247_80872[(2)] = inst_79235);

(statearr_79247_80872[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79245 === (1))){
var state_79244__$1 = state_79244;
var statearr_79248_80873 = state_79244__$1;
(statearr_79248_80873[(2)] = null);

(statearr_79248_80873[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79245 === (4))){
var inst_79212 = (state_79244[(2)]);
var state_79244__$1 = state_79244;
var statearr_79249_80874 = state_79244__$1;
(statearr_79249_80874[(2)] = inst_79212);

(statearr_79249_80874[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79245 === (6))){
var inst_79220 = (state_79244[(8)]);
var inst_79225 = (state_79244[(7)]);
var inst_79219 = (state_79244[(2)]);
var inst_79220__$1 = com.wsscode.async.async_cljs.throw_err(inst_79219);
var inst_79221 = cljs.core.set(attributes);
var inst_79222 = com.wsscode.pathom.core.elide_not_found(inst_79220__$1);
var inst_79223 = cljs.core.keys(inst_79222);
var inst_79224 = cljs.core.set(inst_79223);
var inst_79225__$1 = clojure.set.difference.cljs$core$IFn$_invoke$arity$2(inst_79221,inst_79224);
var inst_79226 = cljs.core.seq(inst_79225__$1);
var state_79244__$1 = (function (){var statearr_79250 = state_79244;
(statearr_79250[(8)] = inst_79220__$1);

(statearr_79250[(7)] = inst_79225__$1);

return statearr_79250;
})();
if(inst_79226){
var statearr_79251_80875 = state_79244__$1;
(statearr_79251_80875[(1)] = (7));

} else {
var statearr_79252_80876 = state_79244__$1;
(statearr_79252_80876[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79245 === (3))){
var inst_79241 = (state_79244[(2)]);
var state_79244__$1 = state_79244;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79244__$1,inst_79241);
} else {
if((state_val_79245 === (2))){
var _ = (function (){var statearr_79254 = state_79244;
(statearr_79254[(4)] = cljs.core.cons((5),(state_79244[(4)])));

return statearr_79254;
})();
var state_79244__$1 = state_79244;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79244__$1,(6),res__75232__auto__);
} else {
if((state_val_79245 === (9))){
var inst_79220 = (state_79244[(8)]);
var inst_79238 = (state_79244[(2)]);
var _ = (function (){var statearr_79262 = state_79244;
(statearr_79262[(4)] = cljs.core.rest((state_79244[(4)])));

return statearr_79262;
})();
var state_79244__$1 = (function (){var statearr_79263 = state_79244;
(statearr_79263[(9)] = inst_79238);

return statearr_79263;
})();
var statearr_79265_80877 = state_79244__$1;
(statearr_79265_80877[(2)] = inst_79220);

(statearr_79265_80877[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79245 === (5))){
var _ = (function (){var statearr_79267 = state_79244;
(statearr_79267[(4)] = cljs.core.rest((state_79244[(4)])));

return statearr_79267;
})();
var state_79244__$1 = state_79244;
var ex79253 = (state_79244__$1[(2)]);
var statearr_79268_80878 = state_79244__$1;
(statearr_79268_80878[(5)] = ex79253);


var statearr_79269_80879 = state_79244__$1;
(statearr_79269_80879[(1)] = (4));

(statearr_79269_80879[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79245 === (8))){
var state_79244__$1 = state_79244;
var statearr_79270_80880 = state_79244__$1;
(statearr_79270_80880[(2)] = null);

(statearr_79270_80880[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto____0 = (function (){
var statearr_79271 = [null,null,null,null,null,null,null,null,null,null];
(statearr_79271[(0)] = com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto__);

(statearr_79271[(1)] = (1));

return statearr_79271;
});
var com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto____1 = (function (state_79244){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79244);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79273){var ex__50860__auto__ = e79273;
var statearr_79275_80881 = state_79244;
(statearr_79275_80881[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79244[(4)]))){
var statearr_79276_80882 = state_79244;
(statearr_79276_80882[(1)] = cljs.core.first((state_79244[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80883 = state_79244;
state_79244 = G__80883;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto__ = function(state_79244){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto____1.call(this,state_79244);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto____0;
com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$entity_BANG__$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79278 = f__50893__auto__();
(statearr_79278[(6)] = c__50892__auto__);

return statearr_79278;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var e = res__75232__auto__;
var missing = clojure.set.difference.cljs$core$IFn$_invoke$arity$2(cljs.core.set(attributes),cljs.core.set(cljs.core.keys(com.wsscode.pathom.core.elide_not_found(e))));
if(cljs.core.seq(missing)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2(["Entity attributes ",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([missing], 0))," could not be realized"].join(''),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),e,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),path,new cljs.core.Keyword("com.wsscode.pathom.core","missing-attributes","com.wsscode.pathom.core/missing-attributes",1114260849),missing], null));
} else {
}

return e;
}
});
/**
 * Like entity-attr. Raises an exception if the property can't be retrieved.
 */
com.wsscode.pathom.core.entity_attr_BANG_ = (function com$wsscode$pathom$core$entity_attr_BANG_(env,attr){
var res__75232__auto__ = com.wsscode.pathom.core.entity_BANG_(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [attr], null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79293){
var state_val_79294 = (state_79293[(1)]);
if((state_val_79294 === (1))){
var state_79293__$1 = state_79293;
var statearr_79300_80884 = state_79293__$1;
(statearr_79300_80884[(2)] = null);

(statearr_79300_80884[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79294 === (2))){
var _ = (function (){var statearr_79301 = state_79293;
(statearr_79301[(4)] = cljs.core.cons((5),(state_79293[(4)])));

return statearr_79301;
})();
var state_79293__$1 = state_79293;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79293__$1,(6),res__75232__auto__);
} else {
if((state_val_79294 === (3))){
var inst_79291 = (state_79293[(2)]);
var state_79293__$1 = state_79293;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79293__$1,inst_79291);
} else {
if((state_val_79294 === (4))){
var inst_79279 = (state_79293[(2)]);
var state_79293__$1 = state_79293;
var statearr_79305_80885 = state_79293__$1;
(statearr_79305_80885[(2)] = inst_79279);

(statearr_79305_80885[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79294 === (5))){
var _ = (function (){var statearr_79306 = state_79293;
(statearr_79306[(4)] = cljs.core.rest((state_79293[(4)])));

return statearr_79306;
})();
var state_79293__$1 = state_79293;
var ex79302 = (state_79293__$1[(2)]);
var statearr_79307_80886 = state_79293__$1;
(statearr_79307_80886[(5)] = ex79302);


var statearr_79308_80887 = state_79293__$1;
(statearr_79308_80887[(1)] = (4));

(statearr_79308_80887[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79294 === (6))){
var inst_79286 = (state_79293[(2)]);
var inst_79287 = com.wsscode.async.async_cljs.throw_err(inst_79286);
var inst_79288 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_79287,attr);
var _ = (function (){var statearr_79309 = state_79293;
(statearr_79309[(4)] = cljs.core.rest((state_79293[(4)])));

return statearr_79309;
})();
var state_79293__$1 = state_79293;
var statearr_79310_80888 = state_79293__$1;
(statearr_79310_80888[(2)] = inst_79288);

(statearr_79310_80888[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto____0 = (function (){
var statearr_79311 = [null,null,null,null,null,null,null];
(statearr_79311[(0)] = com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto__);

(statearr_79311[(1)] = (1));

return statearr_79311;
});
var com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto____1 = (function (state_79293){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79293);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79312){var ex__50860__auto__ = e79312;
var statearr_79313_80889 = state_79293;
(statearr_79313_80889[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79293[(4)]))){
var statearr_79314_80890 = state_79293;
(statearr_79314_80890[(1)] = cljs.core.first((state_79293[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80891 = state_79293;
state_79293 = G__80891;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto__ = function(state_79293){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto____1.call(this,state_79293);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto____0;
com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$entity_attr_BANG__$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79315 = f__50893__auto__();
(statearr_79315[(6)] = c__50892__auto__);

return statearr_79315;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var e = res__75232__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,attr);
}
});
/**
 * Helper to swap the current entity value.
 */
com.wsscode.pathom.core.swap_entity_BANG_ = (function com$wsscode$pathom$core$swap_entity_BANG_(var_args){
var args__4742__auto__ = [];
var len__4736__auto___80892 = arguments.length;
var i__4737__auto___80893 = (0);
while(true){
if((i__4737__auto___80893 < len__4736__auto___80892)){
args__4742__auto__.push((arguments[i__4737__auto___80893]));

var G__80894 = (i__4737__auto___80893 + (1));
i__4737__auto___80893 = G__80894;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((2) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.swap_entity_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4743__auto__);
});

(com.wsscode.pathom.core.swap_entity_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (env,fn,args){
var e = com.wsscode.pathom.core.raw_entity(env);
if(com.wsscode.pathom.core.atom_QMARK_(e)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.swap_BANG_,e,fn,args);
} else {
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(fn,e,args);
}
}));

(com.wsscode.pathom.core.swap_entity_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.swap_entity_BANG_.cljs$lang$applyTo = (function (seq79319){
var G__79320 = cljs.core.first(seq79319);
var seq79319__$1 = cljs.core.next(seq79319);
var G__79321 = cljs.core.first(seq79319__$1);
var seq79319__$2 = cljs.core.next(seq79319__$1);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__79320,G__79321,seq79319__$2);
}));

/**
 * Given an AST, find the child with a given key and run update against it.
 */
com.wsscode.pathom.core.update_child = (function com$wsscode$pathom$core$update_child(var_args){
var args__4742__auto__ = [];
var len__4736__auto___80895 = arguments.length;
var i__4737__auto___80896 = (0);
while(true){
if((i__4737__auto___80896 < len__4736__auto___80895)){
args__4742__auto__.push((arguments[i__4737__auto___80896]));

var G__80897 = (i__4737__auto___80896 + (1));
i__4737__auto___80896 = G__80897;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((2) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.update_child.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4743__auto__);
});

(com.wsscode.pathom.core.update_child.cljs$core$IFn$_invoke$arity$variadic = (function (ast,key,args){
var temp__5733__auto__ = (function (){var G__79331 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast);
var G__79331__$1 = (((G__79331 == null))?null:cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,G__79331));
var G__79331__$2 = (((G__79331__$1 == null))?null:cljs.core.filter.cljs$core$IFn$_invoke$arity$2(cljs.core.comp.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentHashSet.createAsIfByAssoc([key]),new cljs.core.Keyword(null,"key","key",-1516042587),cljs.core.second),G__79331__$1));
if((G__79331__$2 == null)){
return null;
} else {
return cljs.core.ffirst(G__79331__$2);
}
})();
if(cljs.core.truth_(temp__5733__auto__)){
var idx = temp__5733__auto__;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.update_in,ast,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx], null),args);
} else {
return ast;
}
}));

(com.wsscode.pathom.core.update_child.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.update_child.cljs$lang$applyTo = (function (seq79327){
var G__79328 = cljs.core.first(seq79327);
var seq79327__$1 = cljs.core.next(seq79327);
var G__79330 = cljs.core.first(seq79327__$1);
var seq79327__$2 = cljs.core.next(seq79327__$1);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__79328,G__79330,seq79327__$2);
}));

/**
 * Given an AST, find the child with a given key and run update against it.
 */
com.wsscode.pathom.core.update_recursive_depth = (function com$wsscode$pathom$core$update_recursive_depth(var_args){
var args__4742__auto__ = [];
var len__4736__auto___80898 = arguments.length;
var i__4737__auto___80899 = (0);
while(true){
if((i__4737__auto___80899 < len__4736__auto___80898)){
args__4742__auto__.push((arguments[i__4737__auto___80899]));

var G__80900 = (i__4737__auto___80899 + (1));
i__4737__auto___80899 = G__80900;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((2) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((2)),(0),null)):null);
return com.wsscode.pathom.core.update_recursive_depth.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__4743__auto__);
});

(com.wsscode.pathom.core.update_recursive_depth.cljs$core$IFn$_invoke$arity$variadic = (function (ast,key,args){
var temp__5733__auto__ = (function (){var G__79336 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast);
var G__79336__$1 = (((G__79336 == null))?null:cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2(cljs.core.vector,G__79336));
var G__79336__$2 = (((G__79336__$1 == null))?null:cljs.core.filter.cljs$core$IFn$_invoke$arity$2(cljs.core.comp.cljs$core$IFn$_invoke$arity$2((function (p1__79332_SHARP_){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(key,new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(p1__79332_SHARP_))) && (cljs.core.pos_int_QMARK_(new cljs.core.Keyword(null,"query","query",-1288509510).cljs$core$IFn$_invoke$arity$1(p1__79332_SHARP_))));
}),cljs.core.second),G__79336__$1));
if((G__79336__$2 == null)){
return null;
} else {
return cljs.core.ffirst(G__79336__$2);
}
})();
if(cljs.core.truth_(temp__5733__auto__)){
var idx = temp__5733__auto__;
return cljs.core.apply.cljs$core$IFn$_invoke$arity$4(cljs.core.update_in,ast,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx,new cljs.core.Keyword(null,"query","query",-1288509510)], null),args);
} else {
return ast;
}
}));

(com.wsscode.pathom.core.update_recursive_depth.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(com.wsscode.pathom.core.update_recursive_depth.cljs$lang$applyTo = (function (seq79333){
var G__79334 = cljs.core.first(seq79333);
var seq79333__$1 = cljs.core.next(seq79333);
var G__79335 = cljs.core.first(seq79333__$1);
var seq79333__$2 = cljs.core.next(seq79333__$1);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__79334,G__79335,seq79333__$2);
}));

com.wsscode.pathom.core.remove_query_wildcard = (function com$wsscode$pathom$core$remove_query_wildcard(query){
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.with_meta(cljs.core.PersistentVector.EMPTY,cljs.core.meta(query)),cljs.core.remove.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Symbol(null,"*","*",345799209,null),null], null), null)),query);
});
com.wsscode.pathom.core.default_union_path = (function com$wsscode$pathom$core$default_union_path(p__79338){
var map__79339 = p__79338;
var map__79339__$1 = (((((!((map__79339 == null))))?(((((map__79339.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79339.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79339):map__79339);
var env = map__79339__$1;
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79339__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var e = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
var temp__5733__auto__ = (function (){var G__79341 = cljs.core.keys(query);
var G__79341__$1 = (((G__79341 == null))?null:cljs.core.filter.cljs$core$IFn$_invoke$arity$2((function (p1__79337_SHARP_){
return ((cljs.core.contains_QMARK_(e,p1__79337_SHARP_)) && (cljs.core.not((function (){var G__79343 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(e,p1__79337_SHARP_);
return (com.wsscode.pathom.core.break_values.cljs$core$IFn$_invoke$arity$1 ? com.wsscode.pathom.core.break_values.cljs$core$IFn$_invoke$arity$1(G__79343) : com.wsscode.pathom.core.break_values.call(null,G__79343));
})())));
}),G__79341));
if((G__79341__$1 == null)){
return null;
} else {
return cljs.core.first(G__79341__$1);
}
})();
if(cljs.core.truth_(temp__5733__auto__)){
var path = temp__5733__auto__;
return path;
} else {
return null;
}
});
com.wsscode.pathom.core.placeholder_key_QMARK_ = (function com$wsscode$pathom$core$placeholder_key_QMARK_(p__79344,k){
var map__79345 = p__79344;
var map__79345__$1 = (((((!((map__79345 == null))))?(((((map__79345.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79345.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79345):map__79345);
var placeholder_prefixes = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79345__$1,new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644));
var placeholder_prefixes__$1 = (function (){var or__4126__auto__ = placeholder_prefixes;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [">",null], null), null);
}
})();
return (((k instanceof cljs.core.Keyword)) && (cljs.core.contains_QMARK_(placeholder_prefixes__$1,cljs.core.namespace(k))));
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","path-without-placeholders","com.wsscode.pathom.core/path-without-placeholders",817934882,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__79352){
return cljs.core.map_QMARK_(G__79352);
}),(function (G__79352){
return cljs.core.contains_QMARK_(G__79352,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
})], null),(function (G__79352){
return ((cljs.core.map_QMARK_(G__79352)) && (cljs.core.contains_QMARK_(G__79352,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661))));
}),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)))], null),null]))], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null,null));


/**
 * @type {function(!cljs.core.IMap): *}
 */
com.wsscode.pathom.core.path_without_placeholders = (function com$wsscode$pathom$core$path_without_placeholders(p__79353){
var map__79354 = p__79353;
var map__79354__$1 = (((((!((map__79354 == null))))?(((((map__79354.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79354.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79354):map__79354);
var env = map__79354__$1;
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79354__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
var map__79356 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__79357){
return cljs.core.map_QMARK_(G__79357);
}),(function (G__79357){
return cljs.core.contains_QMARK_(G__79357,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
})], null),(function (G__79357){
return ((cljs.core.map_QMARK_(G__79357)) && (cljs.core.contains_QMARK_(G__79357,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661))));
}),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null),cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null))),cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","contains?","cljs.core/contains?",-976526835,null),new cljs.core.Symbol(null,"%","%",-950237169,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)))], null),null]))], null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661)], null))),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),null,null,null);
var map__79356__$1 = (((((!((map__79356 == null))))?(((((map__79356.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79356.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79356):map__79356);
var argspec79348 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79356__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec79349 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79356__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec79348)){
com.fulcrologic.guardrails.core.run_check(true,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:456 path-without-placeholders's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),argspec79348,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [env], null));
} else {
}

var f79351 = (function (p__79359){
var map__79360 = p__79359;
var map__79360__$1 = (((((!((map__79360 == null))))?(((((map__79360.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79360.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79360):map__79360);
var env__$1 = map__79360__$1;
var path__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79360__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$1((function (p1__79347_SHARP_){
return com.wsscode.pathom.core.placeholder_key_QMARK_(env__$1,p1__79347_SHARP_);
})),path__$1);
});
var ret79350 = f79351(env);
if(cljs.core.truth_(retspec79349)){
com.fulcrologic.guardrails.core.run_check(false,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:456 path-without-placeholders's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),retspec79349,ret79350);
} else {
}

return ret79350;
});
/**
 * Find the closest parent key that's not a placeholder key.
 */
com.wsscode.pathom.core.find_closest_non_placeholder_parent_join_key = (function com$wsscode$pathom$core$find_closest_non_placeholder_parent_join_key(p__79363){
var map__79364 = p__79363;
var map__79364__$1 = (((((!((map__79364 == null))))?(((((map__79364.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79364.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79364):map__79364);
var env = map__79364__$1;
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79364__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
return cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__79362_SHARP_){
return com.wsscode.pathom.core.placeholder_key_QMARK_(env,p1__79362_SHARP_);
}),cljs.core.drop.cljs$core$IFn$_invoke$arity$2((1),cljs.core.rseq((function (){var or__4126__auto__ = path;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.PersistentVector.EMPTY;
}
})()))));
});
/**
 * Runs a parser with current sub-query. When run with an `entity` argument, that entity is set as the new environment
 * value of `::entity`, and the subquery is parsed with that new environment. When run without an `entity` it
 * parses the current subquery in the context of whatever entity was already in `::entity` of the env.
 */
com.wsscode.pathom.core.join = (function com$wsscode$pathom$core$join(var_args){
var G__79367 = arguments.length;
switch (G__79367) {
case 2:
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2 = (function (entity,p__79368){
var map__79369 = p__79368;
var map__79369__$1 = (((((!((map__79369 == null))))?(((((map__79369.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79369.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79369):map__79369);
var env = map__79369__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79369__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79369__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79369__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
if(com.wsscode.pathom.core.atom_QMARK_(entity)){
var temp__5733__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(entity));
if(cljs.core.truth_(temp__5733__auto__)){
var env_SINGLEQUOTE_ = temp__5733__auto__;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(entity,cljs.core.dissoc,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378));

return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(env_SINGLEQUOTE_,new cljs.core.Keyword(null,"ast","ast",-860334068),ast,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"query","query",-1288509510),query,entity_key,entity], 0)));
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,entity));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378).cljs$core$IFn$_invoke$arity$1(entity))){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(cljs.core.get.cljs$core$IFn$_invoke$arity$2(entity,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378)),new cljs.core.Keyword(null,"ast","ast",-860334068),ast,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"query","query",-1288509510),query,entity_key,cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(entity,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378)))], 0)));
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,cljs.core.atom.cljs$core$IFn$_invoke$arity$1(entity)));
}
}
}));

(com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1 = (function (p__79371){
var map__79372 = p__79371;
var map__79372__$1 = (((((!((map__79372 == null))))?(((((map__79372.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79372.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79372):map__79372);
var env = map__79372__$1;
var parser = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79372__$1,new cljs.core.Keyword(null,"parser","parser",-1543495310));
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79372__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79372__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var union_path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79372__$1,new cljs.core.Keyword("com.wsscode.pathom.core","union-path","com.wsscode.pathom.core/union-path",-2083478095));
var parent_query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79372__$1,new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594));
var processing_sequence = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79372__$1,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637));
var e = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
var placeholder_QMARK_ = com.wsscode.pathom.core.placeholder_key_QMARK_(env,new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(ast));
var union_path__$1 = ((com.wsscode.pathom.core.union_children_QMARK_(ast))?(function (){var union_path__$1 = (function (){var or__4126__auto__ = union_path;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return com.wsscode.pathom.core.default_union_path;
}
})();
var path = ((cljs.core.fn_QMARK_(union_path__$1))?(union_path__$1.cljs$core$IFn$_invoke$arity$1 ? union_path__$1.cljs$core$IFn$_invoke$arity$1(env) : union_path__$1.call(null,env)):(((union_path__$1 instanceof cljs.core.Keyword))?cljs.core.get.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.entity_BANG_(env,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [union_path__$1], null)),union_path__$1):null));
return path;
})():null);
var query__$1 = ((com.wsscode.pathom.core.union_children_QMARK_(ast))?(function (){var or__4126__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(query,union_path__$1);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","blank-union","com.wsscode.pathom.core/blank-union",1901666614);
}
})():query);
var env_SINGLEQUOTE_ = (function (){var G__79374 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic((function (){var G__79375 = env;
if(cljs.core.truth_(union_path__$1)){
return cljs.core.update.cljs$core$IFn$_invoke$arity$4(G__79375,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,union_path__$1);
} else {
return G__79375;
}
})(),new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),query__$1,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.core","parent-join-key","com.wsscode.pathom.core/parent-join-key",-289005491),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast)], 0));
if((!(placeholder_QMARK_))){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__79374,new cljs.core.Keyword("com.wsscode.pathom.parser","waiting","com.wsscode.pathom.parser/waiting",-798662278),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.parser","key-watchers","com.wsscode.pathom.parser/key-watchers",-1670257404)], 0));
} else {
return G__79374;
}
})();
var env_SINGLEQUOTE___$1 = (cljs.core.truth_(processing_sequence)?(cljs.core.truth_((function (){var and__4115__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","stop-sequence?","com.wsscode.pathom.core/stop-sequence?",-1854144982).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(processing_sequence));
if(cljs.core.truth_(and__4115__auto__)){
return (!(placeholder_QMARK_));
} else {
return and__4115__auto__;
}
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(env_SINGLEQUOTE_,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637)):cljs.core.update.cljs$core$IFn$_invoke$arity$6(env_SINGLEQUOTE_,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),cljs.core.vary_meta,cljs.core.assoc,new cljs.core.Keyword("com.wsscode.pathom.core","stop-sequence?","com.wsscode.pathom.core/stop-sequence?",-1854144982),true)):env_SINGLEQUOTE_);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("com.wsscode.pathom.core","blank-union","com.wsscode.pathom.core/blank-union",1901666614),query__$1)){
return cljs.core.PersistentArrayMap.EMPTY;
} else {
if((query__$1 == null)){
return e;
} else {
if(cljs.core.nat_int_QMARK_(query__$1)){
if((query__$1 === (0))){
return null;
} else {
var parent_query_SINGLEQUOTE_ = com.wsscode.pathom.core.ast__GT_query(com.wsscode.pathom.core.update_recursive_depth.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.query__GT_ast(parent_query),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.dec], 0)));
var G__79376 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env_SINGLEQUOTE___$1,new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),parent_query_SINGLEQUOTE_);
var G__79377 = com.wsscode.pathom.core.remove_query_wildcard(parent_query_SINGLEQUOTE_);
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__79376,G__79377) : parser.call(null,G__79376,G__79377));
}
} else {
if(cljs.core.truth_(cljs.core.some(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Symbol(null,"*","*",345799209,null),null], null), null),query__$1))){
var res__75232__auto__ = (function (){var G__79378 = env_SINGLEQUOTE___$1;
var G__79379 = com.wsscode.pathom.core.remove_query_wildcard(query__$1);
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__79378,G__79379) : parser.call(null,G__79378,G__79379));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79395){
var state_val_79396 = (state_79395[(1)]);
if((state_val_79396 === (1))){
var state_79395__$1 = state_79395;
var statearr_79397_80904 = state_79395__$1;
(statearr_79397_80904[(2)] = null);

(statearr_79397_80904[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79396 === (2))){
var _ = (function (){var statearr_79398 = state_79395;
(statearr_79398[(4)] = cljs.core.cons((5),(state_79395[(4)])));

return statearr_79398;
})();
var state_79395__$1 = state_79395;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79395__$1,(6),res__75232__auto__);
} else {
if((state_val_79396 === (3))){
var inst_79393 = (state_79395[(2)]);
var state_79395__$1 = state_79395;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79395__$1,inst_79393);
} else {
if((state_val_79396 === (4))){
var inst_79380 = (state_79395[(2)]);
var state_79395__$1 = state_79395;
var statearr_79400_80905 = state_79395__$1;
(statearr_79400_80905[(2)] = inst_79380);

(statearr_79400_80905[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79396 === (5))){
var _ = (function (){var statearr_79401 = state_79395;
(statearr_79401[(4)] = cljs.core.rest((state_79395[(4)])));

return statearr_79401;
})();
var state_79395__$1 = state_79395;
var ex79399 = (state_79395__$1[(2)]);
var statearr_79402_80906 = state_79395__$1;
(statearr_79402_80906[(5)] = ex79399);


var statearr_79403_80907 = state_79395__$1;
(statearr_79403_80907[(1)] = (4));

(statearr_79403_80907[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79396 === (6))){
var inst_79387 = (state_79395[(2)]);
var inst_79388 = com.wsscode.async.async_cljs.throw_err(inst_79387);
var inst_79389 = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env_SINGLEQUOTE___$1);
var inst_79390 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_79389,inst_79388], 0));
var _ = (function (){var statearr_79404 = state_79395;
(statearr_79404[(4)] = cljs.core.rest((state_79395[(4)])));

return statearr_79404;
})();
var state_79395__$1 = state_79395;
var statearr_79405_80908 = state_79395__$1;
(statearr_79405_80908[(2)] = inst_79390);

(statearr_79405_80908[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$state_machine__50857__auto____0 = (function (){
var statearr_79406 = [null,null,null,null,null,null,null];
(statearr_79406[(0)] = com$wsscode$pathom$core$state_machine__50857__auto__);

(statearr_79406[(1)] = (1));

return statearr_79406;
});
var com$wsscode$pathom$core$state_machine__50857__auto____1 = (function (state_79395){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79395);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79407){var ex__50860__auto__ = e79407;
var statearr_79408_80909 = state_79395;
(statearr_79408_80909[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79395[(4)]))){
var statearr_79409_80910 = state_79395;
(statearr_79409_80910[(1)] = cljs.core.first((state_79395[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80911 = state_79395;
state_79395 = G__80911;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$state_machine__50857__auto__ = function(state_79395){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$state_machine__50857__auto____1.call(this,state_79395);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$state_machine__50857__auto____0;
com$wsscode$pathom$core$state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$state_machine__50857__auto____1;
return com$wsscode$pathom$core$state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79410 = f__50893__auto__();
(statearr_79410[(6)] = c__50892__auto__);

return statearr_79410;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var computed_e = res__75232__auto__;
return cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env_SINGLEQUOTE___$1),computed_e], 0));
}
} else {
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env_SINGLEQUOTE___$1,query__$1) : parser.call(null,env_SINGLEQUOTE___$1,query__$1));

}
}
}
}
}));

(com.wsscode.pathom.core.join.cljs$lang$maxFixedArity = 2);

com.wsscode.pathom.core.join_seq_parallel = (function com$wsscode$pathom$core$join_seq_parallel(p__79412,coll){
var map__79413 = p__79412;
var map__79413__$1 = (((((!((map__79413 == null))))?(((((map__79413.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79413.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79413):map__79413);
var env = map__79413__$1;
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79413__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var entity_path_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79413__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-path-cache","com.wsscode.pathom.core/entity-path-cache",-1017384397));
var parent_query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79413__$1,new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594));
var query_SINGLEQUOTE_ = ((cljs.core.nat_int_QMARK_(query))?parent_query:query);
if(((cljs.core.seq(coll)) && (((cljs.core.vector_QMARK_(query)) || (cljs.core.pos_int_QMARK_(query)) || (cljs.core.map_QMARK_(query)))))){
var ch__75196__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__50892__auto___80914 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79549){
var state_val_79550 = (state_79549[(1)]);
if((state_val_79550 === (7))){
var inst_79490 = (state_79549[(7)]);
var inst_79493 = (state_79549[(8)]);
var inst_79496 = (state_79549[(9)]);
var inst_79495 = (state_79549[(10)]);
var inst_79494 = (state_79549[(11)]);
var inst_79498 = (state_79549[(12)]);
var inst_79501 = (state_79549[(13)]);
var inst_79490__$1 = edn_query_language.core.query__GT_ast(query_SINGLEQUOTE_);
var inst_79491 = (function (){var ast = inst_79490__$1;
return (function (p1__79411_SHARP_){
return cljs.core.not(new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(p1__79411_SHARP_));
});
})();
var inst_79492 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(inst_79490__$1);
var inst_79493__$1 = cljs.core.every_QMARK_(inst_79491,inst_79492);
var inst_79494__$1 = (function (){var ast = inst_79490__$1;
var check_ast_opt_QMARK_ = inst_79493__$1;
return (function com$wsscode$pathom$core$join_seq_parallel_$_join_item(env__$1,entity){
var or__4126__auto__ = ((check_ast_opt_QMARK_)?cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ent,p__79569){
var map__79570 = p__79569;
var map__79570__$1 = (((((!((map__79570 == null))))?(((((map__79570.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79570.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79570):map__79570);
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79570__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79570__$1,new cljs.core.Keyword(null,"params","params",710516235));
var temp__5733__auto__ = cljs.core.find(entity,key);
if(cljs.core.truth_(temp__5733__auto__)){
var vec__79572 = temp__5733__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79572,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79572,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(ent,cljs.core.get.cljs$core$IFn$_invoke$arity$3(params,new cljs.core.Keyword("pathom","as","pathom/as",-2134138450),key),v);
} else {
return cljs.core.reduced(null);
}
}),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast)):false);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(entity,env__$1);
}
});
})();
var inst_79495__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),coll);
var inst_79496__$1 = coll;
var inst_79497 = cljs.core.seq(inst_79496__$1);
var inst_79498__$1 = cljs.core.first(inst_79497);
var inst_79499 = cljs.core.next(inst_79497);
var inst_79500 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(inst_79495__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,(0));
var inst_79501__$1 = inst_79494__$1(inst_79500,inst_79498__$1);
var inst_79502 = com.wsscode.async.async_cljs.chan_QMARK_(inst_79501__$1);
var state_79549__$1 = (function (){var statearr_79575 = state_79549;
(statearr_79575[(7)] = inst_79490__$1);

(statearr_79575[(8)] = inst_79493__$1);

(statearr_79575[(11)] = inst_79494__$1);

(statearr_79575[(10)] = inst_79495__$1);

(statearr_79575[(9)] = inst_79496__$1);

(statearr_79575[(12)] = inst_79498__$1);

(statearr_79575[(14)] = inst_79499);

(statearr_79575[(13)] = inst_79501__$1);

return statearr_79575;
})();
if(inst_79502){
var statearr_79576_80919 = state_79549__$1;
(statearr_79576_80919[(1)] = (21));

} else {
var statearr_79577_80920 = state_79549__$1;
(statearr_79577_80920[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (20))){
var inst_79429 = (state_79549[(15)]);
var inst_79478 = (state_79549[(2)]);
var inst_79479 = [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.trace","style","com.wsscode.pathom.trace/style",496837297)];
var inst_79480 = [new cljs.core.Keyword(null,"fill","fill",883462889),new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_79481 = ["#e0e3a4","0.8"];
var inst_79482 = cljs.core.PersistentHashMap.fromArrays(inst_79480,inst_79481);
var inst_79483 = [new cljs.core.Keyword("com.wsscode.pathom.core","parallel-sequence-loop","com.wsscode.pathom.core/parallel-sequence-loop",1692546245),inst_79482];
var inst_79484 = cljs.core.PersistentHashMap.fromArrays(inst_79479,inst_79483);
var inst_79485 = com.wsscode.pathom.trace.trace_leave.cljs$core$IFn$_invoke$arity$3(env,inst_79429,inst_79484);
var state_79549__$1 = (function (){var statearr_79578 = state_79549;
(statearr_79578[(16)] = inst_79485);

return statearr_79578;
})();
var statearr_79579_80921 = state_79549__$1;
(statearr_79579_80921[(2)] = inst_79478);

(statearr_79579_80921[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (27))){
var inst_79520 = (state_79549[(2)]);
var state_79549__$1 = state_79549;
var statearr_79580_80922 = state_79549__$1;
(statearr_79580_80922[(2)] = inst_79520);

(statearr_79580_80922[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (1))){
var state_79549__$1 = state_79549;
var statearr_79581_80923 = state_79549__$1;
(statearr_79581_80923[(2)] = null);

(statearr_79581_80923[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (24))){
var inst_79505 = (state_79549[(2)]);
var inst_79506 = com.wsscode.async.async_cljs.throw_err(inst_79505);
var state_79549__$1 = state_79549;
var statearr_79582_80924 = state_79549__$1;
(statearr_79582_80924[(2)] = inst_79506);

(statearr_79582_80924[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (4))){
var inst_79415 = (state_79549[(2)]);
var state_79549__$1 = state_79549;
var statearr_79583_80925 = state_79549__$1;
(statearr_79583_80925[(2)] = inst_79415);

(statearr_79583_80925[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (15))){
var inst_79463 = (state_79549[(2)]);
var state_79549__$1 = state_79549;
var statearr_79584_80927 = state_79549__$1;
(statearr_79584_80927[(2)] = inst_79463);

(statearr_79584_80927[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (21))){
var inst_79501 = (state_79549[(13)]);
var state_79549__$1 = state_79549;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79549__$1,(24),inst_79501);
} else {
if((state_val_79550 === (31))){
var inst_79518 = (state_79549[(2)]);
var state_79549__$1 = state_79549;
var statearr_79585_80928 = state_79549__$1;
(statearr_79585_80928[(2)] = inst_79518);

(statearr_79585_80928[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (32))){
var inst_79535 = (state_79549[(2)]);
var state_79549__$1 = state_79549;
var statearr_79586_80930 = state_79549__$1;
(statearr_79586_80930[(2)] = inst_79535);

(statearr_79586_80930[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (33))){
var inst_79543 = cljs.core.async.close_BANG_(ch__75196__auto__);
var state_79549__$1 = state_79549;
var statearr_79587_80932 = state_79549__$1;
(statearr_79587_80932[(2)] = inst_79543);

(statearr_79587_80932[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (13))){
var inst_79444 = (state_79549[(17)]);
var inst_79453 = com.wsscode.async.async_cljs.promise__GT_chan(inst_79444);
var state_79549__$1 = state_79549;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79549__$1,(16),inst_79453);
} else {
if((state_val_79550 === (22))){
var inst_79501 = (state_79549[(13)]);
var inst_79508 = com.wsscode.async.async_cljs.promise_QMARK_(inst_79501);
var state_79549__$1 = state_79549;
if(cljs.core.truth_(inst_79508)){
var statearr_79588_80934 = state_79549__$1;
(statearr_79588_80934[(1)] = (25));

} else {
var statearr_79589_80935 = state_79549__$1;
(statearr_79589_80935[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (29))){
var inst_79501 = (state_79549[(13)]);
var state_79549__$1 = state_79549;
var statearr_79590_80936 = state_79549__$1;
(statearr_79590_80936[(2)] = inst_79501);

(statearr_79590_80936[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (6))){
var inst_79429 = (state_79549[(15)]);
var inst_79433 = (state_79549[(18)]);
var inst_79436 = (state_79549[(19)]);
var inst_79439 = (state_79549[(20)]);
var inst_79438 = (state_79549[(21)]);
var inst_79437 = (state_79549[(22)]);
var inst_79441 = (state_79549[(23)]);
var inst_79444 = (state_79549[(17)]);
var inst_79423 = [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.trace","style","com.wsscode.pathom.trace/style",496837297)];
var inst_79424 = [new cljs.core.Keyword(null,"fill","fill",883462889),new cljs.core.Keyword(null,"opacity","opacity",397153780)];
var inst_79425 = ["#e0e3a4","0.8"];
var inst_79426 = cljs.core.PersistentHashMap.fromArrays(inst_79424,inst_79425);
var inst_79427 = [new cljs.core.Keyword("com.wsscode.pathom.core","parallel-sequence-loop","com.wsscode.pathom.core/parallel-sequence-loop",1692546245),inst_79426];
var inst_79428 = cljs.core.PersistentHashMap.fromArrays(inst_79423,inst_79427);
var inst_79429__$1 = com.wsscode.pathom.trace.trace_enter.cljs$core$IFn$_invoke$arity$2(env,inst_79428);
var inst_79433__$1 = edn_query_language.core.query__GT_ast(query_SINGLEQUOTE_);
var inst_79434 = (function (){var trace_id__29442__auto__ = inst_79429__$1;
var ast = inst_79433__$1;
return (function (p1__79411_SHARP_){
return cljs.core.not(new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(p1__79411_SHARP_));
});
})();
var inst_79435 = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(inst_79433__$1);
var inst_79436__$1 = cljs.core.every_QMARK_(inst_79434,inst_79435);
var inst_79437__$1 = (function (){var trace_id__29442__auto__ = inst_79429__$1;
var ast = inst_79433__$1;
var check_ast_opt_QMARK_ = inst_79436__$1;
return (function com$wsscode$pathom$core$join_seq_parallel_$_join_item(env__$1,entity){
var or__4126__auto__ = ((check_ast_opt_QMARK_)?cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ent,p__79609){
var map__79610 = p__79609;
var map__79610__$1 = (((((!((map__79610 == null))))?(((((map__79610.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79610.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79610):map__79610);
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79610__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79610__$1,new cljs.core.Keyword(null,"params","params",710516235));
var temp__5733__auto__ = cljs.core.find(entity,key);
if(cljs.core.truth_(temp__5733__auto__)){
var vec__79612 = temp__5733__auto__;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79612,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79612,(1),null);
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(ent,cljs.core.get.cljs$core$IFn$_invoke$arity$3(params,new cljs.core.Keyword("pathom","as","pathom/as",-2134138450),key),v);
} else {
return cljs.core.reduced(null);
}
}),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast)):false);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(entity,env__$1);
}
});
})();
var inst_79438__$1 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),coll);
var inst_79439__$1 = coll;
var inst_79440 = cljs.core.seq(inst_79439__$1);
var inst_79441__$1 = cljs.core.first(inst_79440);
var inst_79442 = cljs.core.next(inst_79440);
var inst_79443 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(inst_79438__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,(0));
var inst_79444__$1 = inst_79437__$1(inst_79443,inst_79441__$1);
var inst_79445 = com.wsscode.async.async_cljs.chan_QMARK_(inst_79444__$1);
var state_79549__$1 = (function (){var statearr_79615 = state_79549;
(statearr_79615[(15)] = inst_79429__$1);

(statearr_79615[(18)] = inst_79433__$1);

(statearr_79615[(19)] = inst_79436__$1);

(statearr_79615[(22)] = inst_79437__$1);

(statearr_79615[(21)] = inst_79438__$1);

(statearr_79615[(20)] = inst_79439__$1);

(statearr_79615[(23)] = inst_79441__$1);

(statearr_79615[(24)] = inst_79442);

(statearr_79615[(17)] = inst_79444__$1);

return statearr_79615;
})();
if(inst_79445){
var statearr_79616_80937 = state_79549__$1;
(statearr_79616_80937[(1)] = (9));

} else {
var statearr_79617_80938 = state_79549__$1;
(statearr_79617_80938[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (28))){
var inst_79512 = (state_79549[(2)]);
var inst_79513 = com.wsscode.async.async_cljs.consumer_pair(inst_79512);
var state_79549__$1 = state_79549;
var statearr_79618_80940 = state_79549__$1;
(statearr_79618_80940[(2)] = inst_79513);

(statearr_79618_80940[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (25))){
var inst_79501 = (state_79549[(13)]);
var inst_79510 = com.wsscode.async.async_cljs.promise__GT_chan(inst_79501);
var state_79549__$1 = state_79549;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79549__$1,(28),inst_79510);
} else {
if((state_val_79550 === (34))){
var inst_79540 = (state_79549[(25)]);
var inst_79545 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__75196__auto__,inst_79540);
var state_79549__$1 = state_79549;
var statearr_79619_80943 = state_79549__$1;
(statearr_79619_80943[(2)] = inst_79545);

(statearr_79619_80943[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (17))){
var inst_79444 = (state_79549[(17)]);
var state_79549__$1 = state_79549;
var statearr_79620_80944 = state_79549__$1;
(statearr_79620_80944[(2)] = inst_79444);

(statearr_79620_80944[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (3))){
var inst_79540 = (state_79549[(25)]);
var inst_79540__$1 = (state_79549[(2)]);
var inst_79541 = (inst_79540__$1 == null);
var state_79549__$1 = (function (){var statearr_79621 = state_79549;
(statearr_79621[(25)] = inst_79540__$1);

return statearr_79621;
})();
if(cljs.core.truth_(inst_79541)){
var statearr_79622_80945 = state_79549__$1;
(statearr_79622_80945[(1)] = (33));

} else {
var statearr_79623_80946 = state_79549__$1;
(statearr_79623_80946[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (12))){
var inst_79448 = (state_79549[(2)]);
var inst_79449 = com.wsscode.async.async_cljs.throw_err(inst_79448);
var state_79549__$1 = state_79549;
var statearr_79624_80947 = state_79549__$1;
(statearr_79624_80947[(2)] = inst_79449);

(statearr_79624_80947[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (2))){
var _ = (function (){var statearr_79625 = state_79549;
(statearr_79625[(4)] = cljs.core.cons((5),(state_79549[(4)])));

return statearr_79625;
})();
var inst_79421 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword("com.wsscode.pathom.trace","trace*","com.wsscode.pathom.trace/trace*",-80191782));
var state_79549__$1 = state_79549;
if(cljs.core.truth_(inst_79421)){
var statearr_79626_80957 = state_79549__$1;
(statearr_79626_80957[(1)] = (6));

} else {
var statearr_79627_80958 = state_79549__$1;
(statearr_79627_80958[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (23))){
var inst_79499 = (state_79549[(14)]);
var inst_79494 = (state_79549[(11)]);
var inst_79493 = (state_79549[(8)]);
var inst_79498 = (state_79549[(12)]);
var inst_79490 = (state_79549[(7)]);
var inst_79495 = (state_79549[(10)]);
var inst_79496 = (state_79549[(9)]);
var inst_79522 = (state_79549[(2)]);
var inst_79523 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_79524 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_79525 = cljs.core.range.cljs$core$IFn$_invoke$arity$0();
var inst_79526 = cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.vector,inst_79499,inst_79525);
var inst_79527 = cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(inst_79523,inst_79526);
var inst_79528 = (function (){var from_chan = inst_79523;
var join_item = inst_79494;
var seq__79488 = inst_79499;
var first_res = inst_79522;
var check_ast_opt_QMARK_ = inst_79493;
var out_chan = inst_79524;
var first__79489 = inst_79498;
var ast = inst_79490;
var env__$1 = inst_79495;
var vec__79487 = inst_79496;
var head = inst_79498;
var tail = inst_79499;
return (function com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline(p__79628,res_ch){
var vec__79629 = p__79628;
var ent = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79629,(0),null);
var i = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79629,(1),null);
var c__50892__auto____$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79673){
var state_val_79674 = (state_79673[(1)]);
if((state_val_79674 === (7))){
var inst_79647 = (state_79673[(2)]);
var state_79673__$1 = state_79673;
var statearr_79675_80959 = state_79673__$1;
(statearr_79675_80959[(2)] = inst_79647);

(statearr_79675_80959[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (1))){
var inst_79634 = (state_79673[(7)]);
var inst_79633 = (i + (1));
var inst_79634__$1 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(env__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,inst_79633);
var inst_79636 = (inst_79634__$1 == null);
var inst_79637 = cljs.core.not(inst_79636);
var state_79673__$1 = (function (){var statearr_79676 = state_79673;
(statearr_79676[(7)] = inst_79634__$1);

return statearr_79676;
})();
if(inst_79637){
var statearr_79677_80960 = state_79673__$1;
(statearr_79677_80960[(1)] = (2));

} else {
var statearr_79678_80961 = state_79673__$1;
(statearr_79678_80961[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (4))){
var inst_79650 = (state_79673[(2)]);
var state_79673__$1 = state_79673;
if(cljs.core.truth_(inst_79650)){
var statearr_79679_80962 = state_79673__$1;
(statearr_79679_80962[(1)] = (8));

} else {
var statearr_79680_80963 = state_79673__$1;
(statearr_79680_80963[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (15))){
var inst_79670 = (state_79673[(2)]);
var inst_79671 = cljs.core.async.close_BANG_(res_ch);
var state_79673__$1 = (function (){var statearr_79681 = state_79673;
(statearr_79681[(8)] = inst_79670);

return statearr_79681;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_79673__$1,inst_79671);
} else {
if((state_val_79674 === (13))){
var inst_79668 = (state_79673[(2)]);
var state_79673__$1 = state_79673;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_79673__$1,(15),res_ch,inst_79668);
} else {
if((state_val_79674 === (6))){
var state_79673__$1 = state_79673;
var statearr_79682_80964 = state_79673__$1;
(statearr_79682_80964[(2)] = false);

(statearr_79682_80964[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (3))){
var state_79673__$1 = state_79673;
var statearr_79683_80966 = state_79673__$1;
(statearr_79683_80966[(2)] = false);

(statearr_79683_80966[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (12))){
var inst_79661 = (state_79673[(9)]);
var state_79673__$1 = state_79673;
var statearr_79684_80967 = state_79673__$1;
(statearr_79684_80967[(2)] = inst_79661);

(statearr_79684_80967[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (2))){
var inst_79634 = (state_79673[(7)]);
var inst_79639 = inst_79634.cljs$lang$protocol_mask$partition0$;
var inst_79640 = (inst_79639 & (64));
var inst_79641 = inst_79634.cljs$core$ISeq$;
var inst_79642 = (cljs.core.PROTOCOL_SENTINEL === inst_79641);
var inst_79643 = ((inst_79640) || (inst_79642));
var state_79673__$1 = state_79673;
if(cljs.core.truth_(inst_79643)){
var statearr_79685_80968 = state_79673__$1;
(statearr_79685_80968[(1)] = (5));

} else {
var statearr_79686_80969 = state_79673__$1;
(statearr_79686_80969[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (11))){
var inst_79661 = (state_79673[(9)]);
var state_79673__$1 = state_79673;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79673__$1,(14),inst_79661);
} else {
if((state_val_79674 === (9))){
var inst_79634 = (state_79673[(7)]);
var state_79673__$1 = state_79673;
var statearr_79687_80970 = state_79673__$1;
(statearr_79687_80970[(2)] = inst_79634);

(statearr_79687_80970[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (5))){
var state_79673__$1 = state_79673;
var statearr_79688_80971 = state_79673__$1;
(statearr_79688_80971[(2)] = true);

(statearr_79688_80971[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (14))){
var inst_79665 = (state_79673[(2)]);
var state_79673__$1 = state_79673;
var statearr_79689_80972 = state_79673__$1;
(statearr_79689_80972[(2)] = inst_79665);

(statearr_79689_80972[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (10))){
var inst_79661 = (state_79673[(9)]);
var inst_79655 = (state_79673[(2)]);
var inst_79656 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_79655,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
var inst_79657 = cljs.core.deref(entity_path_cache);
var inst_79658 = cljs.core.PersistentHashMap.EMPTY;
var inst_79659 = cljs.core.get.cljs$core$IFn$_invoke$arity$3(inst_79657,inst_79656,inst_79658);
var inst_79660 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_79659,ent], 0));
var inst_79661__$1 = (join_item.cljs$core$IFn$_invoke$arity$2 ? join_item.cljs$core$IFn$_invoke$arity$2(inst_79655,inst_79660) : join_item.call(null,inst_79655,inst_79660));
var inst_79662 = com.wsscode.async.async_cljs.chan_QMARK_(inst_79661__$1);
var state_79673__$1 = (function (){var statearr_79690 = state_79673;
(statearr_79690[(9)] = inst_79661__$1);

return statearr_79690;
})();
if(inst_79662){
var statearr_79691_80973 = state_79673__$1;
(statearr_79691_80973[(1)] = (11));

} else {
var statearr_79692_80974 = state_79673__$1;
(statearr_79692_80974[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79674 === (8))){
var inst_79634 = (state_79673[(7)]);
var inst_79652 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_79634);
var state_79673__$1 = state_79673;
var statearr_79693_80975 = state_79673__$1;
(statearr_79693_80975[(2)] = inst_79652);

(statearr_79693_80975[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____0 = (function (){
var statearr_79694 = [null,null,null,null,null,null,null,null,null,null];
(statearr_79694[(0)] = com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__);

(statearr_79694[(1)] = (1));

return statearr_79694;
});
var com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____1 = (function (state_79673){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79673);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79695){var ex__50860__auto__ = e79695;
var statearr_79696_80976 = state_79673;
(statearr_79696_80976[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79673[(4)]))){
var statearr_79697_80977 = state_79673;
(statearr_79697_80977[(1)] = cljs.core.first((state_79673[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__80978 = state_79673;
state_79673 = G__80978;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__ = function(state_79673){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____1.call(this,state_79673);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79698 = f__50893__auto__();
(statearr_79698[(6)] = c__50892__auto____$1);

return statearr_79698;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto____$1;
});
})();
var inst_79529 = cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((10),inst_79524,inst_79528,inst_79523);
var inst_79530 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_79531 = [inst_79522];
var inst_79532 = (new cljs.core.PersistentVector(null,1,(5),inst_79530,inst_79531,null));
var inst_79533 = cljs.core.async.into(inst_79532,inst_79524);
var state_79549__$1 = (function (){var statearr_79699 = state_79549;
(statearr_79699[(26)] = inst_79527);

(statearr_79699[(27)] = inst_79529);

return statearr_79699;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79549__$1,(32),inst_79533);
} else {
if((state_val_79550 === (35))){
var inst_79547 = (state_79549[(2)]);
var state_79549__$1 = state_79549;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79549__$1,inst_79547);
} else {
if((state_val_79550 === (19))){
var inst_79461 = (state_79549[(2)]);
var state_79549__$1 = state_79549;
var statearr_79700_80980 = state_79549__$1;
(statearr_79700_80980[(2)] = inst_79461);

(statearr_79700_80980[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (11))){
var inst_79442 = (state_79549[(24)]);
var inst_79437 = (state_79549[(22)]);
var inst_79439 = (state_79549[(20)]);
var inst_79436 = (state_79549[(19)]);
var inst_79433 = (state_79549[(18)]);
var inst_79438 = (state_79549[(21)]);
var inst_79429 = (state_79549[(15)]);
var inst_79441 = (state_79549[(23)]);
var inst_79465 = (state_79549[(2)]);
var inst_79466 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_79467 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((10));
var inst_79468 = cljs.core.range.cljs$core$IFn$_invoke$arity$0();
var inst_79469 = cljs.core.map.cljs$core$IFn$_invoke$arity$3(cljs.core.vector,inst_79442,inst_79468);
var inst_79470 = cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(inst_79466,inst_79469);
var inst_79471 = (function (){var from_chan = inst_79466;
var join_item = inst_79437;
var seq__79431 = inst_79442;
var first_res = inst_79465;
var vec__79430 = inst_79439;
var check_ast_opt_QMARK_ = inst_79436;
var out_chan = inst_79467;
var ast = inst_79433;
var env__$1 = inst_79438;
var trace_id__29442__auto__ = inst_79429;
var head = inst_79441;
var first__79432 = inst_79441;
var tail = inst_79442;
return (function com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline(p__79702,res_ch){
var vec__79703 = p__79702;
var ent = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79703,(0),null);
var i = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__79703,(1),null);
var c__50892__auto____$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_79747){
var state_val_79748 = (state_79747[(1)]);
if((state_val_79748 === (7))){
var inst_79721 = (state_79747[(2)]);
var state_79747__$1 = state_79747;
var statearr_79749_80981 = state_79747__$1;
(statearr_79749_80981[(2)] = inst_79721);

(statearr_79749_80981[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (1))){
var inst_79708 = (state_79747[(7)]);
var inst_79707 = (i + (1));
var inst_79708__$1 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(env__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,inst_79707);
var inst_79710 = (inst_79708__$1 == null);
var inst_79711 = cljs.core.not(inst_79710);
var state_79747__$1 = (function (){var statearr_79750 = state_79747;
(statearr_79750[(7)] = inst_79708__$1);

return statearr_79750;
})();
if(inst_79711){
var statearr_79751_80982 = state_79747__$1;
(statearr_79751_80982[(1)] = (2));

} else {
var statearr_79752_80983 = state_79747__$1;
(statearr_79752_80983[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (4))){
var inst_79724 = (state_79747[(2)]);
var state_79747__$1 = state_79747;
if(cljs.core.truth_(inst_79724)){
var statearr_79753_80984 = state_79747__$1;
(statearr_79753_80984[(1)] = (8));

} else {
var statearr_79754_80985 = state_79747__$1;
(statearr_79754_80985[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (15))){
var inst_79744 = (state_79747[(2)]);
var inst_79745 = cljs.core.async.close_BANG_(res_ch);
var state_79747__$1 = (function (){var statearr_79755 = state_79747;
(statearr_79755[(8)] = inst_79744);

return statearr_79755;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_79747__$1,inst_79745);
} else {
if((state_val_79748 === (13))){
var inst_79742 = (state_79747[(2)]);
var state_79747__$1 = state_79747;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_79747__$1,(15),res_ch,inst_79742);
} else {
if((state_val_79748 === (6))){
var state_79747__$1 = state_79747;
var statearr_79756_80986 = state_79747__$1;
(statearr_79756_80986[(2)] = false);

(statearr_79756_80986[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (3))){
var state_79747__$1 = state_79747;
var statearr_79757_80992 = state_79747__$1;
(statearr_79757_80992[(2)] = false);

(statearr_79757_80992[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (12))){
var inst_79735 = (state_79747[(9)]);
var state_79747__$1 = state_79747;
var statearr_79758_80993 = state_79747__$1;
(statearr_79758_80993[(2)] = inst_79735);

(statearr_79758_80993[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (2))){
var inst_79708 = (state_79747[(7)]);
var inst_79713 = inst_79708.cljs$lang$protocol_mask$partition0$;
var inst_79714 = (inst_79713 & (64));
var inst_79715 = inst_79708.cljs$core$ISeq$;
var inst_79716 = (cljs.core.PROTOCOL_SENTINEL === inst_79715);
var inst_79717 = ((inst_79714) || (inst_79716));
var state_79747__$1 = state_79747;
if(cljs.core.truth_(inst_79717)){
var statearr_79759_80994 = state_79747__$1;
(statearr_79759_80994[(1)] = (5));

} else {
var statearr_79760_80995 = state_79747__$1;
(statearr_79760_80995[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (11))){
var inst_79735 = (state_79747[(9)]);
var state_79747__$1 = state_79747;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79747__$1,(14),inst_79735);
} else {
if((state_val_79748 === (9))){
var inst_79708 = (state_79747[(7)]);
var state_79747__$1 = state_79747;
var statearr_79761_80996 = state_79747__$1;
(statearr_79761_80996[(2)] = inst_79708);

(statearr_79761_80996[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (5))){
var state_79747__$1 = state_79747;
var statearr_79762_81000 = state_79747__$1;
(statearr_79762_81000[(2)] = true);

(statearr_79762_81000[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (14))){
var inst_79739 = (state_79747[(2)]);
var state_79747__$1 = state_79747;
var statearr_79763_81001 = state_79747__$1;
(statearr_79763_81001[(2)] = inst_79739);

(statearr_79763_81001[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (10))){
var inst_79735 = (state_79747[(9)]);
var inst_79729 = (state_79747[(2)]);
var inst_79730 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_79729,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
var inst_79731 = cljs.core.deref(entity_path_cache);
var inst_79732 = cljs.core.PersistentHashMap.EMPTY;
var inst_79733 = cljs.core.get.cljs$core$IFn$_invoke$arity$3(inst_79731,inst_79730,inst_79732);
var inst_79734 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([inst_79733,ent], 0));
var inst_79735__$1 = (join_item.cljs$core$IFn$_invoke$arity$2 ? join_item.cljs$core$IFn$_invoke$arity$2(inst_79729,inst_79734) : join_item.call(null,inst_79729,inst_79734));
var inst_79736 = com.wsscode.async.async_cljs.chan_QMARK_(inst_79735__$1);
var state_79747__$1 = (function (){var statearr_79764 = state_79747;
(statearr_79764[(9)] = inst_79735__$1);

return statearr_79764;
})();
if(inst_79736){
var statearr_79765_81002 = state_79747__$1;
(statearr_79765_81002[(1)] = (11));

} else {
var statearr_79766_81003 = state_79747__$1;
(statearr_79766_81003[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79748 === (8))){
var inst_79708 = (state_79747[(7)]);
var inst_79726 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,inst_79708);
var state_79747__$1 = state_79747;
var statearr_79767_81004 = state_79747__$1;
(statearr_79767_81004[(2)] = inst_79726);

(statearr_79767_81004[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____0 = (function (){
var statearr_79768 = [null,null,null,null,null,null,null,null,null,null];
(statearr_79768[(0)] = com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__);

(statearr_79768[(1)] = (1));

return statearr_79768;
});
var com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____1 = (function (state_79747){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79747);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79769){var ex__50860__auto__ = e79769;
var statearr_79770_81005 = state_79747;
(statearr_79770_81005[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79747[(4)]))){
var statearr_79771_81006 = state_79747;
(statearr_79771_81006[(1)] = cljs.core.first((state_79747[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81007 = state_79747;
state_79747 = G__81007;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__ = function(state_79747){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____1.call(this,state_79747);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$join_seq_parallel_$_join_seq_pipeline_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79772 = f__50893__auto__();
(statearr_79772[(6)] = c__50892__auto____$1);

return statearr_79772;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto____$1;
});
})();
var inst_79472 = cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((10),inst_79467,inst_79471,inst_79466);
var inst_79473 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_79474 = [inst_79465];
var inst_79475 = (new cljs.core.PersistentVector(null,1,(5),inst_79473,inst_79474,null));
var inst_79476 = cljs.core.async.into(inst_79475,inst_79467);
var state_79549__$1 = (function (){var statearr_79773 = state_79549;
(statearr_79773[(28)] = inst_79470);

(statearr_79773[(29)] = inst_79472);

return statearr_79773;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79549__$1,(20),inst_79476);
} else {
if((state_val_79550 === (9))){
var inst_79444 = (state_79549[(17)]);
var state_79549__$1 = state_79549;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79549__$1,(12),inst_79444);
} else {
if((state_val_79550 === (5))){
var _ = (function (){var statearr_79774 = state_79549;
(statearr_79774[(4)] = cljs.core.rest((state_79549[(4)])));

return statearr_79774;
})();
var state_79549__$1 = state_79549;
var ex79701 = (state_79549__$1[(2)]);
var statearr_79775_81008 = state_79549__$1;
(statearr_79775_81008[(5)] = ex79701);


var statearr_79776_81009 = state_79549__$1;
(statearr_79776_81009[(1)] = (4));

(statearr_79776_81009[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (14))){
var state_79549__$1 = state_79549;
var statearr_79777_81011 = state_79549__$1;
(statearr_79777_81011[(1)] = (17));



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (26))){
var state_79549__$1 = state_79549;
var statearr_79779_81012 = state_79549__$1;
(statearr_79779_81012[(1)] = (29));



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (16))){
var inst_79455 = (state_79549[(2)]);
var inst_79456 = com.wsscode.async.async_cljs.consumer_pair(inst_79455);
var state_79549__$1 = state_79549;
var statearr_79781_81013 = state_79549__$1;
(statearr_79781_81013[(2)] = inst_79456);

(statearr_79781_81013[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (30))){
var state_79549__$1 = state_79549;
var statearr_79782_81014 = state_79549__$1;
(statearr_79782_81014[(2)] = null);

(statearr_79782_81014[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (10))){
var inst_79444 = (state_79549[(17)]);
var inst_79451 = com.wsscode.async.async_cljs.promise_QMARK_(inst_79444);
var state_79549__$1 = state_79549;
if(cljs.core.truth_(inst_79451)){
var statearr_79783_81015 = state_79549__$1;
(statearr_79783_81015[(1)] = (13));

} else {
var statearr_79784_81016 = state_79549__$1;
(statearr_79784_81016[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (18))){
var state_79549__$1 = state_79549;
var statearr_79785_81017 = state_79549__$1;
(statearr_79785_81017[(2)] = null);

(statearr_79785_81017[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79550 === (8))){
var inst_79537 = (state_79549[(2)]);
var _ = (function (){var statearr_79786 = state_79549;
(statearr_79786[(4)] = cljs.core.rest((state_79549[(4)])));

return statearr_79786;
})();
var state_79549__$1 = state_79549;
var statearr_79787_81018 = state_79549__$1;
(statearr_79787_81018[(2)] = inst_79537);

(statearr_79787_81018[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto____0 = (function (){
var statearr_79788 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_79788[(0)] = com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto__);

(statearr_79788[(1)] = (1));

return statearr_79788;
});
var com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto____1 = (function (state_79549){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79549);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79789){var ex__50860__auto__ = e79789;
var statearr_79790_81019 = state_79549;
(statearr_79790_81019[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79549[(4)]))){
var statearr_79791_81020 = state_79549;
(statearr_79791_81020[(1)] = cljs.core.first((state_79549[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81021 = state_79549;
state_79549 = G__81021;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto__ = function(state_79549){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto____1.call(this,state_79549);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$join_seq_parallel_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_79792 = f__50893__auto__();
(statearr_79792[(6)] = c__50892__auto___80914);

return statearr_79792;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));


return ch__75196__auto__;
} else {
return cljs.core.PersistentVector.EMPTY;
}
});
/**
 * Runs the current subquery against the items of the given collection.
 */
com.wsscode.pathom.core.join_seq = (function com$wsscode$pathom$core$join_seq(p__79793,coll){
var map__79794 = p__79793;
var map__79794__$1 = (((((!((map__79794 == null))))?(((((map__79794.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79794.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79794):map__79794);
var env = map__79794__$1;
var parallel_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79794__$1,new cljs.core.Keyword("com.wsscode.pathom.parser","parallel?","com.wsscode.pathom.parser/parallel?",2071817193));
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","join-seq","com.wsscode.pathom.core/join-seq",523699901),new cljs.core.Keyword("com.wsscode.pathom.core","seq-count","com.wsscode.pathom.core/seq-count",-1671473836),cljs.core.count(coll)], null));

if(cljs.core.truth_(parallel_QMARK_)){
return com.wsscode.pathom.core.join_seq_parallel(env,coll);
} else {
var join_item = (function com$wsscode$pathom$core$join_seq_$_join_item(ent,out){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(ent,cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),coll),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,cljs.core.count(out)));
});
var out = cljs.core.PersistentVector.EMPTY;
var G__79799 = coll;
var vec__79800 = G__79799;
var seq__79801 = cljs.core.seq(vec__79800);
var first__79802 = cljs.core.first(seq__79801);
var seq__79801__$1 = cljs.core.next(seq__79801);
var ent = first__79802;
var tail = seq__79801__$1;
var out__$1 = out;
var G__79799__$1 = G__79799;
while(true){
var out__$2 = out__$1;
var vec__79892 = G__79799__$1;
var seq__79893 = cljs.core.seq(vec__79892);
var first__79894 = cljs.core.first(seq__79893);
var seq__79893__$1 = cljs.core.next(seq__79893);
var ent__$1 = first__79894;
var tail__$1 = seq__79893__$1;
if(cljs.core.truth_(ent__$1)){
var res = join_item(ent__$1,out__$2);
if(com.wsscode.async.async_cljs.chan_QMARK_(res)){
var ch__75196__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__50892__auto___81032 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (out__$1,G__79799__$1,c__50892__auto___81032,ch__75196__auto__,res,out__$2,vec__79892,seq__79893,first__79894,seq__79893__$1,ent__$1,tail__$1,out,G__79799,vec__79800,seq__79801,first__79802,seq__79801__$1,ent,tail,map__79794,map__79794__$1,env,parallel_QMARK_){
return (function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = ((function (out__$1,G__79799__$1,c__50892__auto___81032,ch__75196__auto__,res,out__$2,vec__79892,seq__79893,first__79894,seq__79893__$1,ent__$1,tail__$1,out,G__79799,vec__79800,seq__79801,first__79802,seq__79801__$1,ent,tail,map__79794,map__79794__$1,env,parallel_QMARK_){
return (function (state_79950){
var state_val_79951 = (state_79950[(1)]);
if((state_val_79951 === (7))){
var inst_79919 = (state_79950[(7)]);
var inst_79925 = (state_79950[(8)]);
var inst_79924 = cljs.core.seq(inst_79919);
var inst_79925__$1 = cljs.core.first(inst_79924);
var inst_79926 = cljs.core.next(inst_79924);
var state_79950__$1 = (function (){var statearr_79952 = state_79950;
(statearr_79952[(8)] = inst_79925__$1);

(statearr_79952[(9)] = inst_79926);

return statearr_79952;
})();
if(cljs.core.truth_(inst_79925__$1)){
var statearr_79953_81033 = state_79950__$1;
(statearr_79953_81033[(1)] = (9));

} else {
var statearr_79954_81034 = state_79950__$1;
(statearr_79954_81034[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (1))){
var state_79950__$1 = state_79950;
var statearr_79955_81035 = state_79950__$1;
(statearr_79955_81035[(2)] = null);

(statearr_79955_81035[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (4))){
var inst_79895 = (state_79950[(2)]);
var state_79950__$1 = state_79950;
var statearr_79956_81036 = state_79950__$1;
(statearr_79956_81036[(2)] = inst_79895);

(statearr_79956_81036[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (15))){
var inst_79948 = (state_79950[(2)]);
var state_79950__$1 = state_79950;
return cljs.core.async.impl.ioc_helpers.return_chan(state_79950__$1,inst_79948);
} else {
if((state_val_79951 === (13))){
var inst_79944 = cljs.core.async.close_BANG_(ch__75196__auto__);
var state_79950__$1 = state_79950;
var statearr_79957_81037 = state_79950__$1;
(statearr_79957_81037[(2)] = inst_79944);

(statearr_79957_81037[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (6))){
var inst_79908 = (state_79950[(10)]);
var inst_79910 = (state_79950[(2)]);
var inst_79911 = com.wsscode.async.async_cljs.throw_err(inst_79910);
var inst_79912 = [inst_79911];
var inst_79913 = (new cljs.core.PersistentVector(null,1,(5),inst_79908,inst_79912,null));
var inst_79914 = tail__$1;
var inst_79915 = cljs.core.seq(inst_79914);
var inst_79916 = cljs.core.first(inst_79915);
var inst_79917 = cljs.core.next(inst_79915);
var inst_79918 = inst_79913;
var inst_79919 = inst_79914;
var state_79950__$1 = (function (){var statearr_79958 = state_79950;
(statearr_79958[(11)] = inst_79916);

(statearr_79958[(12)] = inst_79917);

(statearr_79958[(13)] = inst_79918);

(statearr_79958[(7)] = inst_79919);

return statearr_79958;
})();
var statearr_79959_81038 = state_79950__$1;
(statearr_79959_81038[(2)] = null);

(statearr_79959_81038[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (3))){
var inst_79941 = (state_79950[(14)]);
var inst_79941__$1 = (state_79950[(2)]);
var inst_79942 = (inst_79941__$1 == null);
var state_79950__$1 = (function (){var statearr_79960 = state_79950;
(statearr_79960[(14)] = inst_79941__$1);

return statearr_79960;
})();
if(cljs.core.truth_(inst_79942)){
var statearr_79961_81039 = state_79950__$1;
(statearr_79961_81039[(1)] = (13));

} else {
var statearr_79962_81040 = state_79950__$1;
(statearr_79962_81040[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (12))){
var inst_79918 = (state_79950[(13)]);
var inst_79926 = (state_79950[(9)]);
var inst_79930 = (state_79950[(2)]);
var inst_79931 = com.wsscode.async.async_cljs.throw_err(inst_79930);
var inst_79932 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(inst_79918,inst_79931);
var inst_79918__$1 = inst_79932;
var inst_79919 = inst_79926;
var state_79950__$1 = (function (){var statearr_79963 = state_79950;
(statearr_79963[(13)] = inst_79918__$1);

(statearr_79963[(7)] = inst_79919);

return statearr_79963;
})();
var statearr_79964_81046 = state_79950__$1;
(statearr_79964_81046[(2)] = null);

(statearr_79964_81046[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (2))){
var _ = (function (){var statearr_79965 = state_79950;
(statearr_79965[(4)] = cljs.core.cons((5),(state_79950[(4)])));

return statearr_79965;
})();
var inst_79908 = cljs.core.PersistentVector.EMPTY_NODE;
var state_79950__$1 = (function (){var statearr_79966 = state_79950;
(statearr_79966[(10)] = inst_79908);

return statearr_79966;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79950__$1,(6),res);
} else {
if((state_val_79951 === (11))){
var inst_79936 = (state_79950[(2)]);
var state_79950__$1 = state_79950;
var statearr_79968_81047 = state_79950__$1;
(statearr_79968_81047[(2)] = inst_79936);

(statearr_79968_81047[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (9))){
var inst_79925 = (state_79950[(8)]);
var inst_79918 = (state_79950[(13)]);
var inst_79928 = join_item(inst_79925,inst_79918);
var state_79950__$1 = state_79950;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_79950__$1,(12),inst_79928);
} else {
if((state_val_79951 === (5))){
var _ = (function (){var statearr_79969 = state_79950;
(statearr_79969[(4)] = cljs.core.rest((state_79950[(4)])));

return statearr_79969;
})();
var state_79950__$1 = state_79950;
var ex79967 = (state_79950__$1[(2)]);
var statearr_79970_81048 = state_79950__$1;
(statearr_79970_81048[(5)] = ex79967);


var statearr_79971_81049 = state_79950__$1;
(statearr_79971_81049[(1)] = (4));

(statearr_79971_81049[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (14))){
var inst_79941 = (state_79950[(14)]);
var inst_79946 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__75196__auto__,inst_79941);
var state_79950__$1 = state_79950;
var statearr_79972_81053 = state_79950__$1;
(statearr_79972_81053[(2)] = inst_79946);

(statearr_79972_81053[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (10))){
var inst_79918 = (state_79950[(13)]);
var state_79950__$1 = state_79950;
var statearr_79973_81054 = state_79950__$1;
(statearr_79973_81054[(2)] = inst_79918);

(statearr_79973_81054[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_79951 === (8))){
var inst_79938 = (state_79950[(2)]);
var _ = (function (){var statearr_79974 = state_79950;
(statearr_79974[(4)] = cljs.core.rest((state_79950[(4)])));

return statearr_79974;
})();
var state_79950__$1 = state_79950;
var statearr_79975_81055 = state_79950__$1;
(statearr_79975_81055[(2)] = inst_79938);

(statearr_79975_81055[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(out__$1,G__79799__$1,c__50892__auto___81032,ch__75196__auto__,res,out__$2,vec__79892,seq__79893,first__79894,seq__79893__$1,ent__$1,tail__$1,out,G__79799,vec__79800,seq__79801,first__79802,seq__79801__$1,ent,tail,map__79794,map__79794__$1,env,parallel_QMARK_))
;
return ((function (out__$1,G__79799__$1,switch__50856__auto__,c__50892__auto___81032,ch__75196__auto__,res,out__$2,vec__79892,seq__79893,first__79894,seq__79893__$1,ent__$1,tail__$1,out,G__79799,vec__79800,seq__79801,first__79802,seq__79801__$1,ent,tail,map__79794,map__79794__$1,env,parallel_QMARK_){
return (function() {
var com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto____0 = (function (){
var statearr_79976 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_79976[(0)] = com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto__);

(statearr_79976[(1)] = (1));

return statearr_79976;
});
var com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto____1 = (function (state_79950){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_79950);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e79977){var ex__50860__auto__ = e79977;
var statearr_79978_81056 = state_79950;
(statearr_79978_81056[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_79950[(4)]))){
var statearr_79979_81057 = state_79950;
(statearr_79979_81057[(1)] = cljs.core.first((state_79950[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81063 = state_79950;
state_79950 = G__81063;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto__ = function(state_79950){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto____1.call(this,state_79950);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$join_seq_$_state_machine__50857__auto__;
})()
;})(out__$1,G__79799__$1,switch__50856__auto__,c__50892__auto___81032,ch__75196__auto__,res,out__$2,vec__79892,seq__79893,first__79894,seq__79893__$1,ent__$1,tail__$1,out,G__79799,vec__79800,seq__79801,first__79802,seq__79801__$1,ent,tail,map__79794,map__79794__$1,env,parallel_QMARK_))
})();
var state__50894__auto__ = (function (){var statearr_79980 = f__50893__auto__();
(statearr_79980[(6)] = c__50892__auto___81032);

return statearr_79980;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
});})(out__$1,G__79799__$1,c__50892__auto___81032,ch__75196__auto__,res,out__$2,vec__79892,seq__79893,first__79894,seq__79893__$1,ent__$1,tail__$1,out,G__79799,vec__79800,seq__79801,first__79802,seq__79801__$1,ent,tail,map__79794,map__79794__$1,env,parallel_QMARK_))
);


return ch__75196__auto__;
} else {
var G__81064 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(out__$2,res);
var G__81065 = tail__$1;
out__$1 = G__81064;
G__79799__$1 = G__81065;
continue;
}
} else {
return out__$2;
}
break;
}
}
});
cljs.spec.alpha.def_impl(new cljs.core.Symbol("com.wsscode.pathom.core","join-map","com.wsscode.pathom.core/join-map",145426168,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","fspec","cljs.spec.alpha/fspec",-1289128341,null),new cljs.core.Keyword(null,"args","args",1315556576),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),new cljs.core.Keyword(null,"ret","ret",-468222814),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null))),cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235),new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__79985){
return cljs.core.map_QMARK_(G__79985);
})], null),(function (G__79985){
return cljs.core.map_QMARK_(G__79985);
}),cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,null,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null)))], null),null])),cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_,com.wsscode.async.async_cljs.chan_QMARK_], null),null),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),null,null,null));


/**
 * Runs the current subquery against the items of the given collection.
 * @type {function(!cljs.core.IMap, !cljs.core.IMap): *}
 */
com.wsscode.pathom.core.join_map = (function com$wsscode$pathom$core$join_map(env,m){
var map__79986 = cljs.spec.alpha.fspec_impl(cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.cat_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"env","env",-1815813235),new cljs.core.Keyword(null,"m","m",1632677161)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.alpha.map_spec_impl(cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"req-un","req-un",1074571008),new cljs.core.Keyword(null,"opt-un","opt-un",883442496),new cljs.core.Keyword(null,"gfn","gfn",791517474),new cljs.core.Keyword(null,"pred-exprs","pred-exprs",1792271395),new cljs.core.Keyword(null,"keys-pred","keys-pred",858984739),new cljs.core.Keyword(null,"opt-keys","opt-keys",1262688261),new cljs.core.Keyword(null,"req-specs","req-specs",553962313),new cljs.core.Keyword(null,"req","req",-326448303),new cljs.core.Keyword(null,"req-keys","req-keys",514319221),new cljs.core.Keyword(null,"opt-specs","opt-specs",-384905450),new cljs.core.Keyword(null,"pred-forms","pred-forms",172611832),new cljs.core.Keyword(null,"opt","opt",-794706369)],[null,null,null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (G__79987){
return cljs.core.map_QMARK_(G__79987);
})], null),(function (G__79987){
return cljs.core.map_QMARK_(G__79987);
}),cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,null,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentVector.EMPTY,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.core","fn","cljs.core/fn",-1065745098,null),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"%","%",-950237169,null)], null),cljs.core.list(new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol(null,"%","%",-950237169,null)))], null),null])),cljs.core.map_QMARK_], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)], null)),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","cat","cljs.spec.alpha/cat",-1471398329,null),new cljs.core.Keyword(null,"env","env",-1815813235),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","keys","cljs.spec.alpha/keys",1109346032,null)),new cljs.core.Keyword(null,"m","m",1632677161),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null)),cljs.spec.alpha.spec_impl.cljs$core$IFn$_invoke$arity$4(cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),cljs.spec.alpha.or_spec_impl(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map_QMARK_,com.wsscode.async.async_cljs.chan_QMARK_], null),null),null,null),cljs.core.list(new cljs.core.Symbol("cljs.spec.alpha","or","cljs.spec.alpha/or",-831679639,null),new cljs.core.Keyword(null,"map","map",1371690461),new cljs.core.Symbol("cljs.core","map?","cljs.core/map?",-1390345523,null),new cljs.core.Keyword(null,"map-chan","map-chan",-205889131),new cljs.core.Symbol("com.wsscode.async.async-cljs","chan?","com.wsscode.async.async-cljs/chan?",-1214839656,null)),null,null,null);
var map__79986__$1 = (((((!((map__79986 == null))))?(((((map__79986.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__79986.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__79986):map__79986);
var argspec79981 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79986__$1,new cljs.core.Keyword(null,"args","args",1315556576));
var retspec79982 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__79986__$1,new cljs.core.Keyword(null,"ret","ret",-468222814));
if(cljs.core.truth_(argspec79981)){
com.fulcrologic.guardrails.core.run_check(true,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:600 join-map's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),argspec79981,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [env,m], null));
} else {
}

var f79984 = (function (env__$1,m__$1){
com.wsscode.pathom.trace.trace(env__$1,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","join-map","com.wsscode.pathom.core/join-map",-1495105359),new cljs.core.Keyword("com.wsscode.pathom.core","seq-count","com.wsscode.pathom.core/seq-count",-1671473836),cljs.core.count(m__$1)], null));

var join_item = (function com$wsscode$pathom$core$join_map_$_join_item(k,ent){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(ent,cljs.core.update.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env__$1,new cljs.core.Keyword("com.wsscode.pathom.core","processing-sequence","com.wsscode.pathom.core/processing-sequence",1929448637),m__$1),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.conj,k));
});
var out = cljs.core.PersistentArrayMap.EMPTY;
var G__79992 = m__$1;
var vec__79993 = G__79992;
var seq__79994 = cljs.core.seq(vec__79993);
var first__79995 = cljs.core.first(seq__79994);
var seq__79994__$1 = cljs.core.next(seq__79994);
var pair = first__79995;
var tail = seq__79994__$1;
var out__$1 = out;
var G__79992__$1 = G__79992;
while(true){
var out__$2 = out__$1;
var vec__80094 = G__79992__$1;
var seq__80095 = cljs.core.seq(vec__80094);
var first__80096 = cljs.core.first(seq__80095);
var seq__80095__$1 = cljs.core.next(seq__80095);
var pair__$1 = first__80096;
var tail__$1 = seq__80095__$1;
if(cljs.core.truth_(pair__$1)){
var vec__80097 = pair__$1;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80097,(0),null);
var ent = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80097,(1),null);
var res = join_item(k,ent);
if(com.wsscode.async.async_cljs.chan_QMARK_(res)){
var ch__75196__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__50892__auto___81067 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (out__$1,G__79992__$1,c__50892__auto___81067,ch__75196__auto__,vec__80097,k,ent,res,out__$2,vec__80094,seq__80095,first__80096,seq__80095__$1,pair__$1,tail__$1,out,G__79992,vec__79993,seq__79994,first__79995,seq__79994__$1,pair,tail,map__79986,map__79986__$1,argspec79981,retspec79982){
return (function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = ((function (out__$1,G__79992__$1,c__50892__auto___81067,ch__75196__auto__,vec__80097,k,ent,res,out__$2,vec__80094,seq__80095,first__80096,seq__80095__$1,pair__$1,tail__$1,out,G__79992,vec__79993,seq__79994,first__79995,seq__79994__$1,pair,tail,map__79986,map__79986__$1,argspec79981,retspec79982){
return (function (state_80160){
var state_val_80161 = (state_80160[(1)]);
if((state_val_80161 === (7))){
var inst_80124 = (state_80160[(7)]);
var inst_80130 = (state_80160[(8)]);
var inst_80129 = cljs.core.seq(inst_80124);
var inst_80130__$1 = cljs.core.first(inst_80129);
var inst_80131 = cljs.core.next(inst_80129);
var state_80160__$1 = (function (){var statearr_80162 = state_80160;
(statearr_80162[(8)] = inst_80130__$1);

(statearr_80162[(9)] = inst_80131);

return statearr_80162;
})();
if(cljs.core.truth_(inst_80130__$1)){
var statearr_80163_81068 = state_80160__$1;
(statearr_80163_81068[(1)] = (9));

} else {
var statearr_80164_81069 = state_80160__$1;
(statearr_80164_81069[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (1))){
var state_80160__$1 = state_80160;
var statearr_80165_81070 = state_80160__$1;
(statearr_80165_81070[(2)] = null);

(statearr_80165_81070[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (4))){
var inst_80100 = (state_80160[(2)]);
var state_80160__$1 = state_80160;
var statearr_80166_81071 = state_80160__$1;
(statearr_80166_81071[(2)] = inst_80100);

(statearr_80166_81071[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (15))){
var inst_80158 = (state_80160[(2)]);
var state_80160__$1 = state_80160;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80160__$1,inst_80158);
} else {
if((state_val_80161 === (13))){
var inst_80154 = cljs.core.async.close_BANG_(ch__75196__auto__);
var state_80160__$1 = state_80160;
var statearr_80167_81077 = state_80160__$1;
(statearr_80167_81077[(2)] = inst_80154);

(statearr_80167_81077[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (6))){
var inst_80113 = (state_80160[(10)]);
var inst_80115 = (state_80160[(2)]);
var inst_80116 = com.wsscode.async.async_cljs.throw_err(inst_80115);
var inst_80117 = [inst_80116];
var inst_80118 = cljs.core.PersistentHashMap.fromArrays(inst_80113,inst_80117);
var inst_80119 = tail__$1;
var inst_80120 = cljs.core.seq(inst_80119);
var inst_80121 = cljs.core.first(inst_80120);
var inst_80122 = cljs.core.next(inst_80120);
var inst_80123 = inst_80118;
var inst_80124 = inst_80119;
var state_80160__$1 = (function (){var statearr_80168 = state_80160;
(statearr_80168[(11)] = inst_80121);

(statearr_80168[(12)] = inst_80122);

(statearr_80168[(13)] = inst_80123);

(statearr_80168[(7)] = inst_80124);

return statearr_80168;
})();
var statearr_80169_81078 = state_80160__$1;
(statearr_80169_81078[(2)] = null);

(statearr_80169_81078[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (3))){
var inst_80151 = (state_80160[(14)]);
var inst_80151__$1 = (state_80160[(2)]);
var inst_80152 = (inst_80151__$1 == null);
var state_80160__$1 = (function (){var statearr_80170 = state_80160;
(statearr_80170[(14)] = inst_80151__$1);

return statearr_80170;
})();
if(cljs.core.truth_(inst_80152)){
var statearr_80171_81079 = state_80160__$1;
(statearr_80171_81079[(1)] = (13));

} else {
var statearr_80172_81080 = state_80160__$1;
(statearr_80172_81080[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (12))){
var inst_80123 = (state_80160[(13)]);
var inst_80136 = (state_80160[(15)]);
var inst_80131 = (state_80160[(9)]);
var inst_80140 = (state_80160[(2)]);
var inst_80141 = com.wsscode.async.async_cljs.throw_err(inst_80140);
var inst_80142 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(inst_80123,inst_80136,inst_80141);
var inst_80123__$1 = inst_80142;
var inst_80124 = inst_80131;
var state_80160__$1 = (function (){var statearr_80173 = state_80160;
(statearr_80173[(13)] = inst_80123__$1);

(statearr_80173[(7)] = inst_80124);

return statearr_80173;
})();
var statearr_80174_81081 = state_80160__$1;
(statearr_80174_81081[(2)] = null);

(statearr_80174_81081[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (2))){
var _ = (function (){var statearr_80175 = state_80160;
(statearr_80175[(4)] = cljs.core.cons((5),(state_80160[(4)])));

return statearr_80175;
})();
var inst_80113 = [k];
var state_80160__$1 = (function (){var statearr_80176 = state_80160;
(statearr_80176[(10)] = inst_80113);

return statearr_80176;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80160__$1,(6),res);
} else {
if((state_val_80161 === (11))){
var inst_80146 = (state_80160[(2)]);
var state_80160__$1 = state_80160;
var statearr_80178_81083 = state_80160__$1;
(statearr_80178_81083[(2)] = inst_80146);

(statearr_80178_81083[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (9))){
var inst_80130 = (state_80160[(8)]);
var inst_80136 = (state_80160[(15)]);
var inst_80136__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_80130,(0),null);
var inst_80137 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_80130,(1),null);
var inst_80138 = join_item(inst_80136__$1,inst_80137);
var state_80160__$1 = (function (){var statearr_80179 = state_80160;
(statearr_80179[(15)] = inst_80136__$1);

return statearr_80179;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80160__$1,(12),inst_80138);
} else {
if((state_val_80161 === (5))){
var _ = (function (){var statearr_80180 = state_80160;
(statearr_80180[(4)] = cljs.core.rest((state_80160[(4)])));

return statearr_80180;
})();
var state_80160__$1 = state_80160;
var ex80177 = (state_80160__$1[(2)]);
var statearr_80181_81087 = state_80160__$1;
(statearr_80181_81087[(5)] = ex80177);


var statearr_80182_81088 = state_80160__$1;
(statearr_80182_81088[(1)] = (4));

(statearr_80182_81088[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (14))){
var inst_80151 = (state_80160[(14)]);
var inst_80156 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__75196__auto__,inst_80151);
var state_80160__$1 = state_80160;
var statearr_80183_81090 = state_80160__$1;
(statearr_80183_81090[(2)] = inst_80156);

(statearr_80183_81090[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (10))){
var inst_80123 = (state_80160[(13)]);
var state_80160__$1 = state_80160;
var statearr_80184_81097 = state_80160__$1;
(statearr_80184_81097[(2)] = inst_80123);

(statearr_80184_81097[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80161 === (8))){
var inst_80148 = (state_80160[(2)]);
var _ = (function (){var statearr_80185 = state_80160;
(statearr_80185[(4)] = cljs.core.rest((state_80160[(4)])));

return statearr_80185;
})();
var state_80160__$1 = state_80160;
var statearr_80186_81098 = state_80160__$1;
(statearr_80186_81098[(2)] = inst_80148);

(statearr_80186_81098[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(out__$1,G__79992__$1,c__50892__auto___81067,ch__75196__auto__,vec__80097,k,ent,res,out__$2,vec__80094,seq__80095,first__80096,seq__80095__$1,pair__$1,tail__$1,out,G__79992,vec__79993,seq__79994,first__79995,seq__79994__$1,pair,tail,map__79986,map__79986__$1,argspec79981,retspec79982))
;
return ((function (out__$1,G__79992__$1,switch__50856__auto__,c__50892__auto___81067,ch__75196__auto__,vec__80097,k,ent,res,out__$2,vec__80094,seq__80095,first__80096,seq__80095__$1,pair__$1,tail__$1,out,G__79992,vec__79993,seq__79994,first__79995,seq__79994__$1,pair,tail,map__79986,map__79986__$1,argspec79981,retspec79982){
return (function() {
var com$wsscode$pathom$core$join_map_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$join_map_$_state_machine__50857__auto____0 = (function (){
var statearr_80187 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_80187[(0)] = com$wsscode$pathom$core$join_map_$_state_machine__50857__auto__);

(statearr_80187[(1)] = (1));

return statearr_80187;
});
var com$wsscode$pathom$core$join_map_$_state_machine__50857__auto____1 = (function (state_80160){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80160);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80188){var ex__50860__auto__ = e80188;
var statearr_80189_81099 = state_80160;
(statearr_80189_81099[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80160[(4)]))){
var statearr_80190_81100 = state_80160;
(statearr_80190_81100[(1)] = cljs.core.first((state_80160[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81101 = state_80160;
state_80160 = G__81101;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$join_map_$_state_machine__50857__auto__ = function(state_80160){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$join_map_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$join_map_$_state_machine__50857__auto____1.call(this,state_80160);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$join_map_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$join_map_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$join_map_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$join_map_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$join_map_$_state_machine__50857__auto__;
})()
;})(out__$1,G__79992__$1,switch__50856__auto__,c__50892__auto___81067,ch__75196__auto__,vec__80097,k,ent,res,out__$2,vec__80094,seq__80095,first__80096,seq__80095__$1,pair__$1,tail__$1,out,G__79992,vec__79993,seq__79994,first__79995,seq__79994__$1,pair,tail,map__79986,map__79986__$1,argspec79981,retspec79982))
})();
var state__50894__auto__ = (function (){var statearr_80191 = f__50893__auto__();
(statearr_80191[(6)] = c__50892__auto___81067);

return statearr_80191;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
});})(out__$1,G__79992__$1,c__50892__auto___81067,ch__75196__auto__,vec__80097,k,ent,res,out__$2,vec__80094,seq__80095,first__80096,seq__80095__$1,pair__$1,tail__$1,out,G__79992,vec__79993,seq__79994,first__79995,seq__79994__$1,pair,tail,map__79986,map__79986__$1,argspec79981,retspec79982))
);


return ch__75196__auto__;
} else {
var G__81102 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(out__$2,k,res);
var G__81103 = tail__$1;
out__$1 = G__81102;
G__79992__$1 = G__81103;
continue;
}
} else {
return out__$2;
}
break;
}
});
var ret79983 = f79984(env,m);
if(cljs.core.truth_(retspec79982)){
com.fulcrologic.guardrails.core.run_check(false,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"fn-name","fn-name",-766594004),"com/wsscode/pathom/core.cljc:600 join-map's",new cljs.core.Keyword(null,"emit-spec?","emit-spec?",-837774868),true,new cljs.core.Keyword(null,"log-level","log-level",862121670),null,new cljs.core.Keyword(null,"throw?","throw?",-2036749118),false,new cljs.core.Keyword(null,"vararg?","vararg?",1908105777),false], null),retspec79982,ret79983);
} else {
}

return ret79983;
});
com.wsscode.pathom.core.ident_QMARK_ = (function com$wsscode$pathom$core$ident_QMARK_(x){
return ((cljs.core.vector_QMARK_(x)) && ((cljs.core.first(x) instanceof cljs.core.Keyword)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((2),cljs.core.count(x))));
});
com.wsscode.pathom.core.ident_key_STAR_ = (function com$wsscode$pathom$core$ident_key_STAR_(key){
if(cljs.core.vector_QMARK_(key)){
return cljs.core.first(key);
} else {
return null;
}
});
/**
 * The first element of an ident.
 */
com.wsscode.pathom.core.ident_key = (function com$wsscode$pathom$core$ident_key(p__80192){
var map__80193 = p__80192;
var map__80193__$1 = (((((!((map__80193 == null))))?(((((map__80193.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80193.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80193):map__80193);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80193__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var key = (function (){var G__80195 = ast;
if((G__80195 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(G__80195);
}
})();
if(cljs.core.vector_QMARK_(key)){
return cljs.core.first(key);
} else {
return null;
}
});
com.wsscode.pathom.core.ident_value_STAR_ = (function com$wsscode$pathom$core$ident_value_STAR_(key){
if(cljs.core.vector_QMARK_(key)){
return cljs.core.second(key);
} else {
return null;
}
});
/**
 * The second element of an ident
 */
com.wsscode.pathom.core.ident_value = (function com$wsscode$pathom$core$ident_value(p__80196){
var map__80197 = p__80196;
var map__80197__$1 = (((((!((map__80197 == null))))?(((((map__80197.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80197.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80197):map__80197);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80197__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var key = (function (){var G__80199 = ast;
if((G__80199 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(G__80199);
}
})();
if(cljs.core.sequential_QMARK_(key)){
return cljs.core.second(key);
} else {
return null;
}
});
/**
 * Remove items from a query (AST) that have a key listed in the elision-set
 */
com.wsscode.pathom.core.elide_ast_nodes = (function com$wsscode$pathom$core$elide_ast_nodes(p__80201,elision_set){
var map__80202 = p__80201;
var map__80202__$1 = (((((!((map__80202 == null))))?(((((map__80202.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80202.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80202):map__80202);
var ast = map__80202__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80202__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var union_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80202__$1,new cljs.core.Keyword(null,"union-key","union-key",1529707234));
var union_elision_QMARK_ = cljs.core.contains_QMARK_(elision_set,union_key);
if(((union_elision_QMARK_) || (cljs.core.contains_QMARK_(elision_set,key)))){
return null;
} else {
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(ast,new cljs.core.Keyword(null,"children","children",-940561982),(function (c){
if(cljs.core.truth_(c)){
return cljs.core.vec(cljs.core.keep.cljs$core$IFn$_invoke$arity$2((function (p1__80200_SHARP_){
return (com.wsscode.pathom.core.elide_ast_nodes.cljs$core$IFn$_invoke$arity$2 ? com.wsscode.pathom.core.elide_ast_nodes.cljs$core$IFn$_invoke$arity$2(p1__80200_SHARP_,elision_set) : com.wsscode.pathom.core.elide_ast_nodes.call(null,p1__80200_SHARP_,elision_set));
}),c));
} else {
return null;
}
}));
}
});
com.wsscode.pathom.core.normalize_env = (function com$wsscode$pathom$core$normalize_env(p__80204){
var map__80205 = p__80204;
var map__80205__$1 = (((((!((map__80205 == null))))?(((((map__80205.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80205.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80205):map__80205);
var env = map__80205__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80205__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var G__80207 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(env,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.conj,cljs.core.PersistentVector.EMPTY),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast));
if((new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249).cljs$core$IFn$_invoke$arity$1(env) == null)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__80207,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249),new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031));
} else {
return G__80207;
}
});
com.wsscode.pathom.core.merge_queries_STAR_ = (function com$wsscode$pathom$core$merge_queries_STAR_(qa,qb){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ast,p__80210){
var map__80211 = p__80210;
var map__80211__$1 = (((((!((map__80211 == null))))?(((((map__80211.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80211.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80211):map__80211);
var item_b = map__80211__$1;
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80211__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80211__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var params = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80211__$1,new cljs.core.Keyword(null,"params","params",710516235));
var temp__5733__auto__ = cljs.core.first(cljs.core.keep_indexed.cljs$core$IFn$_invoke$arity$2((function (p1__80209_SHARP_,p2__80208_SHARP_){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(p2__80208_SHARP_),key)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__80209_SHARP_,p2__80208_SHARP_], null);
} else {
return null;
}
}),new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(ast)));
if(cljs.core.truth_(temp__5733__auto__)){
var vec__80213 = temp__5733__auto__;
var idx = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80213,(0),null);
var item = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80213,(1),null);
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"join","join",-758861890),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(item),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([type], 0))) || (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$variadic(new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(item),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([type], 0))))){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(item),params)){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(ast,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx], null),com.wsscode.pathom.core.merge_queries_STAR_,item_b);
} else {
return cljs.core.reduced(null);
}
} else {
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"prop","prop",-515168332),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(item))) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"join","join",-758861890),type)))){
return cljs.core.assoc_in(ast,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"children","children",-940561982),idx], null),item_b);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"call","call",-519999866),type)){
return cljs.core.reduced(null);
} else {
return ast;

}
}
}
} else {
return cljs.core.update.cljs$core$IFn$_invoke$arity$4(ast,new cljs.core.Keyword(null,"children","children",-940561982),cljs.core.conj,item_b);
}
}),qa,new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(qb));
});
com.wsscode.pathom.core.merge_queries = (function com$wsscode$pathom$core$merge_queries(qa,qb){
var G__80216 = com.wsscode.pathom.core.merge_queries_STAR_(com.wsscode.pathom.core.query__GT_ast(qa),com.wsscode.pathom.core.query__GT_ast(qb));
if((G__80216 == null)){
return null;
} else {
return com.wsscode.pathom.core.ast__GT_query(G__80216);
}
});
/**
 * Converts ident values and param values to ::p/var.
 */
com.wsscode.pathom.core.normalize_query_variables = (function com$wsscode$pathom$core$normalize_query_variables(query){
return com.wsscode.pathom.core.ast__GT_query(com.wsscode.pathom.core.transduce_children(cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (x){
var G__80218 = x;
var G__80218__$1 = ((com.wsscode.pathom.core.ident_QMARK_(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(x)))?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__80218,new cljs.core.Keyword(null,"key","key",-1516042587),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.first(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(x)),new cljs.core.Keyword("com.wsscode.pathom.core","var","com.wsscode.pathom.core/var",-2126559442)], null)):G__80218);
if(cljs.core.truth_(new cljs.core.Keyword(null,"params","params",710516235).cljs$core$IFn$_invoke$arity$1(x))){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(G__80218__$1,new cljs.core.Keyword(null,"params","params",710516235),(function (p1__80217_SHARP_){
return cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1((function (p__80219){
var vec__80220 = p__80219;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80220,(0),null);
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80220,(1),null);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [k,new cljs.core.Keyword("com.wsscode.pathom.core","var","com.wsscode.pathom.core/var",-2126559442)], null);
})),p1__80217_SHARP_);
}));
} else {
return G__80218__$1;
}
})),com.wsscode.pathom.core.query__GT_ast(query)));
});
/**
 * Generates a consistent hash from the query. The query first goes to a process to remove any
 *   variables from idents and params, then we get the Clojure hash of it. You can use this to save
 *   information about a query that can be used to correlate with the query later.
 */
com.wsscode.pathom.core.query_id = (function com$wsscode$pathom$core$query_id(query){
return cljs.core.hash(com.wsscode.pathom.core.normalize_query_variables(query));
});
com.wsscode.pathom.core.key_dispatch = (function com$wsscode$pathom$core$key_dispatch(p__80223){
var map__80224 = p__80223;
var map__80224__$1 = (((((!((map__80224 == null))))?(((((map__80224.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80224.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80224):map__80224);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80224__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
});
/**
 * Dispatch on the first element (type) of an incoming ident.
 */
com.wsscode.pathom.core.entity_dispatch = (function com$wsscode$pathom$core$entity_dispatch(p__80226){
var map__80227 = p__80226;
var map__80227__$1 = (((((!((map__80227 == null))))?(((((map__80227.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80227.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80227):map__80227);
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80227__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
if(cljs.core.vector_QMARK_(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast))){
return cljs.core.first(new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast));
} else {
return null;
}
});
/**
 * Produces a reader that will respond to any keyword whose namespace
 *   is in the set `(::placeholder-prefixes env)`. The join node logical
 *   level stays the same as the parent where the placeholder node is
 *   requested.
 */
com.wsscode.pathom.core.env_placeholder_reader = (function com$wsscode$pathom$core$env_placeholder_reader(p__80229){
var map__80230 = p__80229;
var map__80230__$1 = (((((!((map__80230 == null))))?(((((map__80230.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80230.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80230):map__80230);
var env = map__80230__$1;
var placeholder_prefixes = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80230__$1,new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644));
if(cljs.core.truth_(placeholder_prefixes)){
} else {
throw (new Error(["Assert failed: ","To use env-placeholder-reader please add ::p/placeholder-prefixes to your environment.","\n","placeholder-prefixes"].join('')));
}

if(com.wsscode.pathom.core.placeholder_key_QMARK_(env,new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"ast","ast",-860334068).cljs$core$IFn$_invoke$arity$1(env)))){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(env);
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * This will lift the AST from placeholders to the same level of the query, as if there was not placeholders in it.
 */
com.wsscode.pathom.core.lift_placeholders_ast = (function com$wsscode$pathom$core$lift_placeholders_ast(env,ast){
return clojure.walk.postwalk((function (x){
var temp__5733__auto__ = new cljs.core.Keyword(null,"children","children",-940561982).cljs$core$IFn$_invoke$arity$1(x);
if(cljs.core.truth_(temp__5733__auto__)){
var children = temp__5733__auto__;
var map__80233 = cljs.core.group_by((function (p1__80232_SHARP_){
return ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"join","join",-758861890),new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(p1__80232_SHARP_))) && (com.wsscode.pathom.core.placeholder_key_QMARK_(env,new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(p1__80232_SHARP_))));
}),children);
var map__80233__$1 = (((((!((map__80233 == null))))?(((((map__80233.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80233.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80233):map__80233);
var placeholders = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80233__$1,true);
var regular = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80233__$1,false);
var _LT__GT_ = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(x,new cljs.core.Keyword(null,"children","children",-940561982),(function (){var or__4126__auto__ = regular;
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return cljs.core.PersistentVector.EMPTY;
}
})());
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(com.wsscode.pathom.core.merge_queries_STAR_,_LT__GT_,placeholders);
} else {
return x;
}
}),ast);
});
/**
 * This will lift the queries from placeholders to the same level of the query, as if there was not placeholders in it.
 */
com.wsscode.pathom.core.lift_placeholders = (function com$wsscode$pathom$core$lift_placeholders(env,query){
return com.wsscode.pathom.core.ast__GT_query(com.wsscode.pathom.core.lift_placeholders_ast(env,com.wsscode.pathom.core.query__GT_ast(query)));
});
/**
 * Children should join when there is a query, unless the value is marked as final.
 */
com.wsscode.pathom.core.join_children_QMARK_ = (function com$wsscode$pathom$core$join_children_QMARK_(p__80235,v){
var map__80236 = p__80235;
var map__80236__$1 = (((((!((map__80236 == null))))?(((((map__80236.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80236.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80236):map__80236);
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80236__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var and__4115__auto__ = query;
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core.not(new cljs.core.Keyword("com.wsscode.pathom.core","final","com.wsscode.pathom.core/final",891454300).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(v)));
} else {
return and__4115__auto__;
}
});
/**
 * Map reader will try to find the ast key on the current entity and output it. When the value is a map and a
 *   sub query is present, it will apply the sub query on that value (recursively). When the value is a sequence,
 *   map-reader will do a join on each of the items (and apply sub queries if it's present and values are maps.
 * 
 *   Map-reader will defer the read when the key is not present at entity.
 */
com.wsscode.pathom.core.map_reader = (function com$wsscode$pathom$core$map_reader(p__80238){
var map__80239 = p__80238;
var map__80239__$1 = (((((!((map__80239 == null))))?(((((map__80239.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80239.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80239):map__80239);
var env = map__80239__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80239__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80239__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var key = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var entity = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
if(cljs.core.contains_QMARK_(entity,key)){
var v = cljs.core.get.cljs$core$IFn$_invoke$arity$2(entity,key);
if(cljs.core.sequential_QMARK_(v)){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_seq(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_(((cljs.core.map_QMARK_(v))?(function (){var or__4126__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(v));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(query));
}
})():false))){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_map(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_(((cljs.core.map_QMARK_(v))?com.wsscode.pathom.core.join_children_QMARK_(env,v):false))){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(v,env);
} else {
return v;
}

}
}
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * Like map-reader, but it has extra options (read from the environment):
 *   map-key-transform: (fn [key]) will transform the key on the AST before trying to match with entity key
 *   map-value-transform: (fn [key value]) will transform the output value after reading from the entity.
 * 
 *   The reason to have a separated reader is so the plain version (map-reader) can be faster by avoiding checking
 *   the presence of transform functions.
 */
com.wsscode.pathom.core.map_reader_STAR_ = (function com$wsscode$pathom$core$map_reader_STAR_(p__80241){
var map__80242 = p__80241;
var map__80242__$1 = (((((!((map__80242 == null))))?(((((map__80242.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80242.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80242):map__80242);
var map_key_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80242__$1,new cljs.core.Keyword("com.wsscode.pathom.core","map-key-transform","com.wsscode.pathom.core/map-key-transform",-238565800));
var map_value_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80242__$1,new cljs.core.Keyword("com.wsscode.pathom.core","map-value-transform","com.wsscode.pathom.core/map-value-transform",1252006952));
return (function (p__80244){
var map__80245 = p__80244;
var map__80245__$1 = (((((!((map__80245 == null))))?(((((map__80245.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80245.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80245):map__80245);
var env = map__80245__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80245__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80245__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80245__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
var key = (function (){var G__80247 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
if(cljs.core.truth_(map_key_transform)){
return (map_key_transform.cljs$core$IFn$_invoke$arity$1 ? map_key_transform.cljs$core$IFn$_invoke$arity$1(G__80247) : map_key_transform.call(null,G__80247));
} else {
return G__80247;
}
})();
var entity = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
if(cljs.core.contains_QMARK_(entity,key)){
var v = cljs.core.get.cljs$core$IFn$_invoke$arity$2(entity,key);
if(cljs.core.sequential_QMARK_(v)){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_seq(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_(((cljs.core.map_QMARK_(v))?(function (){var or__4126__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(v));
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","map-of-maps","com.wsscode.pathom.core/map-of-maps",-1598019706).cljs$core$IFn$_invoke$arity$1(cljs.core.meta(query));
}
})():false))){
if(cljs.core.truth_(com.wsscode.pathom.core.join_children_QMARK_(env,v))){
return com.wsscode.pathom.core.join_map(env,v);
} else {
return v;
}
} else {
if(cljs.core.truth_(((cljs.core.map_QMARK_(v))?com.wsscode.pathom.core.join_children_QMARK_(env,v):false))){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,v));
} else {
var G__80248 = v;
if(cljs.core.truth_(map_value_transform)){
var G__80249 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var G__80250 = G__80248;
return (map_value_transform.cljs$core$IFn$_invoke$arity$2 ? map_value_transform.cljs$core$IFn$_invoke$arity$2(G__80249,G__80250) : map_value_transform.call(null,G__80249,G__80250));
} else {
return G__80248;
}
}

}
}
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
});
/**
 * Like map-reader*, but handles plain Javascript objects instead of Clojure maps.
 */
com.wsscode.pathom.core.js_obj_reader = (function com$wsscode$pathom$core$js_obj_reader(p__80251){
var map__80252 = p__80251;
var map__80252__$1 = (((((!((map__80252 == null))))?(((((map__80252.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80252.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80252):map__80252);
var env = map__80252__$1;
var query = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80252__$1,new cljs.core.Keyword(null,"query","query",-1288509510));
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80252__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var js_key_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__80252__$1,new cljs.core.Keyword("com.wsscode.pathom.core","js-key-transform","com.wsscode.pathom.core/js-key-transform",-1588372758),cljs.core.name);
var js_value_transform = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__80252__$1,new cljs.core.Keyword("com.wsscode.pathom.core","js-value-transform","com.wsscode.pathom.core/js-value-transform",1418749137),(function (_,v){
return v;
}));
var entity_key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80252__$1,new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249));
var js_key = (function (){var G__80254 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
return (js_key_transform.cljs$core$IFn$_invoke$arity$1 ? js_key_transform.cljs$core$IFn$_invoke$arity$1(G__80254) : js_key_transform.call(null,G__80254));
})();
var entity = com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$1(env);
if(cljs.core.truth_(goog.object.containsKey(entity,js_key))){
var v = goog.object.get(entity,js_key);
if(cljs.core.truth_(Array.isArray(v))){
if(cljs.core.truth_(query)){
return com.wsscode.pathom.core.join_seq(env,cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(v));
} else {
return v;
}
} else {
if(cljs.core.truth_((function (){var and__4115__auto__ = query;
if(cljs.core.truth_(and__4115__auto__)){
return cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(cljs.core.type(v),Object);
} else {
return and__4115__auto__;
}
})())){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,entity_key,v));
} else {
var G__80255 = new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast);
var G__80256 = v;
return (js_value_transform.cljs$core$IFn$_invoke$arity$2 ? js_value_transform.cljs$core$IFn$_invoke$arity$2(G__80255,G__80256) : js_value_transform.call(null,G__80255,G__80256));
}
}
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * This reader will join on any ident join, the entity for the join will be a map containing the same
 *   key and value expressed on the ident, eg: [{[:id 123] [:id]}], the join entry will be {:id 123}.
 */
com.wsscode.pathom.core.ident_join_reader = (function com$wsscode$pathom$core$ident_join_reader(env){
var temp__5733__auto__ = com.wsscode.pathom.core.ident_key(env);
if(cljs.core.truth_(temp__5733__auto__)){
var key = temp__5733__auto__;
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.createAsIfByAssoc([key,com.wsscode.pathom.core.ident_value(env)]),env);
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
/**
 * Helper to create a plugin that can view/modify the env/tx of a top-level request.
 *   f - (fn [{:keys [env tx]}] {:env new-env :tx new-tx})
 *   If the function returns no env or tx, then the parser will not be called (aborts the parse)
 */
com.wsscode.pathom.core.pre_process_parser_plugin = (function com$wsscode$pathom$core$pre_process_parser_plugin(f){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$pre_process_parser_plugin_$_transform_parser_out_plugin_external(parser){
return (function com$wsscode$pathom$core$pre_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal(env,tx){
var map__80257 = (function (){var G__80258 = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"env","env",-1815813235),env,new cljs.core.Keyword(null,"tx","tx",466630418),tx], null);
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(G__80258) : f.call(null,G__80258));
})();
var map__80257__$1 = (((((!((map__80257 == null))))?(((((map__80257.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80257.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80257):map__80257);
var env__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80257__$1,new cljs.core.Keyword(null,"env","env",-1815813235));
var tx__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80257__$1,new cljs.core.Keyword(null,"tx","tx",466630418));
if(((cljs.core.map_QMARK_(env__$1)) && (cljs.core.seq(tx__$1)))){
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env__$1,tx__$1) : parser.call(null,env__$1,tx__$1));
} else {
return cljs.core.PersistentArrayMap.EMPTY;
}
});
})], null);
});
/**
 * Helper to create a plugin to work on the parser output. `f` will run once with the parser final result.
 */
com.wsscode.pathom.core.post_process_parser_plugin = (function com$wsscode$pathom$core$post_process_parser_plugin(f){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external(parser){
return (function com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal(env,tx){
var res__75232__auto__ = (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env,tx) : parser.call(null,env,tx));
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80274){
var state_val_80275 = (state_80274[(1)]);
if((state_val_80275 === (1))){
var state_80274__$1 = state_80274;
var statearr_80276_81109 = state_80274__$1;
(statearr_80276_81109[(2)] = null);

(statearr_80276_81109[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80275 === (2))){
var _ = (function (){var statearr_80277 = state_80274;
(statearr_80277[(4)] = cljs.core.cons((5),(state_80274[(4)])));

return statearr_80277;
})();
var state_80274__$1 = state_80274;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80274__$1,(6),res__75232__auto__);
} else {
if((state_val_80275 === (3))){
var inst_80272 = (state_80274[(2)]);
var state_80274__$1 = state_80274;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80274__$1,inst_80272);
} else {
if((state_val_80275 === (4))){
var inst_80260 = (state_80274[(2)]);
var state_80274__$1 = state_80274;
var statearr_80279_81110 = state_80274__$1;
(statearr_80279_81110[(2)] = inst_80260);

(statearr_80279_81110[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80275 === (5))){
var _ = (function (){var statearr_80280 = state_80274;
(statearr_80280[(4)] = cljs.core.rest((state_80274[(4)])));

return statearr_80280;
})();
var state_80274__$1 = state_80274;
var ex80278 = (state_80274__$1[(2)]);
var statearr_80281_81117 = state_80274__$1;
(statearr_80281_81117[(5)] = ex80278);


var statearr_80282_81118 = state_80274__$1;
(statearr_80282_81118[(1)] = (4));

(statearr_80282_81118[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80275 === (6))){
var inst_80267 = (state_80274[(2)]);
var inst_80268 = com.wsscode.async.async_cljs.throw_err(inst_80267);
var inst_80269 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_80268) : f.call(null,inst_80268));
var _ = (function (){var statearr_80283 = state_80274;
(statearr_80283[(4)] = cljs.core.rest((state_80274[(4)])));

return statearr_80283;
})();
var state_80274__$1 = state_80274;
var statearr_80284_81119 = state_80274__$1;
(statearr_80284_81119[(2)] = inst_80269);

(statearr_80284_81119[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto____0 = (function (){
var statearr_80285 = [null,null,null,null,null,null,null];
(statearr_80285[(0)] = com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto__);

(statearr_80285[(1)] = (1));

return statearr_80285;
});
var com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto____1 = (function (state_80274){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80274);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80286){var ex__50860__auto__ = e80286;
var statearr_80287_81121 = state_80274;
(statearr_80287_81121[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80274[(4)]))){
var statearr_80288_81122 = state_80274;
(statearr_80288_81122[(1)] = cljs.core.first((state_80274[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81123 = state_80274;
state_80274 = G__81123;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto__ = function(state_80274){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto____1.call(this,state_80274);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$post_process_parser_plugin_$_transform_parser_out_plugin_external_$_transform_parser_out_plugin_internal_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80289 = f__50893__auto__();
(statearr_80289[(6)] = c__50892__auto__);

return statearr_80289;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var res = res__75232__auto__;
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(res) : f.call(null,res));
}
});
})], null);
});
com.wsscode.pathom.core.elide_special_outputs_plugin = com.wsscode.pathom.core.post_process_parser_plugin(com.wsscode.pathom.core.elide_special_outputs);
com.wsscode.pathom.core.error_message = (function com$wsscode$pathom$core$error_message(err){
return err.message;
});
com.wsscode.pathom.core.error_str = (function com$wsscode$pathom$core$error_str(err){
var msg = err.message;
var data = cljs.core.ex_data(err);
var G__80290 = msg;
if(cljs.core.truth_(data)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__80290)," - ",cljs.core.pr_str.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([data], 0))].join('');
} else {
return G__80290;
}
});
/**
 * Helper function to update a mutation action.
 */
com.wsscode.pathom.core.update_action = (function com$wsscode$pathom$core$update_action(m,f){
if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"action","action",-811238024))){
return cljs.core.update.cljs$core$IFn$_invoke$arity$3(m,new cljs.core.Keyword(null,"action","action",-811238024),f);
} else {
return m;
}
});
com.wsscode.pathom.core.process_error = (function com$wsscode$pathom$core$process_error(p__80291,e){
var map__80292 = p__80291;
var map__80292__$1 = (((((!((map__80292 == null))))?(((((map__80292.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80292.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80292):map__80292);
var env = map__80292__$1;
var process_error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80292__$1,new cljs.core.Keyword("com.wsscode.pathom.core","process-error","com.wsscode.pathom.core/process-error",-2116719411));
if(cljs.core.truth_(process_error)){
return (process_error.cljs$core$IFn$_invoke$arity$2 ? process_error.cljs$core$IFn$_invoke$arity$2(env,e) : process_error.call(null,env,e));
} else {
return com.wsscode.pathom.core.error_str(e);
}
});
com.wsscode.pathom.core.add_error = (function com$wsscode$pathom$core$add_error(p__80294,e){
var map__80295 = p__80294;
var map__80295__$1 = (((((!((map__80295 == null))))?(((((map__80295.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80295.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80295):map__80295);
var env = map__80295__$1;
var errors_STAR_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80295__$1,new cljs.core.Keyword("com.wsscode.pathom.core","errors*","com.wsscode.pathom.core/errors*",337011276));
var path = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80295__$1,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661));
if(cljs.core.truth_(errors_STAR_)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(errors_STAR_,cljs.core.assoc,path,com.wsscode.pathom.core.process_error(env,e));
} else {
}

return new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882);
});
com.wsscode.pathom.core.wrap_handle_exception = (function com$wsscode$pathom$core$wrap_handle_exception(reader){
return (function com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal(p__80297){
var map__80298 = p__80297;
var map__80298__$1 = (((((!((map__80298 == null))))?(((((map__80298.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80298.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80298):map__80298);
var env = map__80298__$1;
var fail_fast_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80298__$1,new cljs.core.Keyword("com.wsscode.pathom.core","fail-fast?","com.wsscode.pathom.core/fail-fast?",-272943465));
if(cljs.core.truth_(fail_fast_QMARK_)){
return (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env));
} else {
try{var x = (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env));
if(com.wsscode.async.async_cljs.chan_QMARK_(x)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80315){
var state_val_80316 = (state_80315[(1)]);
if((state_val_80316 === (1))){
var state_80315__$1 = state_80315;
var statearr_80317_81135 = state_80315__$1;
(statearr_80317_81135[(2)] = null);

(statearr_80317_81135[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80316 === (2))){
var _ = (function (){var statearr_80318 = state_80315;
(statearr_80318[(4)] = cljs.core.cons((5),(state_80315[(4)])));

return statearr_80318;
})();
var state_80315__$1 = state_80315;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80315__$1,(6),x);
} else {
if((state_val_80316 === (3))){
var inst_80313 = (state_80315[(2)]);
var state_80315__$1 = state_80315;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80315__$1,inst_80313);
} else {
if((state_val_80316 === (4))){
var inst_80301 = (state_80315[(2)]);
var inst_80302 = com.wsscode.pathom.core.add_error(env,inst_80301);
var state_80315__$1 = state_80315;
var statearr_80320_81136 = state_80315__$1;
(statearr_80320_81136[(2)] = inst_80302);

(statearr_80320_81136[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80316 === (5))){
var _ = (function (){var statearr_80321 = state_80315;
(statearr_80321[(4)] = cljs.core.rest((state_80315[(4)])));

return statearr_80321;
})();
var state_80315__$1 = state_80315;
var ex80319 = (state_80315__$1[(2)]);
var statearr_80322_81137 = state_80315__$1;
(statearr_80322_81137[(5)] = ex80319);


var statearr_80323_81138 = state_80315__$1;
(statearr_80323_81138[(1)] = (4));

(statearr_80323_81138[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80316 === (6))){
var inst_80309 = (state_80315[(2)]);
var inst_80310 = com.wsscode.async.async_cljs.throw_err(inst_80309);
var _ = (function (){var statearr_80324 = state_80315;
(statearr_80324[(4)] = cljs.core.rest((state_80315[(4)])));

return statearr_80324;
})();
var state_80315__$1 = state_80315;
var statearr_80325_81139 = state_80315__$1;
(statearr_80325_81139[(2)] = inst_80310);

(statearr_80325_81139[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto____0 = (function (){
var statearr_80326 = [null,null,null,null,null,null,null];
(statearr_80326[(0)] = com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto__);

(statearr_80326[(1)] = (1));

return statearr_80326;
});
var com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto____1 = (function (state_80315){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80315);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80327){var ex__50860__auto__ = e80327;
var statearr_80328_81140 = state_80315;
(statearr_80328_81140[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80315[(4)]))){
var statearr_80329_81141 = state_80315;
(statearr_80329_81141[(1)] = cljs.core.first((state_80315[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81142 = state_80315;
state_80315 = G__81142;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto__ = function(state_80315){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto____1.call(this,state_80315);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$wrap_handle_exception_$_wrap_handle_exception_internal_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80330 = f__50893__auto__();
(statearr_80330[(6)] = c__50892__auto__);

return statearr_80330;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
return x;
}
}catch (e80300){var e = e80300;
return com.wsscode.pathom.core.add_error(env,e);
}}
});
});
com.wsscode.pathom.core.wrap_mutate_handle_exception = (function com$wsscode$pathom$core$wrap_mutate_handle_exception(mutate){
return (function com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal(p__80331,k,p){
var map__80332 = p__80331;
var map__80332__$1 = (((((!((map__80332 == null))))?(((((map__80332.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80332.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80332):map__80332);
var env = map__80332__$1;
var process_error = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80332__$1,new cljs.core.Keyword("com.wsscode.pathom.core","process-error","com.wsscode.pathom.core/process-error",-2116719411));
var fail_fast_QMARK_ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80332__$1,new cljs.core.Keyword("com.wsscode.pathom.core","fail-fast?","com.wsscode.pathom.core/fail-fast?",-272943465));
if(cljs.core.truth_(fail_fast_QMARK_)){
return (mutate.cljs$core$IFn$_invoke$arity$3 ? mutate.cljs$core$IFn$_invoke$arity$3(env,k,p) : mutate.call(null,env,k,p));
} else {
try{return com.wsscode.pathom.core.update_action((mutate.cljs$core$IFn$_invoke$arity$3 ? mutate.cljs$core$IFn$_invoke$arity$3(env,k,p) : mutate.call(null,env,k,p)),(function (action){
return (function (){
try{var res = (action.cljs$core$IFn$_invoke$arity$0 ? action.cljs$core$IFn$_invoke$arity$0() : action.call(null));
if(com.wsscode.async.async_cljs.chan_QMARK_(res)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80358){
var state_val_80359 = (state_80358[(1)]);
if((state_val_80359 === (7))){
var inst_80345 = (state_80358[(2)]);
var state_80358__$1 = state_80358;
var statearr_80360_81148 = state_80358__$1;
(statearr_80360_81148[(2)] = inst_80345);

(statearr_80360_81148[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80359 === (1))){
var state_80358__$1 = state_80358;
var statearr_80361_81149 = state_80358__$1;
(statearr_80361_81149[(2)] = null);

(statearr_80361_81149[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80359 === (4))){
var inst_80336 = (state_80358[(2)]);
var state_80358__$1 = (function (){var statearr_80362 = state_80358;
(statearr_80362[(7)] = inst_80336);

return statearr_80362;
})();
if(cljs.core.truth_(process_error)){
var statearr_80363_81151 = state_80358__$1;
(statearr_80363_81151[(1)] = (5));

} else {
var statearr_80364_81152 = state_80358__$1;
(statearr_80364_81152[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80359 === (6))){
var inst_80336 = (state_80358[(7)]);
var inst_80340 = [new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882)];
var inst_80341 = com.wsscode.pathom.core.error_str(inst_80336);
var inst_80342 = [inst_80341];
var inst_80343 = cljs.core.PersistentHashMap.fromArrays(inst_80340,inst_80342);
var state_80358__$1 = state_80358;
var statearr_80365_81154 = state_80358__$1;
(statearr_80365_81154[(2)] = inst_80343);

(statearr_80365_81154[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80359 === (3))){
var inst_80356 = (state_80358[(2)]);
var state_80358__$1 = state_80358;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80358__$1,inst_80356);
} else {
if((state_val_80359 === (2))){
var _ = (function (){var statearr_80366 = state_80358;
(statearr_80366[(4)] = cljs.core.cons((8),(state_80358[(4)])));

return statearr_80366;
})();
var state_80358__$1 = state_80358;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80358__$1,(9),res);
} else {
if((state_val_80359 === (9))){
var inst_80352 = (state_80358[(2)]);
var inst_80353 = com.wsscode.async.async_cljs.throw_err(inst_80352);
var _ = (function (){var statearr_80368 = state_80358;
(statearr_80368[(4)] = cljs.core.rest((state_80358[(4)])));

return statearr_80368;
})();
var state_80358__$1 = state_80358;
var statearr_80369_81155 = state_80358__$1;
(statearr_80369_81155[(2)] = inst_80353);

(statearr_80369_81155[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80359 === (5))){
var inst_80336 = (state_80358[(7)]);
var inst_80338 = (process_error.cljs$core$IFn$_invoke$arity$2 ? process_error.cljs$core$IFn$_invoke$arity$2(env,inst_80336) : process_error.call(null,env,inst_80336));
var state_80358__$1 = state_80358;
var statearr_80370_81156 = state_80358__$1;
(statearr_80370_81156[(2)] = inst_80338);

(statearr_80370_81156[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80359 === (8))){
var _ = (function (){var statearr_80371 = state_80358;
(statearr_80371[(4)] = cljs.core.rest((state_80358[(4)])));

return statearr_80371;
})();
var state_80358__$1 = state_80358;
var ex80367 = (state_80358__$1[(2)]);
var statearr_80372_81163 = state_80358__$1;
(statearr_80372_81163[(5)] = ex80367);


var statearr_80373_81164 = state_80358__$1;
(statearr_80373_81164[(1)] = (4));

(statearr_80373_81164[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto____0 = (function (){
var statearr_80374 = [null,null,null,null,null,null,null,null];
(statearr_80374[(0)] = com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto__);

(statearr_80374[(1)] = (1));

return statearr_80374;
});
var com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto____1 = (function (state_80358){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80358);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80375){var ex__50860__auto__ = e80375;
var statearr_80376_81166 = state_80358;
(statearr_80376_81166[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80358[(4)]))){
var statearr_80377_81167 = state_80358;
(statearr_80377_81167[(1)] = cljs.core.first((state_80358[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81168 = state_80358;
state_80358 = G__81168;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto__ = function(state_80358){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto____1.call(this,state_80358);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$wrap_mutate_handle_exception_$_wrap_mutate_handle_exception_internal_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80378 = f__50893__auto__();
(statearr_80378[(6)] = c__50892__auto__);

return statearr_80378;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
return res;
}
}catch (e80335){var e = e80335;
if(cljs.core.truth_(process_error)){
return (process_error.cljs$core$IFn$_invoke$arity$2 ? process_error.cljs$core$IFn$_invoke$arity$2(env,e) : process_error.call(null,env,e));
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),com.wsscode.pathom.core.error_str(e)], null);
}
}});
}));
}catch (e80334){var e = e80334;
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"action","action",-811238024),(function (){
if(cljs.core.truth_(process_error)){
return (process_error.cljs$core$IFn$_invoke$arity$2 ? process_error.cljs$core$IFn$_invoke$arity$2(env,e) : process_error.call(null,env,e));
} else {
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),com.wsscode.pathom.core.error_str(e)], null);
}
})], null);
}}
});
});
com.wsscode.pathom.core.wrap_parser_exception = (function com$wsscode$pathom$core$wrap_parser_exception(parser){
return (function com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal(env,tx){
var errors = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var res__75232__auto__ = (function (){var G__80379 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","errors*","com.wsscode.pathom.core/errors*",337011276),errors);
var G__80380 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__80379,G__80380) : parser.call(null,G__80379,G__80380));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80403){
var state_val_80404 = (state_80403[(1)]);
if((state_val_80404 === (7))){
var inst_80389 = (state_80403[(7)]);
var inst_80394 = cljs.core.deref(errors);
var inst_80395 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(inst_80389,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),inst_80394);
var state_80403__$1 = state_80403;
var statearr_80405_81173 = state_80403__$1;
(statearr_80405_81173[(2)] = inst_80395);

(statearr_80405_81173[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80404 === (1))){
var state_80403__$1 = state_80403;
var statearr_80406_81174 = state_80403__$1;
(statearr_80406_81174[(2)] = null);

(statearr_80406_81174[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80404 === (4))){
var inst_80381 = (state_80403[(2)]);
var state_80403__$1 = state_80403;
var statearr_80407_81175 = state_80403__$1;
(statearr_80407_81175[(2)] = inst_80381);

(statearr_80407_81175[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80404 === (6))){
var inst_80388 = (state_80403[(2)]);
var inst_80389 = com.wsscode.async.async_cljs.throw_err(inst_80388);
var inst_80391 = cljs.core.deref(errors);
var inst_80392 = cljs.core.seq(inst_80391);
var state_80403__$1 = (function (){var statearr_80408 = state_80403;
(statearr_80408[(7)] = inst_80389);

return statearr_80408;
})();
if(inst_80392){
var statearr_80409_81177 = state_80403__$1;
(statearr_80409_81177[(1)] = (7));

} else {
var statearr_80410_81178 = state_80403__$1;
(statearr_80410_81178[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80404 === (3))){
var inst_80401 = (state_80403[(2)]);
var state_80403__$1 = state_80403;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80403__$1,inst_80401);
} else {
if((state_val_80404 === (2))){
var _ = (function (){var statearr_80412 = state_80403;
(statearr_80412[(4)] = cljs.core.cons((5),(state_80403[(4)])));

return statearr_80412;
})();
var state_80403__$1 = state_80403;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80403__$1,(6),res__75232__auto__);
} else {
if((state_val_80404 === (9))){
var inst_80398 = (state_80403[(2)]);
var _ = (function (){var statearr_80413 = state_80403;
(statearr_80413[(4)] = cljs.core.rest((state_80403[(4)])));

return statearr_80413;
})();
var state_80403__$1 = state_80403;
var statearr_80414_81181 = state_80403__$1;
(statearr_80414_81181[(2)] = inst_80398);

(statearr_80414_81181[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80404 === (5))){
var _ = (function (){var statearr_80415 = state_80403;
(statearr_80415[(4)] = cljs.core.rest((state_80403[(4)])));

return statearr_80415;
})();
var state_80403__$1 = state_80403;
var ex80411 = (state_80403__$1[(2)]);
var statearr_80416_81183 = state_80403__$1;
(statearr_80416_81183[(5)] = ex80411);


var statearr_80417_81185 = state_80403__$1;
(statearr_80417_81185[(1)] = (4));

(statearr_80417_81185[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80404 === (8))){
var inst_80389 = (state_80403[(7)]);
var state_80403__$1 = state_80403;
var statearr_80418_81186 = state_80403__$1;
(statearr_80418_81186[(2)] = inst_80389);

(statearr_80418_81186[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto____0 = (function (){
var statearr_80419 = [null,null,null,null,null,null,null,null];
(statearr_80419[(0)] = com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto__);

(statearr_80419[(1)] = (1));

return statearr_80419;
});
var com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto____1 = (function (state_80403){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80403);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80420){var ex__50860__auto__ = e80420;
var statearr_80421_81187 = state_80403;
(statearr_80421_81187[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80403[(4)]))){
var statearr_80422_81188 = state_80403;
(statearr_80422_81188[(1)] = cljs.core.first((state_80403[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81189 = state_80403;
state_80403 = G__81189;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto__ = function(state_80403){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto____1.call(this,state_80403);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$wrap_parser_exception_$_wrap_parser_exception_internal_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80423 = f__50893__auto__();
(statearr_80423[(6)] = c__50892__auto__);

return statearr_80423;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var res = res__75232__auto__;
var G__80424 = res;
if(cljs.core.seq(cljs.core.deref(errors))){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__80424,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),cljs.core.deref(errors));
} else {
return G__80424;
}
}
});
});
/**
 * Wrap reads with try-catch and put any errors under `::p/errors` (including the path),
 * setting the value of the errored node to `::p/reader-error`.
 * 
 *   You can customize how the error is exported into the `::p/errors` map by setting the key
 *   `::p/process-error` in your environment to a function of [env, err] -> data.
 */
com.wsscode.pathom.core.error_handler_plugin = new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261),com.wsscode.pathom.core.wrap_handle_exception,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),com.wsscode.pathom.core.wrap_parser_exception,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202),com.wsscode.pathom.core.wrap_mutate_handle_exception], null);
com.wsscode.pathom.core.trace_plugin = com.wsscode.pathom.trace.trace_plugin;
/**
 * Reduces the error path to the last available nesting on the map m.
 */
com.wsscode.pathom.core.collapse_error_path = (function com$wsscode$pathom$core$collapse_error_path(m,path){
return cljs.core.vec((function (){var path_SINGLEQUOTE_ = path;
while(true){
if((cljs.core.count(path_SINGLEQUOTE_) === (0))){
return cljs.core.take.cljs$core$IFn$_invoke$arity$2((1),path);
} else {
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(m,path_SINGLEQUOTE_))){
return path_SINGLEQUOTE_;
} else {
var G__81194 = cljs.core.butlast(path_SINGLEQUOTE_);
path_SINGLEQUOTE_ = G__81194;
continue;
}
}
break;
}
})());
});
/**
 * Extract errors from the data root and inject those in the same level where
 * the error item is present. For example:
 * 
 * {:query {:item :com.wsscode.pathom/reader-error}
 *  :com.wsscode.pathom.core/errors
 *  {[:query :item] {:error "some error"}}}
 * 
 * Is turned into:
 * 
 * {:query {:item :com.wsscode.pathom/reader-error
 *          :com.wsscode.pathom.core/errors {:item {:error "some error"}}}
 * 
 * This makes easier to reach for the error when rendering the UI.
 * 
 * Use it e.g. via [[p/post-process-parser-plugin]], after the [[p/error-handler-plugin]].
 */
com.wsscode.pathom.core.raise_errors = (function com$wsscode$pathom$core$raise_errors(data){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (m,p__80425){
var vec__80426 = p__80425;
var path = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80426,(0),null);
var err = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80426,(1),null);
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword("com.wsscode.pathom.core","reader-error","com.wsscode.pathom.core/reader-error",-803587882),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(m,path))){
var path_SINGLEQUOTE_ = cljs.core.concat.cljs$core$IFn$_invoke$arity$2(cljs.core.butlast(path),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217),cljs.core.last(path)], null));
return cljs.core.assoc_in(m,path_SINGLEQUOTE_,err);
} else {
return m;
}
}),cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(data,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217)),cljs.core.get.cljs$core$IFn$_invoke$arity$2(data,new cljs.core.Keyword("com.wsscode.pathom.core","errors","com.wsscode.pathom.core/errors",-1179549217)));
});
/**
 * Mutations running through a parser all come back in a map like this {'my/mutation {:result {...}}}. This function
 *   converts that to {'my/mutation {...}}. Copied from fulcro.server.
 */
com.wsscode.pathom.core.raise_response = (function com$wsscode$pathom$core$raise_response(resp){
return clojure.walk.prewalk((function (x){
if(cljs.core.map_QMARK_(x)){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (acc,p__80429){
var vec__80430 = p__80429;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80430,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80430,(1),null);
if((((k instanceof cljs.core.Symbol)) && ((!((new cljs.core.Keyword(null,"result","result",1415092211).cljs$core$IFn$_invoke$arity$1(v) == null)))))){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(acc,k,new cljs.core.Keyword(null,"result","result",1415092211).cljs$core$IFn$_invoke$arity$1(v));
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(acc,k,v);
}
}),cljs.core.PersistentArrayMap.EMPTY,x);
} else {
return x;
}
}),resp);
});
com.wsscode.pathom.core.raise_mutation_result_plugin = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$raise_mutation_result_wrap_parser(parser){
return (function com$wsscode$pathom$core$raise_mutation_result_wrap_parser_$_raise_mutation_result_wrap_internal(env,tx){
return com.wsscode.pathom.core.raise_response((parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(env,tx) : parser.call(null,env,tx)));
});
})], null);
com.wsscode.pathom.core.env_plugin = (function com$wsscode$pathom$core$env_plugin(extra_env){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$env_plugin_$_env_plugin_wrap_parser(parser){
return (function com$wsscode$pathom$core$env_plugin_$_env_plugin_wrap_parser_$_env_plugin_wrap_internal(env,tx){
var G__80433 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([extra_env,env], 0));
var G__80434 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__80433,G__80434) : parser.call(null,G__80433,G__80434));
});
})], null);
});
/**
 * This plugin receives a function that will be called to wrap the current
 *   enviroment each time the main parser is called (parser level).
 */
com.wsscode.pathom.core.env_wrap_plugin = (function com$wsscode$pathom$core$env_wrap_plugin(extra_env_wrapper){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793),(function com$wsscode$pathom$core$env_wrap_plugin_$_env_wrap_wrap_parser(parser){
return (function com$wsscode$pathom$core$env_wrap_plugin_$_env_wrap_wrap_parser_$_env_wrap_wrap_internal(env,tx){
var G__80435 = (extra_env_wrapper.cljs$core$IFn$_invoke$arity$1 ? extra_env_wrapper.cljs$core$IFn$_invoke$arity$1(env) : extra_env_wrapper.call(null,env));
var G__80436 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__80435,G__80436) : parser.call(null,G__80435,G__80436));
});
})], null);
});
/**
 * DEPRECATED not required anymore, this was integrated in the main engine.
 */
com.wsscode.pathom.core.request_cache_plugin = cljs.core.PersistentArrayMap.EMPTY;
com.wsscode.pathom.core.cached_STAR_ = (function com$wsscode$pathom$core$cached_STAR_(env,key,body_fn){
var temp__5733__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
if(cljs.core.truth_(temp__5733__auto__)){
var cache = temp__5733__auto__;
var temp__5733__auto____$1 = cljs.core.find(cljs.core.deref(cache),key);
if(cljs.core.truth_(temp__5733__auto____$1)){
var vec__80437 = temp__5733__auto____$1;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80437,(0),null);
var hit = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80437,(1),null);
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-hit","com.wsscode.pathom.core/cache-hit",1851998374),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

return com.wsscode.async.async_cljs.throw_err(hit);
} else {
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-miss","com.wsscode.pathom.core/cache-miss",1311426337),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

var res__75232__auto__ = (function (){try{return (body_fn.cljs$core$IFn$_invoke$arity$0 ? body_fn.cljs$core$IFn$_invoke$arity$0() : body_fn.call(null));
}catch (e80440){var e = e80440;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.assoc,key,e);

throw e;
}})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80455){
var state_val_80456 = (state_80455[(1)]);
if((state_val_80456 === (1))){
var state_80455__$1 = state_80455;
var statearr_80457_81204 = state_80455__$1;
(statearr_80457_81204[(2)] = null);

(statearr_80457_81204[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80456 === (2))){
var _ = (function (){var statearr_80458 = state_80455;
(statearr_80458[(4)] = cljs.core.cons((5),(state_80455[(4)])));

return statearr_80458;
})();
var state_80455__$1 = state_80455;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80455__$1,(6),res__75232__auto__);
} else {
if((state_val_80456 === (3))){
var inst_80453 = (state_80455[(2)]);
var state_80455__$1 = state_80455;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80455__$1,inst_80453);
} else {
if((state_val_80456 === (4))){
var inst_80441 = (state_80455[(2)]);
var state_80455__$1 = state_80455;
var statearr_80460_81205 = state_80455__$1;
(statearr_80460_81205[(2)] = inst_80441);

(statearr_80460_81205[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80456 === (5))){
var _ = (function (){var statearr_80461 = state_80455;
(statearr_80461[(4)] = cljs.core.rest((state_80455[(4)])));

return statearr_80461;
})();
var state_80455__$1 = state_80455;
var ex80459 = (state_80455__$1[(2)]);
var statearr_80462_81209 = state_80455__$1;
(statearr_80462_81209[(5)] = ex80459);


var statearr_80463_81210 = state_80455__$1;
(statearr_80463_81210[(1)] = (4));

(statearr_80463_81210[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80456 === (6))){
var inst_80448 = (state_80455[(2)]);
var inst_80449 = com.wsscode.async.async_cljs.throw_err(inst_80448);
var inst_80450 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.assoc,key,inst_80449);
var _ = (function (){var statearr_80464 = state_80455;
(statearr_80464[(4)] = cljs.core.rest((state_80455[(4)])));

return statearr_80464;
})();
var state_80455__$1 = (function (){var statearr_80465 = state_80455;
(statearr_80465[(7)] = inst_80450);

return statearr_80465;
})();
var statearr_80466_81211 = state_80455__$1;
(statearr_80466_81211[(2)] = inst_80449);

(statearr_80466_81211[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto____0 = (function (){
var statearr_80467 = [null,null,null,null,null,null,null,null];
(statearr_80467[(0)] = com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto__);

(statearr_80467[(1)] = (1));

return statearr_80467;
});
var com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto____1 = (function (state_80455){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80455);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80468){var ex__50860__auto__ = e80468;
var statearr_80469_81215 = state_80455;
(statearr_80469_81215[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80455[(4)]))){
var statearr_80470_81216 = state_80455;
(statearr_80470_81216[(1)] = cljs.core.first((state_80455[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81217 = state_80455;
state_80455 = G__81217;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto__ = function(state_80455){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto____1.call(this,state_80455);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto____0;
com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$cached_STAR__$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80471 = f__50893__auto__();
(statearr_80471[(6)] = c__50892__auto__);

return statearr_80471;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var hit = res__75232__auto__;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.assoc,key,hit);

return hit;
}
}
} else {
return (body_fn.cljs$core$IFn$_invoke$arity$0 ? body_fn.cljs$core$IFn$_invoke$arity$0() : body_fn.call(null));
}
});
com.wsscode.pathom.core.cached_async_STAR_ = (function com$wsscode$pathom$core$cached_async_STAR_(env,key,f){
var temp__5733__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(env,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
if(cljs.core.truth_(temp__5733__auto__)){
var cache = temp__5733__auto__;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(cache,cljs.core.update,key,(function (x){
if(cljs.core.truth_(x)){
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-hit","com.wsscode.pathom.core/cache-hit",1851998374),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

return x;
} else {
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-miss","com.wsscode.pathom.core/cache-miss",1311426337),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

var ch__75196__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__50892__auto___81222 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80497){
var state_val_80498 = (state_80497[(1)]);
if((state_val_80498 === (7))){
var inst_80478 = (state_80497[(7)]);
var state_80497__$1 = state_80497;
var statearr_80499_81226 = state_80497__$1;
(statearr_80499_81226[(2)] = inst_80478);

(statearr_80499_81226[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (1))){
var state_80497__$1 = state_80497;
var statearr_80500_81231 = state_80497__$1;
(statearr_80500_81231[(2)] = null);

(statearr_80500_81231[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (4))){
var inst_80472 = (state_80497[(2)]);
var state_80497__$1 = state_80497;
var statearr_80501_81232 = state_80497__$1;
(statearr_80501_81232[(2)] = inst_80472);

(statearr_80501_81232[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (6))){
var inst_80478 = (state_80497[(7)]);
var state_80497__$1 = state_80497;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80497__$1,(9),inst_80478);
} else {
if((state_val_80498 === (3))){
var inst_80488 = (state_80497[(8)]);
var inst_80488__$1 = (state_80497[(2)]);
var inst_80489 = (inst_80488__$1 == null);
var state_80497__$1 = (function (){var statearr_80502 = state_80497;
(statearr_80502[(8)] = inst_80488__$1);

return statearr_80502;
})();
if(cljs.core.truth_(inst_80489)){
var statearr_80503_81235 = state_80497__$1;
(statearr_80503_81235[(1)] = (10));

} else {
var statearr_80504_81236 = state_80497__$1;
(statearr_80504_81236[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (12))){
var inst_80495 = (state_80497[(2)]);
var state_80497__$1 = state_80497;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80497__$1,inst_80495);
} else {
if((state_val_80498 === (2))){
var inst_80478 = (state_80497[(7)]);
var _ = (function (){var statearr_80505 = state_80497;
(statearr_80505[(4)] = cljs.core.cons((5),(state_80497[(4)])));

return statearr_80505;
})();
var inst_80478__$1 = (f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));
var inst_80479 = com.wsscode.async.async_cljs.chan_QMARK_(inst_80478__$1);
var state_80497__$1 = (function (){var statearr_80506 = state_80497;
(statearr_80506[(7)] = inst_80478__$1);

return statearr_80506;
})();
if(inst_80479){
var statearr_80507_81237 = state_80497__$1;
(statearr_80507_81237[(1)] = (6));

} else {
var statearr_80508_81238 = state_80497__$1;
(statearr_80508_81238[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (11))){
var inst_80488 = (state_80497[(8)]);
var inst_80493 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__75196__auto__,inst_80488);
var state_80497__$1 = state_80497;
var statearr_80510_81239 = state_80497__$1;
(statearr_80510_81239[(2)] = inst_80493);

(statearr_80510_81239[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (9))){
var inst_80482 = (state_80497[(2)]);
var state_80497__$1 = state_80497;
var statearr_80511_81240 = state_80497__$1;
(statearr_80511_81240[(2)] = inst_80482);

(statearr_80511_81240[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (5))){
var _ = (function (){var statearr_80512 = state_80497;
(statearr_80512[(4)] = cljs.core.rest((state_80497[(4)])));

return statearr_80512;
})();
var state_80497__$1 = state_80497;
var ex80509 = (state_80497__$1[(2)]);
var statearr_80513_81241 = state_80497__$1;
(statearr_80513_81241[(5)] = ex80509);


var statearr_80514_81242 = state_80497__$1;
(statearr_80514_81242[(1)] = (4));

(statearr_80514_81242[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (10))){
var inst_80491 = cljs.core.async.close_BANG_(ch__75196__auto__);
var state_80497__$1 = state_80497;
var statearr_80515_81243 = state_80497__$1;
(statearr_80515_81243[(2)] = inst_80491);

(statearr_80515_81243[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80498 === (8))){
var inst_80485 = (state_80497[(2)]);
var _ = (function (){var statearr_80516 = state_80497;
(statearr_80516[(4)] = cljs.core.rest((state_80497[(4)])));

return statearr_80516;
})();
var state_80497__$1 = state_80497;
var statearr_80517_81244 = state_80497__$1;
(statearr_80517_81244[(2)] = inst_80485);

(statearr_80517_81244[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____0 = (function (){
var statearr_80518 = [null,null,null,null,null,null,null,null,null];
(statearr_80518[(0)] = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__);

(statearr_80518[(1)] = (1));

return statearr_80518;
});
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____1 = (function (state_80497){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80497);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80519){var ex__50860__auto__ = e80519;
var statearr_80520_81250 = state_80497;
(statearr_80520_81250[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80497[(4)]))){
var statearr_80521_81251 = state_80497;
(statearr_80521_81251[(1)] = cljs.core.first((state_80497[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81252 = state_80497;
state_80497 = G__81252;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__ = function(state_80497){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____1.call(this,state_80497);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____0;
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80522 = f__50893__auto__();
(statearr_80522[(6)] = c__50892__auto___81222);

return statearr_80522;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));


return ch__75196__auto__;
}
}));

return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(cache),key);
} else {
var ch__75196__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__50892__auto___81253 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80548){
var state_val_80549 = (state_80548[(1)]);
if((state_val_80549 === (7))){
var inst_80529 = (state_80548[(7)]);
var state_80548__$1 = state_80548;
var statearr_80550_81254 = state_80548__$1;
(statearr_80550_81254[(2)] = inst_80529);

(statearr_80550_81254[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (1))){
var state_80548__$1 = state_80548;
var statearr_80551_81255 = state_80548__$1;
(statearr_80551_81255[(2)] = null);

(statearr_80551_81255[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (4))){
var inst_80523 = (state_80548[(2)]);
var state_80548__$1 = state_80548;
var statearr_80552_81256 = state_80548__$1;
(statearr_80552_81256[(2)] = inst_80523);

(statearr_80552_81256[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (6))){
var inst_80529 = (state_80548[(7)]);
var state_80548__$1 = state_80548;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80548__$1,(9),inst_80529);
} else {
if((state_val_80549 === (3))){
var inst_80539 = (state_80548[(8)]);
var inst_80539__$1 = (state_80548[(2)]);
var inst_80540 = (inst_80539__$1 == null);
var state_80548__$1 = (function (){var statearr_80553 = state_80548;
(statearr_80553[(8)] = inst_80539__$1);

return statearr_80553;
})();
if(cljs.core.truth_(inst_80540)){
var statearr_80554_81257 = state_80548__$1;
(statearr_80554_81257[(1)] = (10));

} else {
var statearr_80555_81258 = state_80548__$1;
(statearr_80555_81258[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (12))){
var inst_80546 = (state_80548[(2)]);
var state_80548__$1 = state_80548;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80548__$1,inst_80546);
} else {
if((state_val_80549 === (2))){
var inst_80529 = (state_80548[(7)]);
var _ = (function (){var statearr_80556 = state_80548;
(statearr_80556[(4)] = cljs.core.cons((5),(state_80548[(4)])));

return statearr_80556;
})();
var inst_80529__$1 = (f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));
var inst_80530 = com.wsscode.async.async_cljs.chan_QMARK_(inst_80529__$1);
var state_80548__$1 = (function (){var statearr_80557 = state_80548;
(statearr_80557[(7)] = inst_80529__$1);

return statearr_80557;
})();
if(inst_80530){
var statearr_80558_81259 = state_80548__$1;
(statearr_80558_81259[(1)] = (6));

} else {
var statearr_80559_81260 = state_80548__$1;
(statearr_80559_81260[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (11))){
var inst_80539 = (state_80548[(8)]);
var inst_80544 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__75196__auto__,inst_80539);
var state_80548__$1 = state_80548;
var statearr_80561_81264 = state_80548__$1;
(statearr_80561_81264[(2)] = inst_80544);

(statearr_80561_81264[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (9))){
var inst_80533 = (state_80548[(2)]);
var state_80548__$1 = state_80548;
var statearr_80562_81265 = state_80548__$1;
(statearr_80562_81265[(2)] = inst_80533);

(statearr_80562_81265[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (5))){
var _ = (function (){var statearr_80563 = state_80548;
(statearr_80563[(4)] = cljs.core.rest((state_80548[(4)])));

return statearr_80563;
})();
var state_80548__$1 = state_80548;
var ex80560 = (state_80548__$1[(2)]);
var statearr_80564_81266 = state_80548__$1;
(statearr_80564_81266[(5)] = ex80560);


var statearr_80565_81267 = state_80548__$1;
(statearr_80565_81267[(1)] = (4));

(statearr_80565_81267[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (10))){
var inst_80542 = cljs.core.async.close_BANG_(ch__75196__auto__);
var state_80548__$1 = state_80548;
var statearr_80566_81268 = state_80548__$1;
(statearr_80566_81268[(2)] = inst_80542);

(statearr_80566_81268[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80549 === (8))){
var inst_80536 = (state_80548[(2)]);
var _ = (function (){var statearr_80567 = state_80548;
(statearr_80567[(4)] = cljs.core.rest((state_80548[(4)])));

return statearr_80567;
})();
var state_80548__$1 = state_80548;
var statearr_80568_81269 = state_80548__$1;
(statearr_80568_81269[(2)] = inst_80536);

(statearr_80568_81269[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____0 = (function (){
var statearr_80569 = [null,null,null,null,null,null,null,null,null];
(statearr_80569[(0)] = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__);

(statearr_80569[(1)] = (1));

return statearr_80569;
});
var com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____1 = (function (state_80548){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80548);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80570){var ex__50860__auto__ = e80570;
var statearr_80571_81271 = state_80548;
(statearr_80571_81271[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80548[(4)]))){
var statearr_80572_81273 = state_80548;
(statearr_80572_81273[(1)] = cljs.core.first((state_80548[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81275 = state_80548;
state_80548 = G__81275;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__ = function(state_80548){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____1.call(this,state_80548);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____0;
com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$cached_async_STAR__$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80573 = f__50893__auto__();
(statearr_80573[(6)] = c__50892__auto___81253);

return statearr_80573;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));


return ch__75196__auto__;
}
});
com.wsscode.pathom.core.cached_async = (function com$wsscode$pathom$core$cached_async(p__80574,key,f){
var map__80575 = p__80574;
var map__80575__$1 = (((((!((map__80575 == null))))?(((((map__80575.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80575.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80575):map__80575);
var env = map__80575__$1;
var async_request_cache_ch = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80575__$1,new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch","com.wsscode.pathom.core/async-request-cache-ch",-1864666369));
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80575__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
if(cljs.core.truth_(async_request_cache_ch)){
if(cljs.core.contains_QMARK_(cljs.core.deref(request_cache),key)){
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(request_cache),key);
} else {
var out = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(async_request_cache_ch,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [env,key,f,out], null));

var ch__75196__auto__ = cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();
var c__50892__auto___81276 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80598){
var state_val_80599 = (state_80598[(1)]);
if((state_val_80599 === (7))){
var inst_80584 = (state_80598[(2)]);
var state_80598__$1 = state_80598;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80598__$1,(6),inst_80584);
} else {
if((state_val_80599 === (1))){
var state_80598__$1 = state_80598;
var statearr_80600_81284 = state_80598__$1;
(statearr_80600_81284[(2)] = null);

(statearr_80600_81284[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80599 === (4))){
var inst_80577 = (state_80598[(2)]);
var state_80598__$1 = state_80598;
var statearr_80601_81285 = state_80598__$1;
(statearr_80601_81285[(2)] = inst_80577);

(statearr_80601_81285[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80599 === (6))){
var inst_80586 = (state_80598[(2)]);
var _ = (function (){var statearr_80602 = state_80598;
(statearr_80602[(4)] = cljs.core.rest((state_80598[(4)])));

return statearr_80602;
})();
var state_80598__$1 = state_80598;
var statearr_80603_81286 = state_80598__$1;
(statearr_80603_81286[(2)] = inst_80586);

(statearr_80603_81286[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80599 === (3))){
var inst_80589 = (state_80598[(7)]);
var inst_80589__$1 = (state_80598[(2)]);
var inst_80590 = (inst_80589__$1 == null);
var state_80598__$1 = (function (){var statearr_80604 = state_80598;
(statearr_80604[(7)] = inst_80589__$1);

return statearr_80604;
})();
if(cljs.core.truth_(inst_80590)){
var statearr_80605_81287 = state_80598__$1;
(statearr_80605_81287[(1)] = (8));

} else {
var statearr_80606_81288 = state_80598__$1;
(statearr_80606_81288[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80599 === (2))){
var _ = (function (){var statearr_80608 = state_80598;
(statearr_80608[(4)] = cljs.core.cons((5),(state_80598[(4)])));

return statearr_80608;
})();
var state_80598__$1 = state_80598;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80598__$1,(7),out);
} else {
if((state_val_80599 === (9))){
var inst_80589 = (state_80598[(7)]);
var inst_80594 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(ch__75196__auto__,inst_80589);
var state_80598__$1 = state_80598;
var statearr_80609_81289 = state_80598__$1;
(statearr_80609_81289[(2)] = inst_80594);

(statearr_80609_81289[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80599 === (5))){
var _ = (function (){var statearr_80610 = state_80598;
(statearr_80610[(4)] = cljs.core.rest((state_80598[(4)])));

return statearr_80610;
})();
var state_80598__$1 = state_80598;
var ex80607 = (state_80598__$1[(2)]);
var statearr_80611_81290 = state_80598__$1;
(statearr_80611_81290[(5)] = ex80607);


var statearr_80612_81291 = state_80598__$1;
(statearr_80612_81291[(1)] = (4));

(statearr_80612_81291[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80599 === (10))){
var inst_80596 = (state_80598[(2)]);
var state_80598__$1 = state_80598;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80598__$1,inst_80596);
} else {
if((state_val_80599 === (8))){
var inst_80592 = cljs.core.async.close_BANG_(ch__75196__auto__);
var state_80598__$1 = state_80598;
var statearr_80613_81292 = state_80598__$1;
(statearr_80613_81292[(2)] = inst_80592);

(statearr_80613_81292[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto____0 = (function (){
var statearr_80614 = [null,null,null,null,null,null,null,null];
(statearr_80614[(0)] = com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto__);

(statearr_80614[(1)] = (1));

return statearr_80614;
});
var com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto____1 = (function (state_80598){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80598);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80615){var ex__50860__auto__ = e80615;
var statearr_80616_81293 = state_80598;
(statearr_80616_81293[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80598[(4)]))){
var statearr_80617_81295 = state_80598;
(statearr_80617_81295[(1)] = cljs.core.first((state_80598[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81296 = state_80598;
state_80598 = G__81296;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto__ = function(state_80598){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto____1.call(this,state_80598);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$cached_async_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80618 = f__50893__auto__();
(statearr_80618[(6)] = c__50892__auto___81276);

return statearr_80618;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));


return ch__75196__auto__;
}
} else {
return com.wsscode.pathom.core.cached_async_STAR_(env,key,f);
}
});
com.wsscode.pathom.core.request_cache_async_loop = (function com$wsscode$pathom$core$request_cache_async_loop(ch){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80640){
var state_val_80641 = (state_80640[(1)]);
if((state_val_80641 === (1))){
var state_80640__$1 = state_80640;
var statearr_80642_81301 = state_80640__$1;
(statearr_80642_81301[(2)] = null);

(statearr_80642_81301[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80641 === (2))){
var state_80640__$1 = state_80640;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80640__$1,(4),ch);
} else {
if((state_val_80641 === (3))){
var inst_80638 = (state_80640[(2)]);
var state_80640__$1 = state_80640;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80640__$1,inst_80638);
} else {
if((state_val_80641 === (4))){
var inst_80621 = (state_80640[(7)]);
var inst_80621__$1 = (state_80640[(2)]);
var state_80640__$1 = (function (){var statearr_80643 = state_80640;
(statearr_80643[(7)] = inst_80621__$1);

return statearr_80643;
})();
if(cljs.core.truth_(inst_80621__$1)){
var statearr_80644_81305 = state_80640__$1;
(statearr_80644_81305[(1)] = (5));

} else {
var statearr_80645_81306 = state_80640__$1;
(statearr_80645_81306[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80641 === (5))){
var inst_80621 = (state_80640[(7)]);
var inst_80626 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_80621,(0),null);
var inst_80627 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_80621,(1),null);
var inst_80628 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_80621,(2),null);
var inst_80629 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_80621,(3),null);
var inst_80630 = com.wsscode.pathom.core.cached_async_STAR_(inst_80626,inst_80627,inst_80628);
var state_80640__$1 = state_80640;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_80640__$1,(8),inst_80629,inst_80630);
} else {
if((state_val_80641 === (6))){
var state_80640__$1 = state_80640;
var statearr_80646_81307 = state_80640__$1;
(statearr_80646_81307[(2)] = null);

(statearr_80646_81307[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80641 === (7))){
var inst_80636 = (state_80640[(2)]);
var state_80640__$1 = state_80640;
var statearr_80647_81310 = state_80640__$1;
(statearr_80647_81310[(2)] = inst_80636);

(statearr_80647_81310[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80641 === (8))){
var inst_80632 = (state_80640[(2)]);
var state_80640__$1 = (function (){var statearr_80648 = state_80640;
(statearr_80648[(8)] = inst_80632);

return statearr_80648;
})();
var statearr_80649_81314 = state_80640__$1;
(statearr_80649_81314[(2)] = null);

(statearr_80649_81314[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto____0 = (function (){
var statearr_80650 = [null,null,null,null,null,null,null,null,null];
(statearr_80650[(0)] = com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto__);

(statearr_80650[(1)] = (1));

return statearr_80650;
});
var com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto____1 = (function (state_80640){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80640);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80651){var ex__50860__auto__ = e80651;
var statearr_80652_81316 = state_80640;
(statearr_80652_81316[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80640[(4)]))){
var statearr_80653_81320 = state_80640;
(statearr_80653_81320[(1)] = cljs.core.first((state_80640[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81321 = state_80640;
state_80640 = G__81321;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto__ = function(state_80640){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto____1.call(this,state_80640);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$request_cache_async_loop_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80654 = f__50893__auto__();
(statearr_80654[(6)] = c__50892__auto__);

return statearr_80654;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
});
com.wsscode.pathom.core.cache_hit = (function com$wsscode$pathom$core$cache_hit(p__80655,key,value){
var map__80656 = p__80655;
var map__80656__$1 = (((((!((map__80656 == null))))?(((((map__80656.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80656.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80656):map__80656);
var env = map__80656__$1;
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80656__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
com.wsscode.pathom.trace.trace(env,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.trace","event","com.wsscode.pathom.trace/event",1287398983),new cljs.core.Keyword("com.wsscode.pathom.core","cache-miss","com.wsscode.pathom.core/cache-miss",1311426337),new cljs.core.Keyword("com.wsscode.pathom.core","cache-key","com.wsscode.pathom.core/cache-key",246159991),key], null));

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(request_cache,cljs.core.assoc,key,value);

return value;
});
com.wsscode.pathom.core.cache_contains_QMARK_ = (function com$wsscode$pathom$core$cache_contains_QMARK_(p__80658,key){
var map__80659 = p__80658;
var map__80659__$1 = (((((!((map__80659 == null))))?(((((map__80659.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80659.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80659):map__80659);
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80659__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
return cljs.core.contains_QMARK_(cljs.core.deref(request_cache),key);
});
com.wsscode.pathom.core.cache_read = (function com$wsscode$pathom$core$cache_read(p__80661,key){
var map__80662 = p__80661;
var map__80662__$1 = (((((!((map__80662 == null))))?(((((map__80662.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80662.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80662):map__80662);
var request_cache = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80662__$1,new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617));
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(request_cache),key);
});
com.wsscode.pathom.core.wrap_add_path = (function com$wsscode$pathom$core$wrap_add_path(reader){
return (function (p__80664){
var map__80665 = p__80664;
var map__80665__$1 = (((((!((map__80665 == null))))?(((((map__80665.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80665.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80665):map__80665);
var env = map__80665__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80665__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
var G__80667 = cljs.core.update.cljs$core$IFn$_invoke$arity$4(env,new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.conj,cljs.core.PersistentVector.EMPTY),new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(ast));
return (reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(G__80667) : reader.call(null,G__80667));
});
});
com.wsscode.pathom.core.group_plugins_by_action = (function com$wsscode$pathom$core$group_plugins_by_action(plugins){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (g,p){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (g__$1,p__80668){
var vec__80669 = p__80668;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80669,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__80669,(1),null);
return cljs.core.update.cljs$core$IFn$_invoke$arity$4(g__$1,k,cljs.core.fnil.cljs$core$IFn$_invoke$arity$2(cljs.core.conj,cljs.core.PersistentVector.EMPTY),v);
}),g,p);
}),cljs.core.PersistentArrayMap.EMPTY,plugins);
});
com.wsscode.pathom.core.wrap_normalize_env = (function com$wsscode$pathom$core$wrap_normalize_env(var_args){
var G__80673 = arguments.length;
switch (G__80673) {
case 1:
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$1 = (function (parser){
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(parser,cljs.core.PersistentVector.EMPTY);
}));

(com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2 = (function (parser,plugins){
return (function() {
var com$wsscode$pathom$core$wrap_normalize_env_internal = null;
var com$wsscode$pathom$core$wrap_normalize_env_internal__2 = (function (env,tx){
return com$wsscode$pathom$core$wrap_normalize_env_internal.cljs$core$IFn$_invoke$arity$3(env,tx,null);
});
var com$wsscode$pathom$core$wrap_normalize_env_internal__3 = (function (env,tx,target){
var G__80674 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","request-cache","com.wsscode.pathom.core/request-cache",1469884617),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","entity-key","com.wsscode.pathom.core/entity-key",1494103249),new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),new cljs.core.Keyword("com.wsscode.pathom.core","entity-path-cache","com.wsscode.pathom.core/entity-path-cache",-1017384397),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [">",null], null), null),new cljs.core.Keyword("com.wsscode.pathom.core","parent-query","com.wsscode.pathom.core/parent-query",1683392594),tx,new cljs.core.Keyword("com.wsscode.pathom.core","root-query","com.wsscode.pathom.core/root-query",-100266682),tx], null),env,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword("com.wsscode.pathom.core","plugin-actions","com.wsscode.pathom.core/plugin-actions",-876552970),com.wsscode.pathom.core.group_plugins_by_action(plugins),new cljs.core.Keyword("com.wsscode.pathom.core","plugins","com.wsscode.pathom.core/plugins",-2128476796),plugins,new cljs.core.Keyword(null,"target","target",253001721),target], null)], 0));
var G__80675 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__80674,G__80675) : parser.call(null,G__80674,G__80675));
});
com$wsscode$pathom$core$wrap_normalize_env_internal = function(env,tx,target){
switch(arguments.length){
case 2:
return com$wsscode$pathom$core$wrap_normalize_env_internal__2.call(this,env,tx);
case 3:
return com$wsscode$pathom$core$wrap_normalize_env_internal__3.call(this,env,tx,target);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_normalize_env_internal.cljs$core$IFn$_invoke$arity$2 = com$wsscode$pathom$core$wrap_normalize_env_internal__2;
com$wsscode$pathom$core$wrap_normalize_env_internal.cljs$core$IFn$_invoke$arity$3 = com$wsscode$pathom$core$wrap_normalize_env_internal__3;
return com$wsscode$pathom$core$wrap_normalize_env_internal;
})()
}));

(com.wsscode.pathom.core.wrap_normalize_env.cljs$lang$maxFixedArity = 2);

com.wsscode.pathom.core.wrap_parallel_setup = (function com$wsscode$pathom$core$wrap_parallel_setup(parser){
return (function com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal(env,tx){
var signal = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
var res__75232__auto__ = (function (){var G__80676 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(env,new cljs.core.Keyword("com.wsscode.pathom.parser","done-signal*","com.wsscode.pathom.parser/done-signal*",2069309538),signal,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword("com.wsscode.pathom.parser","active-paths","com.wsscode.pathom.parser/active-paths",457466204),cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentHashSet.EMPTY),new cljs.core.Keyword("com.wsscode.pathom.core","path","com.wsscode.pathom.core/path",-1532419661),cljs.core.PersistentVector.EMPTY], 0));
var G__80677 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__80676,G__80677) : parser.call(null,G__80676,G__80677));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80692){
var state_val_80693 = (state_80692[(1)]);
if((state_val_80693 === (1))){
var state_80692__$1 = state_80692;
var statearr_80694_81340 = state_80692__$1;
(statearr_80694_81340[(2)] = null);

(statearr_80694_81340[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80693 === (2))){
var _ = (function (){var statearr_80695 = state_80692;
(statearr_80695[(4)] = cljs.core.cons((5),(state_80692[(4)])));

return statearr_80695;
})();
var state_80692__$1 = state_80692;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80692__$1,(6),res__75232__auto__);
} else {
if((state_val_80693 === (3))){
var inst_80690 = (state_80692[(2)]);
var state_80692__$1 = state_80692;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80692__$1,inst_80690);
} else {
if((state_val_80693 === (4))){
var inst_80678 = (state_80692[(2)]);
var state_80692__$1 = state_80692;
var statearr_80697_81343 = state_80692__$1;
(statearr_80697_81343[(2)] = inst_80678);

(statearr_80697_81343[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80693 === (5))){
var _ = (function (){var statearr_80698 = state_80692;
(statearr_80698[(4)] = cljs.core.rest((state_80692[(4)])));

return statearr_80698;
})();
var state_80692__$1 = state_80692;
var ex80696 = (state_80692__$1[(2)]);
var statearr_80699_81345 = state_80692__$1;
(statearr_80699_81345[(5)] = ex80696);


var statearr_80700_81346 = state_80692__$1;
(statearr_80700_81346[(1)] = (4));

(statearr_80700_81346[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80693 === (6))){
var inst_80685 = (state_80692[(2)]);
var inst_80686 = com.wsscode.async.async_cljs.throw_err(inst_80685);
var inst_80687 = cljs.core.reset_BANG_(signal,true);
var _ = (function (){var statearr_80701 = state_80692;
(statearr_80701[(4)] = cljs.core.rest((state_80692[(4)])));

return statearr_80701;
})();
var state_80692__$1 = (function (){var statearr_80702 = state_80692;
(statearr_80702[(7)] = inst_80687);

return statearr_80702;
})();
var statearr_80703_81350 = state_80692__$1;
(statearr_80703_81350[(2)] = inst_80686);

(statearr_80703_81350[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto____0 = (function (){
var statearr_80704 = [null,null,null,null,null,null,null,null];
(statearr_80704[(0)] = com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto__);

(statearr_80704[(1)] = (1));

return statearr_80704;
});
var com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto____1 = (function (state_80692){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80692);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80705){var ex__50860__auto__ = e80705;
var statearr_80706_81352 = state_80692;
(statearr_80706_81352[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80692[(4)]))){
var statearr_80707_81353 = state_80692;
(statearr_80707_81353[(1)] = cljs.core.first((state_80692[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81354 = state_80692;
state_80692 = G__81354;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto__ = function(state_80692){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto____1.call(this,state_80692);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$wrap_parallel_setup_$_wrap_async_done_signal_internal_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80708 = f__50893__auto__();
(statearr_80708[(6)] = c__50892__auto__);

return statearr_80708;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var res = res__75232__auto__;
cljs.core.reset_BANG_(signal,true);

return res;
}
});
});
com.wsscode.pathom.core.wrap_setup_async_cache = (function com$wsscode$pathom$core$wrap_setup_async_cache(parser){
return (function com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal(env,tx){
var async_cache_ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.get.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch-size","com.wsscode.pathom.core/async-request-cache-ch-size",-437531159),(1024)));
com.wsscode.pathom.core.request_cache_async_loop(async_cache_ch);

var res__75232__auto__ = (function (){var G__80709 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.Keyword("com.wsscode.pathom.core","async-request-cache-ch","com.wsscode.pathom.core/async-request-cache-ch",-1864666369),async_cache_ch);
var G__80710 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__80709,G__80710) : parser.call(null,G__80709,G__80710));
})();
if(com.wsscode.async.async_cljs.chan_QMARK_(res__75232__auto__)){
var c__50892__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__50893__auto__ = (function (){var switch__50856__auto__ = (function (state_80725){
var state_val_80726 = (state_80725[(1)]);
if((state_val_80726 === (1))){
var state_80725__$1 = state_80725;
var statearr_80727_81355 = state_80725__$1;
(statearr_80727_81355[(2)] = null);

(statearr_80727_81355[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80726 === (2))){
var _ = (function (){var statearr_80728 = state_80725;
(statearr_80728[(4)] = cljs.core.cons((5),(state_80725[(4)])));

return statearr_80728;
})();
var state_80725__$1 = state_80725;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_80725__$1,(6),res__75232__auto__);
} else {
if((state_val_80726 === (3))){
var inst_80723 = (state_80725[(2)]);
var state_80725__$1 = state_80725;
return cljs.core.async.impl.ioc_helpers.return_chan(state_80725__$1,inst_80723);
} else {
if((state_val_80726 === (4))){
var inst_80711 = (state_80725[(2)]);
var state_80725__$1 = state_80725;
var statearr_80730_81359 = state_80725__$1;
(statearr_80730_81359[(2)] = inst_80711);

(statearr_80730_81359[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80726 === (5))){
var _ = (function (){var statearr_80731 = state_80725;
(statearr_80731[(4)] = cljs.core.rest((state_80725[(4)])));

return statearr_80731;
})();
var state_80725__$1 = state_80725;
var ex80729 = (state_80725__$1[(2)]);
var statearr_80732_81361 = state_80725__$1;
(statearr_80732_81361[(5)] = ex80729);


var statearr_80733_81362 = state_80725__$1;
(statearr_80733_81362[(1)] = (4));

(statearr_80733_81362[(5)] = null);



return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_80726 === (6))){
var inst_80718 = (state_80725[(2)]);
var inst_80719 = com.wsscode.async.async_cljs.throw_err(inst_80718);
var inst_80720 = cljs.core.async.close_BANG_(async_cache_ch);
var _ = (function (){var statearr_80734 = state_80725;
(statearr_80734[(4)] = cljs.core.rest((state_80725[(4)])));

return statearr_80734;
})();
var state_80725__$1 = (function (){var statearr_80735 = state_80725;
(statearr_80735[(7)] = inst_80720);

return statearr_80735;
})();
var statearr_80736_81364 = state_80725__$1;
(statearr_80736_81364[(2)] = inst_80719);

(statearr_80736_81364[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
});
return (function() {
var com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto__ = null;
var com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto____0 = (function (){
var statearr_80737 = [null,null,null,null,null,null,null,null];
(statearr_80737[(0)] = com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto__);

(statearr_80737[(1)] = (1));

return statearr_80737;
});
var com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto____1 = (function (state_80725){
while(true){
var ret_value__50858__auto__ = (function (){try{while(true){
var result__50859__auto__ = switch__50856__auto__(state_80725);
if(cljs.core.keyword_identical_QMARK_(result__50859__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__50859__auto__;
}
break;
}
}catch (e80738){var ex__50860__auto__ = e80738;
var statearr_80739_81365 = state_80725;
(statearr_80739_81365[(2)] = ex__50860__auto__);


if(cljs.core.seq((state_80725[(4)]))){
var statearr_80740_81366 = state_80725;
(statearr_80740_81366[(1)] = cljs.core.first((state_80725[(4)])));

} else {
throw ex__50860__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__50858__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__81367 = state_80725;
state_80725 = G__81367;
continue;
} else {
return ret_value__50858__auto__;
}
break;
}
});
com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto__ = function(state_80725){
switch(arguments.length){
case 0:
return com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto____0.call(this);
case 1:
return com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto____1.call(this,state_80725);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$0 = com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto____0;
com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto__.cljs$core$IFn$_invoke$arity$1 = com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto____1;
return com$wsscode$pathom$core$wrap_setup_async_cache_$_wrap_setup_async_cache_internal_$_state_machine__50857__auto__;
})()
})();
var state__50894__auto__ = (function (){var statearr_80741 = f__50893__auto__();
(statearr_80741[(6)] = c__50892__auto__);

return statearr_80741;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__50894__auto__);
}));

return c__50892__auto__;
} else {
var res = res__75232__auto__;
cljs.core.async.close_BANG_(async_cache_ch);

return res;
}
});
});
com.wsscode.pathom.core.wrap_reduce_params = (function com$wsscode$pathom$core$wrap_reduce_params(reader){
return (function() {
var G__81368 = null;
var G__81368__1 = (function (env){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),(reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env))], null);
});
var G__81368__3 = (function (env,_,___$1){
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),(reader.cljs$core$IFn$_invoke$arity$1 ? reader.cljs$core$IFn$_invoke$arity$1(env) : reader.call(null,env))], null);
});
G__81368 = function(env,_,___$1){
switch(arguments.length){
case 1:
return G__81368__1.call(this,env);
case 3:
return G__81368__3.call(this,env,_,___$1);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__81368.cljs$core$IFn$_invoke$arity$1 = G__81368__1;
G__81368.cljs$core$IFn$_invoke$arity$3 = G__81368__3;
return G__81368;
})()
});
com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_ = (function com$wsscode$pathom$core$pathom_read_SINGLEQUOTE_(p__80742){
var map__80743 = p__80742;
var map__80743__$1 = (((((!((map__80743 == null))))?(((((map__80743.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80743.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80743):map__80743);
var env = map__80743__$1;
var reader = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80743__$1,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410));
return com.wsscode.pathom.core.read_from(env,reader);
});
com.wsscode.pathom.core.apply_plugins = (function com$wsscode$pathom$core$apply_plugins(var_args){
var args__4742__auto__ = [];
var len__4736__auto___81369 = arguments.length;
var i__4737__auto___81370 = (0);
while(true){
if((i__4737__auto___81370 < len__4736__auto___81369)){
args__4742__auto__.push((arguments[i__4737__auto___81370]));

var G__81380 = (i__4737__auto___81370 + (1));
i__4737__auto___81370 = G__81380;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic = (function (v,plugins,key,params){
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (x,plugin){
var f = cljs.core.get.cljs$core$IFn$_invoke$arity$2(plugin,key);
if(cljs.core.truth_(f)){
return cljs.core.apply.cljs$core$IFn$_invoke$arity$3(f,x,params);
} else {
return x;
}
}),v,plugins);
}));

(com.wsscode.pathom.core.apply_plugins.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(com.wsscode.pathom.core.apply_plugins.cljs$lang$applyTo = (function (seq80745){
var G__80746 = cljs.core.first(seq80745);
var seq80745__$1 = cljs.core.next(seq80745);
var G__80747 = cljs.core.first(seq80745__$1);
var seq80745__$2 = cljs.core.next(seq80745__$1);
var G__80748 = cljs.core.first(seq80745__$2);
var seq80745__$3 = cljs.core.next(seq80745__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__80746,G__80747,G__80748,seq80745__$3);
}));

com.wsscode.pathom.core.exec_plugin_actions = (function com$wsscode$pathom$core$exec_plugin_actions(var_args){
var args__4742__auto__ = [];
var len__4736__auto___81381 = arguments.length;
var i__4737__auto___81382 = (0);
while(true){
if((i__4737__auto___81382 < len__4736__auto___81381)){
args__4742__auto__.push((arguments[i__4737__auto___81382]));

var G__81383 = (i__4737__auto___81382 + (1));
i__4737__auto___81382 = G__81383;
continue;
} else {
}
break;
}

var argseq__4743__auto__ = ((((3) < args__4742__auto__.length))?(new cljs.core.IndexedSeq(args__4742__auto__.slice((3)),(0),null)):null);
return com.wsscode.pathom.core.exec_plugin_actions.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__4743__auto__);
});

(com.wsscode.pathom.core.exec_plugin_actions.cljs$core$IFn$_invoke$arity$variadic = (function (env,key,v,args){
var plugins = cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(env,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword("com.wsscode.pathom.core","plugin-actions","com.wsscode.pathom.core/plugin-actions",-876552970),key], null),cljs.core.PersistentVector.EMPTY);
var augmented_v = cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (x,f){
return (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(x) : f.call(null,x));
}),v,plugins);
return cljs.core.apply.cljs$core$IFn$_invoke$arity$2(augmented_v,args);
}));

(com.wsscode.pathom.core.exec_plugin_actions.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(com.wsscode.pathom.core.exec_plugin_actions.cljs$lang$applyTo = (function (seq80749){
var G__80750 = cljs.core.first(seq80749);
var seq80749__$1 = cljs.core.next(seq80749);
var G__80751 = cljs.core.first(seq80749__$1);
var seq80749__$2 = cljs.core.next(seq80749__$1);
var G__80752 = cljs.core.first(seq80749__$2);
var seq80749__$3 = cljs.core.next(seq80749__$2);
var self__4723__auto__ = this;
return self__4723__auto__.cljs$core$IFn$_invoke$arity$variadic(G__80750,G__80751,G__80752,seq80749__$3);
}));

com.wsscode.pathom.core.easy_plugins = (function com$wsscode$pathom$core$easy_plugins(p__80753){
var map__80754 = p__80753;
var map__80754__$1 = (((((!((map__80754 == null))))?(((((map__80754.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80754.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80754):map__80754);
var plugins = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80754__$1,new cljs.core.Keyword("com.wsscode.pathom.core","plugins","com.wsscode.pathom.core/plugins",-2128476796));
var env = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80754__$1,new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378));
var G__80756 = plugins;
var G__80756__$1 = ((cljs.core.fn_QMARK_(env))?cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.env_wrap_plugin(env)], null),G__80756):G__80756);
if(cljs.core.map_QMARK_(env)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.env_plugin(env)], null),G__80756__$1);
} else {
return G__80756__$1;
}
});
com.wsscode.pathom.core.settings_mutation = (function com$wsscode$pathom$core$settings_mutation(settings){
var or__4126__auto__ = new cljs.core.Keyword("com.wsscode.pathom.core","mutate","com.wsscode.pathom.core/mutate",-2086097173).cljs$core$IFn$_invoke$arity$1(settings);
if(cljs.core.truth_(or__4126__auto__)){
return or__4126__auto__;
} else {
return new cljs.core.Keyword(null,"mutate","mutate",1422419038).cljs$core$IFn$_invoke$arity$1(settings);
}
});
com.wsscode.pathom.core.wrap_setup_env = (function com$wsscode$pathom$core$wrap_setup_env(parser,env_SINGLEQUOTE_){
return (function com$wsscode$pathom$core$wrap_setup_env_$_wrap_setup_env_internal(env,tx){
var G__80757 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([env,env_SINGLEQUOTE_], 0));
var G__80758 = tx;
return (parser.cljs$core$IFn$_invoke$arity$2 ? parser.cljs$core$IFn$_invoke$arity$2(G__80757,G__80758) : parser.call(null,G__80757,G__80758));
});
});
/**
 * Create a new pathom serial parser, this parser is capable of waiting for core.async
 *   to continue processing, allowing async operations to happen during the parsing.
 * 
 *   Options to tune the parser:
 * 
 *   ::p/env - Use this key to provide a default environment for the parser. This is a sugar
 *   to use the p/env-plugin.
 * 
 *   ::p/mutate - A mutate function that will be called to run mutations, this function
 *   must have the signature: (mutate env key params)
 * 
 *   ::p/plugins - A vector with plugins.
 */
com.wsscode.pathom.core.parser = (function com$wsscode$pathom$core$parser(settings){
var plugins = com.wsscode.pathom.core.easy_plugins(settings);
var mutate = com.wsscode.pathom.core.settings_mutation(settings);
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.wrap_setup_env(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.parser.parser(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"read","read",1140058661),com.wsscode.pathom.core.wrap_add_path(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261))),new cljs.core.Keyword(null,"mutate","mutate",1422419038),(cljs.core.truth_(mutate)?com.wsscode.pathom.core.apply_plugins(mutate,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202)):null)], null)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser2","com.wsscode.pathom.core/wrap-parser2",776559497),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([settings], 0)),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),false], null)),plugins);
});
/**
 * Create a new pathom async parser, this parser is serial and capable of waiting for core.async
 *   to continue processing, allowing async operations to happen during the parsing.
 * 
 *   Options to tune the parser:
 * 
 *   ::p/env - Use this key to provide a default environment for the parser. This is a sugar
 *   to use the p/env-plugin.
 * 
 *   ::p/mutate - A mutate function that will be called to run mutations, this function
 *   must have the signature: (mutate env key params)
 * 
 *   ::p/plugins - A vector with plugins.
 */
com.wsscode.pathom.core.async_parser = (function com$wsscode$pathom$core$async_parser(settings){
var plugins = com.wsscode.pathom.core.easy_plugins(settings);
var mutate = com.wsscode.pathom.core.settings_mutation(settings);
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.wrap_setup_async_cache(com.wsscode.pathom.core.wrap_setup_env(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.parser.async_parser(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"read","read",1140058661),com.wsscode.pathom.core.wrap_add_path(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261))),new cljs.core.Keyword(null,"mutate","mutate",1422419038),(cljs.core.truth_(mutate)?com.wsscode.pathom.core.apply_plugins(mutate,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202)):null)], null)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser2","com.wsscode.pathom.core/wrap-parser2",776559497),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([settings], 0)),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),true], null))),plugins);
});
/**
 * Create a new pathom parallel parser, this parser is capable of coordinating parallel
 *   data fetch. This also works as an async parser and will handle core async channels
 *   properly.
 * 
 *   Options to tune the parser:
 * 
 *   ::p/env - Use this key to provide a default environment for the parser. This is a sugar
 *   to use the p/env-plugin.
 * 
 *   ::p/mutate - A mutate function that will be called to run mutations, this function
 *   must have the signature: (mutate env key params)
 * 
 *   ::p/plugins - A vector with plugins.
 * 
 *   ::p/async-request-cache-ch-size - Pathom uses internally a queue to avoid concurrency
 *   issues with concurrency, each request gets its own channel, so you can consider this
 *   size needs to accommodate the max parallelism for a single query. Default: 1024
 * 
 *   ::pp/external-wait-ignore-timeout - Sometimes external waits get stuck because a concurrency
 *   problem, this timeout will ignore external waits after some time so the request can
 *   go on. Default: 3000
 * 
 *   ::pp/max-key-iterations - there is a loop that happens when processing attributes in
 *   parallel, this loop will cause multiple iterations to happen in order for a single
 *   attribute to be processed, but in some conditions this loop can go indefinitely, to
 *   prevent this situation this option allows to control the max number of iterations, after
 *   that it will give up on processing that attribute. Default: 10
 * 
 *   ::pp/key-process-timeout - Max time allowed to run the full query. This is a cascading
 *   timeout, the first level will have the total amount. Default: 60000
 * 
 *   ::pp/processing-recheck-timer - Periodic time to run a checker to verify no parts are
 *   stuck during the processing, when nil the feature is disabled. Default: nil
 */
com.wsscode.pathom.core.parallel_parser = (function com$wsscode$pathom$core$parallel_parser(settings){
var plugins = com.wsscode.pathom.core.easy_plugins(settings);
var mutate = com.wsscode.pathom.core.settings_mutation(settings);
return com.wsscode.pathom.core.wrap_normalize_env.cljs$core$IFn$_invoke$arity$2(com.wsscode.pathom.core.wrap_setup_async_cache(com.wsscode.pathom.core.wrap_parallel_setup(com.wsscode.pathom.core.wrap_setup_env(com.wsscode.pathom.core.apply_plugins.cljs$core$IFn$_invoke$arity$variadic(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.parser.parallel_parser(new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"read","read",1140058661),com.wsscode.pathom.core.wrap_add_path(com.wsscode.pathom.core.apply_plugins(com.wsscode.pathom.core.pathom_read_SINGLEQUOTE_,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-read","com.wsscode.pathom.core/wrap-read",-904142261))),new cljs.core.Keyword(null,"mutate","mutate",1422419038),(cljs.core.truth_(mutate)?com.wsscode.pathom.core.apply_plugins(mutate,plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-mutate","com.wsscode.pathom.core/wrap-mutate",989863202)):null),new cljs.core.Keyword(null,"add-error","add-error",-1195330235),com.wsscode.pathom.core.add_error], null)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser","com.wsscode.pathom.core/wrap-parser",131527793)),plugins,new cljs.core.Keyword("com.wsscode.pathom.core","wrap-parser2","com.wsscode.pathom.core/wrap-parser2",776559497),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([settings], 0)),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","async-parser?","com.wsscode.pathom.core/async-parser?",920199905),true], null)))),plugins);
});
/**
 * Starting from a map, do a EQL selection on that map. Think of this function as
 *   a power up version of select-keys, but supporting nested selections and placeholders
 *   using the default `>` namespace.
 * 
 *   Example:
 *   (p/map-select {:foo "bar" :deep {:a 1 :b 2}} [{:deep [:a]}])
 *   => {:deep {:a 1}}
 */
com.wsscode.pathom.core.map_select = (function (){var parser = com.wsscode.pathom.core.parser(new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","env","com.wsscode.pathom.core/env",602275378),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.map_reader,com.wsscode.pathom.core.env_placeholder_reader], null),new cljs.core.Keyword("com.wsscode.pathom.core","placeholder-prefixes","com.wsscode.pathom.core/placeholder-prefixes",-1362240644),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [">",null], null), null)], null),new cljs.core.Keyword("com.wsscode.pathom.core","plugins","com.wsscode.pathom.core/plugins",-2128476796),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [com.wsscode.pathom.core.elide_special_outputs_plugin], null)], null));
return (function (map,selection){
return parser(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("com.wsscode.pathom.core","entity","com.wsscode.pathom.core/entity",884706031),map], null),selection);
});
})();
com.wsscode.pathom.core.continue$ = com.wsscode.pathom.core.join;
com.wsscode.pathom.core.continue_seq = com.wsscode.pathom.core.join_seq;
/**
 * DEPRECATED: use env-placeholder-reader instead.
 * 
 *   Produces a reader that will respond to any keyword with the namespace ns. The join node logical level stays the same
 *   as the parent where the placeholder node is requested.
 */
com.wsscode.pathom.core.placeholder_reader = (function com$wsscode$pathom$core$placeholder_reader(var_args){
var G__80760 = arguments.length;
switch (G__80760) {
case 0:
return com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$0 = (function (){
return com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$1(">");
}));

(com.wsscode.pathom.core.placeholder_reader.cljs$core$IFn$_invoke$arity$1 = (function (ns){
return (function (p__80761){
var map__80762 = p__80761;
var map__80762__$1 = (((((!((map__80762 == null))))?(((((map__80762.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80762.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80762):map__80762);
var env = map__80762__$1;
var ast = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80762__$1,new cljs.core.Keyword(null,"ast","ast",-860334068));
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(ns,cljs.core.namespace(new cljs.core.Keyword(null,"dispatch-key","dispatch-key",733619510).cljs$core$IFn$_invoke$arity$1(ast)))){
return com.wsscode.pathom.core.join.cljs$core$IFn$_invoke$arity$1(env);
} else {
return new cljs.core.Keyword("com.wsscode.pathom.core","continue","com.wsscode.pathom.core/continue",591698194);
}
});
}));

(com.wsscode.pathom.core.placeholder_reader.cljs$lang$maxFixedArity = 1);

com.wsscode.pathom.core.placeholder_node = com.wsscode.pathom.core.placeholder_reader;
/**
 * DEPRECATED: use p/parser to create your parser
 */
com.wsscode.pathom.core.pathom_read = (function com$wsscode$pathom$core$pathom_read(p__80764,_,___$1){
var map__80765 = p__80764;
var map__80765__$1 = (((((!((map__80765 == null))))?(((((map__80765.cljs$lang$protocol_mask$partition0$ & (64))) || ((cljs.core.PROTOCOL_SENTINEL === map__80765.cljs$core$ISeq$))))?true:false):false))?cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.hash_map,map__80765):map__80765);
var env = map__80765__$1;
var reader = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80765__$1,new cljs.core.Keyword("com.wsscode.pathom.core","reader","com.wsscode.pathom.core/reader",1510046410));
var process_reader = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__80765__$1,new cljs.core.Keyword("com.wsscode.pathom.core","process-reader","com.wsscode.pathom.core/process-reader",348867871));
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"value","value",305978217),(function (){var env__$1 = com.wsscode.pathom.core.normalize_env(env);
return com.wsscode.pathom.core.read_from(env__$1,(cljs.core.truth_(process_reader)?(process_reader.cljs$core$IFn$_invoke$arity$1 ? process_reader.cljs$core$IFn$_invoke$arity$1(reader) : process_reader.call(null,reader)):reader));
})()], null);
});
/**
 * DEPRECATED: use ident-value instead
 */
com.wsscode.pathom.core.ast_key_id = (function com$wsscode$pathom$core$ast_key_id(ast){
var key = (function (){var G__80767 = ast;
if((G__80767 == null)){
return null;
} else {
return new cljs.core.Keyword(null,"key","key",-1516042587).cljs$core$IFn$_invoke$arity$1(G__80767);
}
})();
if(cljs.core.sequential_QMARK_(key)){
return cljs.core.second(key);
} else {
return null;
}
});
/**
 * DEPRECATED: use p/entity
 *   Runs the parser against current element to garantee that some fields are loaded.
 *   This is useful when you need to ensure some values are loaded in order to fetch some
 *   more complex data.
 */
com.wsscode.pathom.core.ensure_attrs = (function com$wsscode$pathom$core$ensure_attrs(env,attributes){
return com.wsscode.pathom.core.entity.cljs$core$IFn$_invoke$arity$2(env,attributes);
});

//# sourceMappingURL=com.wsscode.pathom.core.js.map
