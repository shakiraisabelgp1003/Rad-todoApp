goog.provide('com.fulcrologic.semantic_ui.modules.dropdown.ui_dropdown_menu');
var module$node_modules$semantic_ui_react$dist$commonjs$modules$Dropdown$DropdownMenu=shadow.js.require("module$node_modules$semantic_ui_react$dist$commonjs$modules$Dropdown$DropdownMenu", {});
/**
 * A dropdown menu can contain a menu.
 * 
 *   Props:
 *  - as (elementType): An element type to render as (string or function).
 *  - children (node): Primary content.
 *  - className (string): Additional classes.
 *  - content (custom): Shorthand for primary content.
 *  - direction (enum): A dropdown menu can open to the left or to the right. (left, right)
 *  - open (bool): Whether or not the dropdown menu is displayed.
 *  - scrolling (bool): A dropdown menu can scroll.
 */
com.fulcrologic.semantic_ui.modules.dropdown.ui_dropdown_menu.ui_dropdown_menu = com.fulcrologic.semantic_ui.factory_helpers.factory_apply(module$node_modules$semantic_ui_react$dist$commonjs$modules$Dropdown$DropdownMenu.default);

//# sourceMappingURL=com.fulcrologic.semantic_ui.modules.dropdown.ui_dropdown_menu.js.map
